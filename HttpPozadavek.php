<?php 
/**
 * @file HttpPozadavek.php 
 *
 *  \brief    Soubor s definicí třídy \c HttpPozadavek
 * \details   
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      18.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo 
 *
 * 18.1.2016
 * Uložení první verze
 *
 */ 

/**
 * \class HttpPozadavek
 * \brief Zpracovává vstupní argumenty, se kterými byla volána hlavní stránka
 *
 */ 
class HttpPozadavek{
  private $formmetoda = ""; /**< Nazev volane metody  */
  private $classNo; /**< Cislo tridy z parametru */
  private $metoda = ""; /**< Nazev volane metody  */
  public $argumenty = array(); /**< Argumenty pro volanou metodu */

  public function __construct()
  {
  }
  /**
   * \brief Vrací hodnotu slotu metoda - název volané metody třídy
   */ 
  public function getMetoda(){
 
    return $this->metoda;
  }

  /**
   * Nastavi slot \$formmetoda na jednu z hodnot \c GET nebo \c POST.
   *
    * @param string $metoda Hodnata, která se zapíše do slotu. Buď  \c GET nebo \c POST.
    * @return void
    */
public function setFormMetoda($metoda)
{
  //echo "<h3>$metoda</h3>";
  $upper = strtoupper($metoda);
  if($upper == 'GET' || $upper == 'POST'){
    $this->formmetoda = $metoda;
  }
  return;
}
/**
 * Vraci obsah slotu \c classNo
 * 
  * @param 
  * @return int hodnota slotu \c classNo 
 */
public function getClassNo()
{
  return $this->classNo;
}
/**
 * Zkontroluje http_request a podle metody zajisti nasteny parametrů
 *
  * @param void
  * @return void 
 */
  public function ctiParametry()
  {
    //echo "<br>$this->formmetoda";
    //self::ctiPost();
    if($this->formmetoda!=''){
      call_user_func('self::cti'.$this->formmetoda);
    }
    return;
  }
/**
 * Zpracuje argumenty zaslane metodou POST.
 *
 * Do slotu \c classNo uloží číslo třídy a do slotu \c metoda uloží název volané metody.
 *
  * @param 
  * @return  
  */
private  function ctiPost()
{
 if (isset($_POST['mod'])){
    $this->classNo=$_POST['mod'];
 }else{
    $this->classNo=0;
 }
 if (isset($_POST['met'])){
    $this->metoda=$_POST['met'];
 }else{
    $this->metoda='';
 }

 foreach($_POST as $key => $val){
  if($key=="mod" OR $key=="met"){
    continue;
  }
  $this->argumenty[$key] = $val;
 }
 return;
}

/**
 * Zpracuje argumenty zaslane metodou GET.
 *
 * Do slotu \c classNo uloží číslo třídy a do slotu \c metoda uloží název volané metody.
 *
  * @param
  * @return 
  */
private function ctiGet()
{
 if (isset($_GET['mod'])){
    $this->classNo=$_GET['mod'];
 }else{
    $this->classNo=0;
 }
 //echo "<h3>".$_GET['met']."</h3>";
 if (isset($_GET['met'])){
    $this->metoda=$_GET['met'];
 }else{
    $this->metoda='';
 }
 foreach($_GET as $key => $val){
  if($key=="mod" OR $key=="met"){
    continue;
  }
  $this->argumenty[$key] = $val;
 }
return;
}
}
?>