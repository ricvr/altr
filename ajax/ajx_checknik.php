<?php
/**
 * @file ajx_checknik.php 
 *
 * \brief    Kontrola jedinečnosti \c nik přezdívky
 * \details   Soubor je volán z formuláře pro registraci nového uživatele. Po zadání přezdívky je a využítím  \c ajaxu zavolán tento soubor a je provedena kontrola na jedinečnost.
 * Pokud je zadaná přezdívka jedinečná, je odesláno potvrzení (obrázek fajfky). Pokud je už použito, je poslán křížek.
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 
include_once"autoload.php";
$file = ".." . DIRECTORY_SEPARATOR .'konstanty.php';
include_once $file;
//echo "Pokus";
$usr = new Uzivatele();
if($usr->checknik($_GET['nik'])){
 echo "<img src='img/fajfka.gif' class='ikona' >";
}else{
 echo "<img src='img/krizek.gif' class='ikona' >Neplatná přezdívka";
}
?> 
