<?php
/**
 * @file autoload.php 
 *
 *  \brief    soubor s funkci \c __autoload() pro zajištění vytváření instancí tříd
 * \details   Funkce je includována do všech souborů v adresáři \c ajax
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      26.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *
 *
 *
 */
  function __autoload($className)
{
  $file = ".." . DIRECTORY_SEPARATOR .'classes' . DIRECTORY_SEPARATOR . $className . '.php';
  include_once $file;
return;
}
?>

