<?php 
 /**
  * 
  * Spousti se pri vytvareni instance tridy jejiz declarace nebyla nalezena.
  *
  * @param string $className Nazev tridy, ktera nebyla nalezena 
  * @return void
  */

  function __autoload($className)
{
  $file = 'classes' . DIRECTORY_SEPARATOR . $className . '.php';
  include_once $file;
return;
} 
?>