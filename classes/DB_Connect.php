<?php
/**
 * @file DB_Connect.php
 * \brief Soubor s tridami DB_Connect, Mysql_Test, Mysql_Produce
 * \details Soubor obsahuje vsechny potrebne definice trid pro ruzne databaze a pro ruzne mody prace,
 * jako napriklad prace na vyvojovem serveru nebo na produkcnim apod.
 * Spojeni do nove databaze se nastavi zde. 
 * Stejne tak se definice trid pro ruzne db vkladaji do tohosto souboru
 *
 * 
 * @author RiC
 * @version 0.1.1
 * @copyright RiC 2015
 *
 */

/**
 * \brief Nastavuje pripojeni do libovelne databaze, pro kterou je zde pripraveno pripojeni v odpovidajicich tridach
 *
 * \details Spojeni do nove databaze se nastavi zde.
 * Stejne tak se definice trid pro ruzne db vkladaji do tohosto souboru
 *
 * <b>Pripojeni nove databaze:</b>
 * \li 1/ Vytvori se rodicovska trida DB_databaze
 * \li 2/ Z ni EXTENDS vytvori potomek, ve kterem budou nastaveny prihlasovaci udaje
 * \li 3/ do DB_Connect::newConnect() se prida do prislusneho SWITCHe novy CASE
 *
 * Pouziti:
 * \code
 *
 * $dbh = DB_Connect::newConnect();
 * $stmt = $dbh->prepare("SELECT id, text FROM test ORDER BY id desC");
 * $result = $stmt->execute(); // provedeni dotazu
 * $vysledek = $result->fetch(); // vytvoreni objektu s vracnymi hodnotami dotazu
 * $vysledek->last(); // prechod na posledni zaznam dotazu
 *
 * echo "Hodnota ID: ",$vysledek->id; // vypsani pole 'id'
 * echo "Text: ",$vysledek->text; // vypsani pole 'text'
 *
 * \endcode
 */
class DB_Connect {
	
	/** @var string $workType hodnota udava, jake pripojeni do db se pouzije */
	private static $workType = ""; // Test|Produce
	
	/**
	 * Konstruktor
	 */
	public function __construct()
	{
	}
	
	/**
	 * Staticka metoda pro nastaveni aktualni db
	 *
	 * Nataveni:
	 * \code
	 * DB_Connect::setDb('Test');
	 * //nebo
	 * DB_Connect::setDb('Produce');
	 * \endcode
	 * V metodě \c newConnect() se automaticky volá metoda \c setServer(), která nastaví buď lokální server nebo 'Produce'
	 * 
	 * @param
	 *        	typ popis
	 * @return string vystup
	 */
	public static function setDb($workType)
	{
		self::$workType = $workType;
	}
	/**
	 * Staticka funkce - vraci instanci (objekt) pro pripojeni do db
	 */
	public static function newConnect()
	{
		self::setServer ();
		switch (self::$workType) {
			case "Test_ntb" :
				return new Mysql_Test_ntb ();
			case "Test" :
				return new Mysql_Test ();
			case "Test_server" :
				return new Mysql_Test_server ();
			case "Produce" :
				return new Mysql_Produce ();
			default :
				throw new Exception ( "Nepodarilo se vytvorit instanci pripojeni do databaze" );
		}
	}
	
	/**
	 * Určuje, jestli jde o local server nebo jiný.
	 * Podle toho nastaví proměnou $workType.
	 * Volá se z metody \c newConnect()
	 */
	private static function setServer()
	{
		if (($_SERVER ['SERVER_ADDR'] == "127.0.0.1") 
				or ($_SERVER ['SERVER_ADDR'] == "::1") 
				or ($_SERVER ['SERVER_NAME'] == "localhost") 
				or ($_SERVER ['SERVER_NAME'] == "pracovna")) {
			self::setDb ( 'Test' );
		} elseif (($_SERVER ['SERVER_ADDR'] == "217.11.249.145") 
				or ($_SERVER ['SERVER_NAME'] == "a1t.it-tovarys.cz")) {
			self::setDb ( 'Test_server' );
		} elseif (($_SERVER ['SERVER_ADDR'] == "31.15.10.16") 
				or ($_SERVER ['SERVER_NAME'] == "www.altrafootwear.cz")) {
			self::setDb ( 'Produce' );
		}
	}
} // konec class
/**
 * @brief Počítač Pracovna
 *
 * Potomek tridy 'DB_Mysql'
 * Obsahuje prihlasovaci udaje do testovaci db
 * Instance je vracena tridou DB_Connect
 */
class Mysql_Test extends DB_Mysql {
	/** @var string Prihlasovaci jmeno do db */
	protected $user = "root";
	
	/** @var string Heslo pro prihlaseni do db */
	protected $pass = "toor16";
	
	/** @var string Adresa serveru s databazi */
	protected $server = 'localhost';
	
	/** @var string Nazev databaze */
	protected $dbname = 'altra';
	
	/**
	 * Konstrukt tridy Mysql_Test
	 */
	public function __construct()
	{
	}
}

/**
 * Potomek tridy 'DB_Mysql'
 * \brief Obsahuje prihlasovaci udaje do testovaci db na notebooku
 * Instance je vracena tridou DB_Connect
 */
class Mysql_Test_ntb extends DB_Mysql {
	/** @var string Prihlasovaci jmeno do db */
	protected $user = "root";
	
	/** @var string Heslo pro prihlaseni do db */
	protected $pass = "";
	
	/** @var string Adresa serveru s databazi */
	protected $server = 'localhost';
	
	/** @var string Nazev databaze */
	protected $dbname = 'prcek';
	
	/**
	 * Konstrukt tridy Mysql_Test
	 */
	public function __construct()
	{
	}
}
/**
 * Potomek tridy 'DB_Mysql'
 * \brief Obsahuje prihlasovaci udaje do testovaci db testovacim serveru
 * Instance je vracena tridou DB_Connect
 */
class Mysql_Test_server extends DB_Mysql {
	/** @var string Prihlasovaci jmeno do db */
	protected $user = "19881_22401";
	
	/** @var string Heslo pro prihlaseni do db */
	protected $pass = "cvrcek36";
	
	/** @var string Adresa serveru s databazi */
	protected $server = 'localhost';
	
	/** @var string Nazev databaze */
	protected $dbname = '19881_test';
	
	/**
	 * Konstrukt tridy Mysql_Test
	 */
	public function __construct()
	{
	}
}

/**
 * Obsahuje prihlasovaci udaje na produkcni server
 *
 * Instance je vracena tridou DB_Connect
 * Potomek tridy DB_Mysql
 */
class Mysql_Produce extends DB_Mysql {
	/** @var string Prihlasovaci jmeno do db */
	protected $user = "altrafoocz";
	
	/** @var string Heslo pro prihlaseni do db */
	protected $pass = "g2M4aNVnFC";
	
	/** @var string Adresa serveru s databazi */
	protected $server = 'localhost';
	
	/** @var string Nazev databaze */
	protected $dbname = 'altrafoocz';
	
	/**
	 * Konstrukt tridy Mysql_Produce
	 */
	public function __construct()
	{
	}
}
?>
