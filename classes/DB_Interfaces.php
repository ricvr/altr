<?php 
/**
 * Soubor s definicemi rozhrani, ktera se pouzivaji
 *
 */

/**
 * Rozhrani pro tridu vytvarejici  pripojeni do db
 */ 
interface iDB_Connection
{
  /** Vytvori pripojeni  */  
  public function connect();

  /** Provede pripravu sql dotazu */
  public function prepare($query);
}

/**
 * Rohrazni pro tridu pracujici s prikazem sql
 */ 
interface iDB_MysqlStatement
{
  //public function bind_param($ph,$pv);
  public function execute();
  public function fetch_row();
  public function fetch_assoc();
  public function fetchall_assoc();
  public function fetch();

}
?>  
