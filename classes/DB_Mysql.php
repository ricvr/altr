<?php 
/**
 * @file DB_Mysql.php
 * \brief Soubor DB_Mysqlphp s definici tridy DB_Mysql
 *
 * Soubor obsahuje ...
 *
 * @author RiC
 * @version 0.1.1
 * @copyright RiC 2015
 */ 

/* Vlozeni definic rozhrani */
require_once 'DB_Interfaces.php';

/**
 * \brief Rodicovska trida pro pripojeni do databaze
 *
 * \details Potomky jsou tridy pro pripojeni do MySql na ruznych serverech. 
 * Instance je vracena fn DB_Connect::newConnect()
 * <br><strong>Pouziti:</strong>
 * \code
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare("SELECT id, text FROM test ORDER BY id desC");
  $result = $stmt->execute(); //provedeni dotazu
  $vysledek = $result->fetch();   //vytvoreni objektu s vracnymi hodnotami dotazu
  $vysledek->last();    // prechod na posledni zaznam dotazu

  echo "Hodnota ID: ",$vysledek->id;    //vypsani pole 'id'
  echo "Text: ",$vysledek->text;    // vypsani pole 'text'
  
 * \endcode
 *
 * @version 0.0.1 xxx Vyvojova verze
 */ 
class DB_Mysql implements iDB_Connection
{
  
  /** @var handler Pripojeni do db vytvorene 'mysqli_connect()' */
  private $dbh;

  /**
   * Konstruktor uloze predane parametry do prislusnych slotu
   *
   * @return void
   */ 
  public function __construct()
  {
  }

  /**
   * \brief Vytvori spojeni do DB pomoc� fn mysli_connect.
   *
   * \details Handler se ulozi do slotu 'dbh' Udaje pro pripojeni se vlozi pri vytvoreni instance
   *
   * @return void 
   */
  public function connect()
  {
/*
    $this->dbh = mysqli_connect($this->server,
                                  $this->user,
                                  $this->pass,
                                  $this->dbname);
*/
    $this->dbh = new mysqli($this->server,
                                  $this->user,
                                  $this->pass,
                                  $this->dbname);
    if(!$this->dbh){
      throw new Exception;
    }
    $this->dbh->query("SET NAMES utf8");
    //echo "<br>Typ resource: " . get_resource_type($this->dbh);
    if(!mysqli_select_db($this->dbh,$this->dbname)){
      throw new Exception;
    }
  }

  /**
   * \brief Kontroluje existenci pripojeni do db, pripadne ho vytvori.
   * Vraci instanci DB_MysqlStatement
   *
   * \details Vraci instanci DB_MysqlStatement, ktery je pripraven na vykonani dotazu
   *
   * Pouzit�:
   * <code>
   * <br>$db = DB_Connect::newConnect();
   * <br> $stmt = $db->prepare("SELECT id, text FROM test WHERE id <= :1 ");
   * <br> $result = $stmt->execute(2); // vlozeni parametru ':1' pro provedeni dotazu
   * </code>
   *
   * @param string $query SQL dotaz, ktery se bude zpracovavat 
   *
   * @return DB_MysqlStatement Pripraveny objekt pro dalsi praci s dotazem 
   * 
   */
  public function prepare($query)
  {
    if(!$this->dbh) {
      $this->connect();
    }
    return new DB_MysqlStatement($this->dbh, $query);
  }
}

?> 
