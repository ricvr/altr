<?php 
/**
 * \file DB_MysqlStatement.php Soubor s definici prislusne tridy
 *
 * Kodování souboru: utf-8
 */ 

/** Vlozeni definic rozhrani */
require_once 'DB_Interfaces.php';

/**
 * \brief Trida pro zpracovani sql dotazu
 * \details Instance je vytvorena v DB_Mysql::prepare(). Vlastni sql dotaz je proveden zavolanim metody DB_MysqlStatement::execute().
 *
 * 
 */ 
class DB_MysqlStatement implements iDB_MysqlStatement
{
  /** \brief Obsahuje vyslede sql dotazu 
   */
  public $result;

  /** @var string|int Zatim neznamy ucel */
  public $binds = array();

  /** @var string SQL dotaz   */  
  public $query;

  /**  @var Handler Pripojeni do db  */
  public $dbh;

  public $insert_id; /**< @brief Pro INSERT a UPDATE vraci ID vlozene AUTO_INCREMENT, pro ostatni vraci '0' */

  /**
   * \brief Konstruktor.
   * \details Provede ulozeni predavanych parametru do prislusnych slotu
   *
   * @param string $dbh pripojeni do databaze popis 
   * @param string $query SQL dotaz, ktery se ma vykonat
   * 
   */
  public function __construct($dbh, $query)
  {
    $this->dbh = $dbh;
    $this->query = $query;
  }

  /**
   * @brief Provede sql dotaz Pokud dotaz ma nejake parametry, provede jejich navazani
   * \details Vraci instanci tridy DB_MysqlStatement
   *
   * Metoda nema povine argumenty. Pripadne argumenty jsou zpracovany funkci 'func_get_args()'
   * a jsou svazany s argumenty sql dotazu
   *
   * Vkladane argumenty jsou osetreny funkci mysqli_real_escape_string().
   *
   * <p><strong>Přiklad:<strong>
   * \code
   $stmt = $db->prepare("SELECT id, text FROM test WHERE id >= :1 ORDER BY id desC");
   $result = $stmt->execute(2);
   \endcode
   *
   *  mix $? argumenty zpracovany funkci 'func_get_args()'
   *
   * @return DB_MysqlStatement Instance tridy DB_MysqlStatement s vysledkem dotazu pro dalsi zpracovani
   */
   public function execute()
{
  // nacteni vazanych parametru pomoci fn 'func_get_args()'
  $binds = func_get_args();
  foreach($binds as $index => $name){
    $this->binds[$index+1] = $name;
  }

  $cnt = count($binds);
  $query = $this->query;

  // vlozeni vazanych parametru do sql dotazu
  foreach($this->binds as $idx=>$val){
    $query = str_replace(":$idx","'" . mysqli_real_escape_string($this->dbh, $val) . "'", $query);
  }
  //echo "<p>$query</p>";
  $this->result = mysqli_query($this->dbh,$query);
  $this->insert_id = $this->dbh->insert_id;
  //echo "<p>";
  //var_dump($this->result);
  //var_dump($this->dbh);
  //Je-li vrace vysledek dotazu nebo modifikujici prikaz
  if(($this->result)OR($this->result === TRUE)){
    return $this;
  }
  throw new Exception($this->dbh->error . " || " . $query);
}


/**
 * Vraci jeden radek z vysledku dotazu
 * <br>Ukazatel zustava na miste a ukazuje na dalsi zaznam
 *
  *
  * @return array Pole obsahujici jeden zaznam. Nejde o asociativni pole 
  */
public function fetch_row()
{
  return mysqli_fetch_row($this->result);
}


/**
 * \brief Vraci jeden zaznam jako asociativni pole
 *
  *  
  * @return array Asociativni pole obsahujici jeden zaznam 
  */
public function fetch_assoc()
{
return mysqli_fetch_assoc($this->result);
}


/**
 * \brief Vraci pole, kde kazda polozka pole je tvorena jednim zaznamem z tabulky. Ukazatel do "result" se vraci na zacatek.
 *
  *  
  * @return array Associativni pole se vsemi zaznamy z tabulky 
  */
public function fetchall_assoc()
{
  $retval = array();
  //echo "<p> seekRow </p>";
  mysqli_data_seek($this->result,0);
  while($row = $this->fetch_assoc()){
    $retval[] = $row;
  }
  mysqli_data_seek($this->result,0);
  return $retval;
}

 /**
  * \brief Vraci objekt tridy \c DB_Result s vysledky dotazu
  * \details S vysledky se pak pracuje pomoci objekut tridy DB_Result
  *
  * @param void
  * @return DB_Result 
  */
  public function fetch()
  {
    return new DB_Result($this);
  }
}
?>
