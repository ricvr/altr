<?php 
/**
 * @file DB_Result.php Definice tridy DB_Result
 *
 * @version 0.1 zkusebni verze souboru
 */  

/**
 * \brief Vysledkova trida, ktera obsahuje vysledek SQL dotazu
 * \details Bude vytvarena objektem 'DB_MysqlStatement'
 *  Obsahuje funkce pro prochazeni pole vysledku
 *
 * @version 0.0.1 Zkusebni verze tridy
 */ 
class DB_Result{
  
  protected $stmt;  /**< \brief DB_MysqlStatement Objekt s provedenym dotazem, ktery se bude zpracovavat */
  protected $result = array(); /**< \brief array[] $result Pole s daty  */ 
  private $rowIndex = 0; /**< \brief int ukazatel do pole s vysledky */
  private $currIndex = 0; /**< \brief int aktualni ukazatel do pole s vysledky */
  private $done = false;  /**< \brief boolen flag pro nastaveni proedene operace*/
  public $num_rows; /**< \brief Po�et z�znam�*/

  /**
   * \brief Priradi parametr do prislusneho slotu
   *
   * \details Kontroluje se, aby predavany parametr byl typu DB_MysqlStatement
   *
   * @param DB_Statement $stmt Objekt danne tridy 
   * @return void
   */
  public function __construct(DB_MysqlStatement $stmt)
  {
    $this->stmt = $stmt;
    $this->first(); // ukazatel se nastav� na prvni zaznam
  }

  /**
   * \brief Je volana pri pokusu o cteni neexistujiciho slotu.
   *
   * @param string $slot  Nazev pozadovaneho slotu/vlastnosti/propeties 
   *
   * @return string vystup 
   */
  public function __get($slot)
  {
    //echo "<br>CurrIndex: ",$this->currIndex,"<br>Slot: ",$slot, "<br>Zaznam: " ;
//     var_dump($this->result);

    //echo "<p>Konec zaznamu - fn __get()</p>";
    if(array_key_exists($slot,$this->result[$this->currIndex])){
      return $this->result[$this->currIndex][$slot];
    }
  }
  /**
   * \brief Nastavi vnitrni ukazatel na prvni zaznam
   *
   * @param void
   * @return DB_Result vlastni instance 
   */
  public function first()
  {
    if(!$this->result){
      foreach($this->stmt->fetchall_assoc() as $val){
        $this->result[] = $val;
      }
    }
    $this->currIndex = 0;
    return $this;
  }


  /**
   * \brief Nastavi vnitrni ukazatel na posledni zaznam
   * vraci vlastni instanci
   *
   * @param void
   * @return DB_Result vlastni instanci 
   */
  public function last()
  {
    if(!$this->done){
      foreach($this->stmt->fetchall_assoc() as $val){
        $this->result[] = $val;
      }
    }
    $this->done = true;
    $this->currIndex = $this->rowIndex = count($this->result)-1;
    return $this;
  }


  /** \brief Posune vnitrni ukazatel na dalsi zaznam ve vysledku 
   * Vraci vlastni instanic.
   * 
   * Pokud index do $this-result bude vetsi nez pocet polozek v tomto poli, vraci FALSE
   * <code>
   * <p>
   *
   * if(!$vysledek->next()){
   *    echo "<h4>P�ekro�en index</h4>";
   * }
   *
   * </code>
   *
   * @TODO odladit pouziti, kdyz je ukazatel na poslednim zaznamu
   *
   * @param void
   * @return DB_Result vlastni instance 
   */
  public function next()
  {
    if($this->done){
      return false;
    }
    $offset = $this->currIndex + 1;

    if(count($this->result) <= $offset){
      return false;
    }

    if(!$this->result[$offset]){
      $row = $this->stmt->fetch_assoc();
      if(!$row){
        $this->done = true;
        return false;
      }
      $this->result[$offset] = $row;
      ++$this->rowIndex;
      ++$this->currIndex;
      return $this;
    }else{
      ++$this->currIndex;
      return $this;
    }
  }

  /**
   * Posune vnitrni ukazatel na predchozi zaznam ve vysledku 
   * Vraci vlastni instanic
   *
   * @TODO telo metody se musi dodelat
   * @param void
   * @return DB_Result vlastni instance 
   */
  public function prev()
  {
    if($this->currIndex == 0){
      return false;
    }
    --$this->currIndex; 
    return $this;
  }
}
?>

