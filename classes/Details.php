<?php 
/**
 * @file Details.php
 *
 *  \brief    24 - Zobrazení detailů produktu
 * \details   
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      18. 6. 2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo  
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief 24 class __Details__ Zobrazení detailů vybraného produktu
 * \details   Detailnější popis
 */ 
class Details{
	private $dbIdx;
	private $categoryId;
	private $productId;
  
  public function __construct()
  {
  	$this->dbIdx = new IndexDb ();
  	 
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy  s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{

    echo "<h3>Použita neznámá metoda: ",$funname,"</h3> Volaná s argumenty:";
    var_dump($arg);
    echo "<h3>Volání bez metody</h3>";

}  

/**
 * @brief #24 - Vstupní metoda pro zobrazení deatilu vybraného produktu
 *
 * @param int $idprod ID produktu, jehoz detail se bude zobrazovat 
 * @param int $idkat ID kategorie, do ktere patří
 *
 * @return
 */
public function dtDetail($in) 
{
	$this->productId = $in['idprod'];
	$this->categoryId = $in['idkat'];
	
	//echo "<h3>Stránka s detaily produktu</h3>";
	//var_dump($in);
	$showp = new ShowProducts();
	$showp->spPredkove($in['idkat']);
	
	echo "<div id='predci'>";
	$showp->spVypsatPredky($in['idkat']);
	echo "</div>"; // id='potomci'>";
	
	//echo "<br>";
	echo "<div id='potomci'>";
	$showp->spVypsatPotomky($in['idkat']);
	echo "</div>";// id='potomci'>";
	
	echo "<div class='detail-produkt'>";
	$this->dtProduktDetails();
	echo "</div>"; // class='detail-produkt'>";
	
	echo "<div class='detail-seznam'>";
	$this->dtTiles();
	echo "</div>"; // class='detail-seznam'>");
	
}
/**
 * @brief Zobrazí dlaždice produktů spadajících do stejné kategorie jako produkt, jehož detaily se aktuálně zobrazují
 *
 * @param int $idkat ID kategorie jejíž produkty a potomci se hledají
 *
 * @return
 */
private function dtTiles() 
{
	echo "<div  class='clearfix'><h3>Produkty v&nbsp;kategorie</h3></div>";
	$tiles = new Tiles();
	$tiles->setCategoryId($this->categoryId);
	$tiles->tilProductes();
	$tiles->tilShowTiles();
	
}
/**
 * @brief Nacte detaily produktu
 *
 * @param type name
 * 	pk_produkt
    		,id_kategorie
    		,nazev
    		,cenakc
    		,cenacizi
    		,cenamena
    		,eshop_url
    		,eshop_text
    		,popis_kratky
    		,popis_dlouhy
    		,detail1 AS hmotnost
    		,detail2 AS velikosti
    		, detail3 as tlumeni
 *
 * @return
 */
private function dtProduktDetails() 
{
	$details = $this->dbIdx->productsdetails($this->productId);
	if (count($details) < 1) {
		echo "Detaily produktu nebyly nalezeny";
		return;
	}
	$pictures = $this->dbIdx->productspictures($this->productId);
	//var_dump($details);
	extract($details);
	$_SESSION['seo']['title'] = "Altra - ".$nazev;
	$_SESSION['seo']['description'] = $popis_kratky;
	
	
	// podle zarizeni se vybere velikost obrazku. Na telefon jdou mensi
	$imgsize=0;
	if(PHONE){
		$imgsize = 0; // zobrazuje se na telefonu, posle se mensi obrazek
	}else{
		$imgsize = 1; // NEzobrazuje se na telefonu, muze se poslat vetsi
	}
	echo "<div class='detail-obrazek'>";
	
	if (count($pictures)>0){
		echo "<img src='vypsat_obr.php?id=".$pictures[0]['pk_obrazek'] ."&amp;vel=$imgsize' width='100%'>";
	}else{
		echo "<img src='vypsat_obr.php?id=0&amp;vel=$imgsize' width='100%'>";
	}
	echo "</div>";
	//echo "<h3>Detaily produktu</h3>";
	echo "<h1 class='detail-nazev'>";
	echo $nazev;
	echo "</h1>";
	
	echo "<div class='detail-popkratky'>";
	echo nl2br($popis_kratky);
	echo "</div>";

	echo "<div class='detail-hmotnost'>";
	echo "<span class='zvyrazneni'>Hmotnost:</span> ".$hmotnost."&nbsp;g";
	echo "</div>";

	echo "<div class='detail-velikosti'>";
	echo "<span class='zvyrazneni'>Velikost:</span> ".$velikosti;
	echo "</div>";

	echo "<div class='detail-tlumeni'>";
	echo "<span class='zvyrazneni'>Tlumení:</span> ".$tlumeni; 
	echo "</div>";

	echo "<div class='detail-cenakc'>";
	echo "<span class='zvyrazneni'>Cena:</span> ".$cenakc.",00&nbsp;Kč";
	echo "</div>";

	echo "<div class='detail-eshop'>";
	if(strlen($eshop_url)>0){
		//echo "<button onClick=\"window.open('$eshop_url')\">";
		echo "<a href='$eshop_url' target='_blank' title='Přesměrování do e-shopu' class='detailbutton'>";
	  echo "Přejít do e-shopu";
		//echo $eshop_text;
		echo "</a>";
		//echo "$eshop_text</button>";
	}
	echo "</div>";
	
	echo "<div class='detail-popdlouhy'>";
	echo nl2br($popis_dlouhy); 
	echo "</div>";
/*
	echo "<div class='detail-'>";
	echo "</div>";

	echo "<div class='detail-'>";
	echo "</div>";
	*/
	
	//var_dump($pictures);
	foreach ($pictures as $pic){
			echo "<img src='vypsat_obr.php?id=".$pic['pk_obrazek'] ."&amp;vel=1' width='100%'>";
	}
	
}
}



?>