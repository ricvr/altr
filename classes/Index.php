<?php 
/**
 * @file Index.php 
 *
 *  \brief    21 Soubor se vstupni class
 * \details   
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      22.5.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 *
 */ 

/**
 * \brief 21 - Vstupni trida webu. Vola se v pripade, ze nejsou zadne pozadavky. Zobrazi se tri nahodne vybrane produkty.
 * \details   Detailnější popis
 */ 
class Index{
  private $abc_products; /**< @brief Obsahuje tri nahodne vybrane produkty */
  private $dbIdx; /**< Spojeni do databaze */

  public function __construct()
  {
  	$this->dbIdx = new IndexDb();
  	 
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
/*
    echo "<h3>Použita neznámá metoda: ",$funname,"</h3> Volaná s argumenty:";
    var_dump($arg);
    echo "<h3>Volání bez metody</h3>";
    */
  $this->inxUvod();
}
/**
  *  \brief Uvodni metoda, ktera zajisti zobrazeni trech produktu na uvodni strance.
  *  @param typ popis
  *  @return string vystup
  */
public function inxUvod()
{ // BEGIN function
 // echo "<h3>Úvodní (default) metoda</h3>".count($this->abc_products);
  //var_dump($this->abc_products);
  
  $this->inxGet3Products();
  // Jetsli nejsou zadne produkty, navrat bez vypisovani
  if(count($this->abc_products) < 1){
  	return;
  }
  
  echo "<div id='uvod-akce'>";
  $this->inxUvodAkce();
  //var_dump($this->abc_products);
  echo "</div>";
  
  echo "<div id='uvod-b'>";
  $this->inxUvodB();
  echo "</div>";
  
  echo "<div id='uvod-c'>";
  $this->inxUvodC();
  echo "</div>";
 	
} // END function
/**
 * @brief Cast uvodni obrazovky pro zobrazeni reklamy|Akce|Upotavky apod.
 * 
 * Normalne by se zde mela zobrazovat nejaka reklamni akce, slevy apod. Pokud o to bude zajem, bude to doprogramovano.
 * Zatim se tam zobrazuje pouze jeden nahodne vybrany produkt.
 * 
 * vola:
 * 	IndexDb::productspictures()
 * 
 * @param type name
 *
 * @return
 */
private function inxUvodAkce() 
{
	
	//echo "inxUvodAkce";
	$poradi_produktu = 0;
	if (!array_key_exists($poradi_produktu, $this->abc_products)){
		//echo "&nbsp;";
		return;
	}
	
	$product = $this->abc_products[$poradi_produktu];
	extract($product);
	$pictures = $this->dbIdx->productspictures($pk_produkt);
	echo "<h1 class='detail-nazev'>";
	echo "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pk_produkt."&amp;idkat=".$id_kategorie."' title='Zobrazení detailů'>";
	echo $nazev;
	echo "</a>";
	echo "</h1>";
	
	echo "<div class='uvod-a-obrazek'>";
	echo "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pk_produkt."&amp;idkat=".$id_kategorie."' title='Zobrazení detailů'>";
	if (count($pictures)>0){
		echo "<img src='vypsat_obr.php?id=".$pictures[0]['pk_obrazek'] ."&amp;vel=1'>";
	}else{
		echo "<img src='vypsat_obr.php?id=0&amp;vel=1'>";
	}
	echo "</a>";
	echo "</div>";
	
	echo "<div class='detail-popkratky'>";
	echo nl2br($popis_kratky);
	echo "</div>";
	
	echo "<div class='detail-cenakc'>";
	echo "Cena: ".$cenakc.",00&nbsp;Kč";
	echo "</div>";
	
	echo "<div class='detail-hmotnost'>";
	echo "Hmotnost: ".$hmotnost."&nbsp;g";
	echo "</div>";
	
	echo "<div class='detail-velikosti'>";
	echo "Velikost: ".$velikosti;
	echo "</div>";
	
	echo "<div class='detail-tlumeni'>";
	echo $tlumeni;
	echo "</div>";
	
	echo "<div class='detail-popdlouhy'>";
	echo nl2br($popis_dlouhy);
	echo "</div>";
	echo "<div class='tile-link' style='margin-top:2em;'>";
	
		echo "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pk_produkt."&amp;idkat=".$id_kategorie."' title='Zobrazení detailů'>";
		echo "Detaily</a>";
		echo "</div>";
	
	
}
/**
 * @brief Cast "B" uvodni obrazovky
 *
 * @param type name
 *
 * @return
 */
private function inxUvodB() 
{
	//echo "inxUvodB";
	$poradi_produktu = 1;
	if (!array_key_exists($poradi_produktu, $this->abc_products)){
		//echo "&nbsp;";
		return;
	}
	
	$pr = $this->abc_products[$poradi_produktu];
	//extract($product);
	$pictures = $this->dbIdx->productspictures($pr['pk_produkt']); 
	
	$link = "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pr['pk_produkt']."&amp;idkat=".$pr['id_kategorie']."'>";
	if (count($pictures)>0){
		$idPic = $pictures[0]['pk_obrazek'];
	}else {
		$idPic = 0;
	}
		//echo("<br>ID obr: $idPic");
	
	echo "<div class='uvod-ab'>";
		echo "<div class='tile-pic'>";
		
		echo $link;
		echo "<img src='vypsat_obr.php?id=".$idPic."&amp;vel=0' >";
		echo "</a>";
		echo "</div>"; // class=tile-pic>";
		echo "<h2 class='tile-nazev'>";
		echo $link;
		echo $pr['nazev'];
		echo "</a>";
		echo "</h2>";
	
		echo "<div class='tile-popkratky'>";
		echo $pr['popis_kratky'];
		echo "</div>";
	
		echo "<div class='tile-cena'>";
		echo $pr['cenakc'].",00&nbsp;Kč";
		echo "</div>";
		
		echo "<div class='tile-link'>";
	
		echo "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pr['pk_produkt']."&amp;idkat=".$pr['id_kategorie']."'title='Zobrazení detailů'>";
		echo "Detaily</a>";
		echo "</div>";
		
	echo "</div>"; // class='uvod-ab'>";
	
}
/**
 * @brief Cast "C" uvodni obrazovky
 *
 * @param type name
 *
 * @return
 */
private function inxUvodC() 
{
	//echo "inxUvodC";
	$poradi_produktu = 2;
	if (!array_key_exists($poradi_produktu, $this->abc_products)){
		//echo "&nbsp;";
		return;
	}
	
	$pr = $this->abc_products[$poradi_produktu];
	//extract($product);
	$pictures = $this->dbIdx->productspictures($pr['pk_produkt']);
	
	$link = "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pr['pk_produkt']."&amp;idkat=".$pr['id_kategorie']."'>";
	if (count($pictures)>0){
		$idPic = $pictures[0]['pk_obrazek'];
	}else {
		$idPic = 0;
	}
	//echo("<br>ID obr: $idPic");
	echo "<div class='uvod-ab'>";
	
	echo "<div class='tile-pic'>";
	
	echo $link;
	echo "<img src='vypsat_obr.php?id=".$idPic."&amp;vel=0' >";
	echo "</a>";
	echo "</div>"; // class=tile-pic>";
	echo "<h2 class='tile-nazev'>";
	echo $link;
	echo $pr['nazev'];
	echo "</a>";
	echo "</h2>";
	
	echo "<div class='tile-popkratky'>";
	echo $pr['popis_kratky'];
	echo "</div>";
	
	echo "<div class='tile-cena'>";
	echo $pr['cenakc'].",00&nbsp;Kč";
	echo "</div>";
	
	echo "<div class='tile-link'>";
	echo "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pr['pk_produkt']."&amp;idkat=".$pr['id_kategorie']."' title='Zobrazení detailů'>";
	echo "Detaily</a>";
	echo "</div>";
	
	echo "</div>";// class='uvod-ab'>";
	
}
/**
 * @brief Vybira nahodne tri produkty ze seznamu vsech produktu. Produkty jsou ulozeny do slotu _$this->abc_products_
 * @details vola IndexDb::productsall()
 * @param type name
 *
 * @return
 */
private function inxGet3Products() 
{
	$products = $this->dbIdx->productsall();
	$pocet = count($products);
	$index =  $pocet-1;
	if($pocet < 1){
		$this->abc_products = array();
		return;
	}
	
	$rand_list = array();
	for($x = 0;$x <= $index;$x++){
		
		// kontrola, aby kazdy index (=product´) byl vybran pouze jednou
		do{
			$rand_num = rand(0,$index);
		}while(in_array($rand_num,$rand_list));

		$rand_list[] = $rand_num;
		$this->abc_products[] = $products[$rand_num];
	}
	
}
}
?>


