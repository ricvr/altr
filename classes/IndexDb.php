<?php 
/**
 * @file IndexDb.php
 *
 *  \brief    __IndexDB__ Prace s databazi pro zobrazovani produktu
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo Po uložení obrázku odstranit dočasné soubory
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief _Nema pridelene cislo_ __IndexDB__ Metody s SQL dotazy pro prezentacni web
 * \details   
 * Class je volana pouze vnitrne z metod a proto nema vlastni cislo
 */ 
class IndexDb{
  private $dbh;
  private $table_obrazky;
  private $table_kategorie;
  private $table_produkty;


  public function __construct()
  {
    $this->dbh = DB_Connect::newConnect();
    $this->table_obrazky = TABLE_PREFIX . "_obrazky";
    $this->table_kategorie = TABLE_PREFIX . "_kategorie";
		$this->table_produkty = TABLE_PREFIX . "_produkty"; 
		$this->table_prodejci = TABLE_PREFIX . "_prodejci"; 
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
  echo "<h3>Použita neznámá metoda: $funname",$arg[0],"</h3>";

}  

/**
 * @brief Nacte obrazky pro produkty, jejichz ID je v paramteru
 *
 * @param string $list Retezec pro frazi IN Selectu
 *
 * @return array Pole s id obrazku
 */
public function takepictures($list) 
{
	if (!isset($list)){
		return array();
	}
	    $q = "SELECT pk_obrazek, fk_produkt, nazev
	    			FROM ".$this->table_obrazky ."
	    			WHERE fk_produkt IN ($list)
	    			ORDER BY	fk_produkt,pk_obrazek";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute();
	    $data = $result->fetchall_assoc();
	    return $data;
}
/**
  *  \brief Vraci deti kategorie, jejiz ID se predava v parametru
  *  @param int $idrodic ID kategorie, jejiz deti se hledaji
  *  
  *  @return array Pole s ID kategorii, ktere jsou primymi potomky kategorie v paramteru
  */
public function kategorierodice($idrodic)
{ // BEGIN function
    $q = "SELECT id_kategorie,id_rodic,uroven,kategorie,popis
          FROM ".$this->table_kategorie ."
          WHERE id_rodic = :1
          ORDER BY razeni";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($idrodic);
    $data = $result->fetchall_assoc();
    return $data;
} // END function

/**
  *  \brief vraci pole se vsemi kategoriemi
  *  @param ---
  *  @return array
  */
public function kategorievse()
{ // BEGIN function
    $q = "SELECT id_kategorie,id_rodic,uroven,kategorie,popis
          FROM ".$this->table_kategorie ."
          ORDER BY id_rodic";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute();
    $data = $result->fetchall_assoc();
    return $data;
} // END function

/**
 * @brief Vraci seznam s produkty, ktere jsou potomky kategorie predane v parametru $ides
 * @param string $ides Retezec s kategorie, pro ktere se bude hledat produkty
 */
public function produktes($ides) {
    $q = "SELECT 
    		pk_produkt
    		,id_kategorie
    		,nazev
    		,cenakc
    		,cenacizi
    		,cenamena
    		,eshop_url
    		,eshop_text
    		,popis_kratky
    		,popis_dlouhy
    		,detail1 AS hmotnost
    		,detail2 AS velikosti
    		, detail3 as tlumeni
    		 FROM ".$this->table_produkty ."
    		 WHERE (skryty IS NULL)  AND (id_kategorie IN ($ides))";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute();
    $data = $result->fetchall_assoc();
    return $data;
	
}
/**
 * @brief Vraci pole s detaily jednoho produktu
 *
 * @param int $idprod ID produktu
 *
 * @return array DEtaily produktu v poli
 */
public function productsdetails($idprod) 
{
    $q = "SELECT 
    		pk_produkt
    		,id_kategorie
    		,nazev
    		,cenakc
    		,cenacizi
    		,cenamena
    		,eshop_url
    		,eshop_text
    		,popis_kratky
    		,popis_dlouhy
    		,detail1 AS hmotnost
    		,detail2 AS velikosti
    		, detail3 as tlumeni
    		 FROM ".$this->table_produkty ."
    		 WHERE pk_produkt= :1 AND skryty IS NULL";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($idprod);
    $data = $result->fetchall_assoc();
    if (isset($data[0])){
	    return $data[0];
    }else {
    	return array();
    }
}
/**
 * @brief Vraci pole s id obrazků, ktere patri k produktu
 *
 * @param int $idprod ID produktu
 *
 * @return
 */
public function productspictures($idprod) 
{
	    $q = "SELECT pk_obrazek,nazev FROM  ".$this->table_obrazky ." 
	    		WHERE fk_produkt=:1";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute($idprod);
	    $data = $result->fetchall_assoc();
	    return $data;
}
/**
 * @brief Vraci seznam vsech produktu v tabulce __altr_produkty__
 * @details Volano z _Index::inxGetProducts()_ a mozna i z dalsich(?)
 * @param 
 */
public function productsall() {
	$q = "SELECT
    		pk_produkt
    		,id_kategorie
    		,nazev
    		,cenakc
    		,cenacizi
    		,cenamena
    		,eshop_url
    		,eshop_text
    		,popis_kratky
    		,popis_dlouhy
    		,detail1 AS hmotnost
    		,detail2 AS velikosti
    		, detail3 as tlumeni
    		 FROM ".$this->table_produkty ." WHERE skryty IS NULL" ;
	$stmt = $this->dbh->prepare($q);
	$result = $stmt->execute();
	$data = $result->fetchall_assoc();
	return $data;

}
/**
 * @brief Seznam všech prodejců
 *
 * @param type name
 *
 * @return array Seznam prodejců
 */
public function prodejci() 
{
	    $q = "SELECT 
	    		pk_prodejce
	    		,prodejce
	    		,adresa
	    		,obec
	    		,telefon
	    		,web
	    		,email
	    		 FROM $this->table_prodejci
	    			ORDER BY obec,prodejce";
	    
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute();
	    $data = $result->fetchall_assoc();
	    return $data;;
}
}
?>


