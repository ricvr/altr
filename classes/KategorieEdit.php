<?php 
/**
 * @file KategorieEdit.php 
 *
 *  \brief    9 Soubor s třídou __KategorieEdit__
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      29. 8. 2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief __KategorieEdit__ Sprava a editace kategorii, do kterych se zařazuji jednotlivé produkty
 * 
 * \details   Detailnější popis
 */ 
class KategorieEdit{
  private static $nazev;
  private $dbkatg; /**< \brief trida s pristupem do db */
  private $kategorieall; /**< \brief Array se vsemi kategoriemi*/
  private $odsazeni; /** @brief Odszeni kategorie pri zanorovani urovni. obsahuje retezec v zavislosti na hloubce zanoreni*/
  private $idkategorie; /**< \brief aktualne zpracovanvana kategorie */

  public function __construct()
  {
    $this->dbkatg = new KategorieEditDb;
    
    if(isset($_SESSION['user']['pk_uzivatel'])){
    	$this->pk_uzivatel = $_SESSION['user']['pk_uzivatel'];
    }else{
    	$this->pk_uzivatel = 0;
    }
    $this->opravneni = new Opravneni($this->pk_uzivatel,session_id(),0);
    $this->kategorieall = $this->dbkatg->kategorielist();
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{

    echo "<h3>Použita neznámá metoda: ",$funname,"</h3> Volaná s argumenty:";
    var_dump($arg);
    echo "<h3>Volání bez metody</h3>";
	$this->katList();
}
/**
 *  \brief Seznam vsech kategorii - nazev + nadrizena kategorie;
 */
public function katList() {
	$this->opravneni->setIdUloha(IDULOHA_KATEGORIE_SPRAVA);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění ke správě kategorií",10);
	}
	
	echo "<h3>Správa kategorií produktů</h3>";
	$this->katNewKategorie();
	$this->kategorieall = $this->dbkatg->kategorielist();
	//var_dump($this->kategorieall);
	$this->odsazeni = "";
	//foreach ($this->kategorieall as $k){
		//echo "<br>1" . $k['kategorie'];
		//$this->katKategorieDeti($k['id_kategorie'],1);
		echo "<div class='kat-form kat-zahlavi'>";
		echo "<div class='kat-kategorie'>";
		echo "Název kategorie";
		echo "</div>";
		
		echo "<div class='kat-nadrazena'>";
		echo "Nadřazená kategorie";
		echo "</div>";
		echo "<div class='kat-razeni'>";
		echo "Řazení";
		echo "</div>";
		echo "<div class='kat-popis'>";
		echo "Stručný popis";
		echo "</div>";
		echo "<hr class='kat-clearfix' >";
		echo "</div>";
		$this->katKategoriePotomci(1,1);
	//}
}
/**
 *  \brief Pomoci rekurze vyhleda vsechny potomky kategorie, jejiz ID je v parametru.
 *  
 */
private function katKategoriePotomci($idrodic,$counter) {
	$xf=0; // pomocna promena pro pocitani vypsanych formularu
	foreach ($this->kategorieall as $ktg){
		// jde o ROOT, ten se nevypisuje
		if ($ktg['id_rodic'] == 0){
			continue;
		}
		//Nejde-li o dite, jde se na dalsi...
		if($ktg['id_rodic'] != $idrodic)  {
			continue;
		}
		//echo "<br>".$ktg['id_rodic'] ." | ". $idrodic;
		
			//echo $ktg['kategorie'] . " - " . $ktg['id_kategorie'];
			
			echo "<div class='kat-form' >";
			$this->katFormular($ktg,$counter);
			$xf++;
			echo "<hr class='kat-clearfix'style='height:1px;border-width:0;background-color:#d3d3d3;'>";
			echo "</div>";
			$this->katKategoriePotomci($ktg['id_kategorie'],$counter+1);
		
	}
	return;
		
}
/**
 *  \brief Vypise editacni formular pro jednu kategorii.;
 */
private function katFormular($ktg,$level) {
	
	$this->idkategorie = $ktg['id_kategorie'];
	echo "<form action='sprava.php' method='GET'>";
  echo "<input type='hidden' name='mod' value='9'>";
  echo "<input type='hidden' name='met' value='katFormEditHandler'>";
  echo "<input type='hidden' name='idkat' value='".$ktg['id_kategorie']."'>";
  echo "<div class='kat-kategorie' >";
  echo "<a href='sprava.php?mod=9&met=katKategorieDel&idkat=". $this->idkategorie ."&kategorie=". $ktg['kategorie'] ."'>";
  echo "<img src='img/krizek.gif' width='16px;' alt='Odstranění kategorie' title='Odstranění kategorie'>";
  echo "</a>";
  echo " |";
		for($x=1;$x<$level;$x++){
			//echo " &nbsp; &nbsp; ";
			echo "> ";
		}
	echo "<input type='text' name='kategorie' value='" . $ktg['kategorie'] . "' title='Název kategorie' 
	onfocus='document.getElementById(\"tl$this->idkategorie\").style.display = \"inline\";' style=''>";
	//echo $ktg['kategorie'] . " - " . $ktg['id_kategorie'];
	echo "</div>";
	
  echo "<div class='kat-nadrazena' >";
	//echo "Nadřízená kat.";
	echo $this->katRodic($ktg['id_rodic']);
	echo "</div>";
	echo "<input type='text' name='razeni' value='" . $ktg['razeni'] . "' size='4' title='Řazení kategorie' 
	onfocus='document.getElementById(\"tl$this->idkategorie\").style.display = \"inline\";'>";
	echo "<input type='text' name='popis' value='" . $ktg['popis'] . "' size='40' title='Popis kategorie' 
	onfocus='document.getElementById(\"tl$this->idkategorie\").style.display = \"inline\";'>";
	
	echo "<input id='tl$this->idkategorie' type='submit'value='Uložit změny' style='display:none;'>";
	
	echo "</form>";
}
/**
 *  \brief Vytvari SELECT s nabidkou vsech kategoii. Predvybrana bude kategorie jejiz ID je v parametru.
 *  Neni-li zadna vybrana, je nastaven ROOT
 *  
 *  \param int $idrodic ID kategorie, ktera bude predvybrana
 */
private function katRodic($idrodic=0) {
	$retval="";
	$retval .="<select name='idrodic' style='color:#000000;' 
			title='Nadřazená kategorie. Výběrem z nabidky lze změnit zařazení kategorie.' 
			onchange='document.getElementById(\"tl$this->idkategorie\").style.display = \"inline\";'>";
	foreach ($this->kategorieall as $k){
		$retval .="<option value='". $k['id_kategorie'] ."'";
		if($k['id_kategorie']==$idrodic){
			$retval .= " SELECTED ";
		}
		$retval .= " >";
		$retval .=$k['rodic']."> ".$k['kategorie'] ; 
		$retval .="</option>";
		
	}
	$retval .="</select>";
	return $retval;
}
/**
 *  \brief Obsluha formulare pro editaci vlasnosti kategorie;
 */
public function katFormEditHandler($kat) {
	$this->opravneni->setIdUloha(IDULOHA_KATEGORIE_SPRAVA);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění ke změně údajů kategorie",10);
	}
	
	/* 
	 * echo "<h3>Správa kategorií produktů</h3>";
	echo "metoda: <strong>".__FUNCTION__ ."</strong>";
	echo "<br>";
	*/
	if ($kat['idkat']==$kat['idrodic']){
		echo "<div class='varovani'>";
		echo "<span style='font-weight:bold;'>".$kat['kategorie']."</span> - Kategorie nemůže být sama sobě nadřízenou kategorií!";
		echo "</div>";
		$this->katList();
		return;
	}
	/*
	echo "<br>";
	echo "<br>";*/
	//var_dump($kat);
	/*
	echo "<br>";
	echo "<p>Probíhá ukládání....";
	*/
	$this->dbkatg->kategorieupdate($kat);
	echo "<div class='info'>Změny v kategorii <span style='font-weight:bold;'>".$kat['kategorie']."</span> byly uloženy</div>";
	$this->katList();
}
/**
 *  \brief Vytvoreni divu pro formular pro vytvoreni nove kategorii;
 */
private function katNewKategorie() {
	
	?>
	<p>
	<button id="btnshowform"onClick="$('#newkat').show();$('#btnshowform').hide();">Nová kategorie</button>
	</p>
	<div id="newkat" style="display: none;margin:2%;">
	<fieldset style="border:solid 1px #808080;background-color:#a4ffa4;">
	<legend style="background-color:#a4ffa4;">Nová kategorie</legend>
	<form id="fnewkat" action="sprava.php" method="GET">
	<input type="hidden" name="mod" value="9">
	<input type="hidden" name="met" value="katNewKategorieIns">
	Název kategorie<input type="text" name="kategorie" value="" size="15">
	<br/>
	Nadřazená kategorie <?php echo $this->katRodic()?>
	<br/>
	Řazení <input type="text" name="razeni" value="" size="4">
	<br/>
	Stručný popis <input type="text" name="popis" value="" size="40">
	<br/>
	<p><input type="submit" value="Uložit">
	</form>
	</fieldset>
	<br/>
	<button id="btnhideform"onClick="$('#newkat').hide();$('#btnshowform').show();">Zavřít formulář</button>
	<br/>
	</div>
	<?php 
	
}
/**
 *  \brief ;
 */
public function katNewKategorieIns($param) {
	$this->opravneni->setIdUloha(IDULOHA_KATEGORIE_SPRAVA);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění ke vkládání nové kategorie",10);
	}
//	var_dump($param);
	$this->dbkatg->kategorieinsert($param);
	$this->katList();
}
/**
 *  \brief Rizeni odstranění kategorie;
 *  Nejdrive se polozi kontrolni otazka a teprve po jejim potvrezni se zavola metodah katErase pro odstraneni kategorie
 */
public function katKategorieDel($par) {
	$this->opravneni->setIdUloha(IDULOHA_KATEGORIE_SPRAVA);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění k odstranění kategorie",10);
	}
/*Kontrola odpovedi na kontrolni otazku, jestli bylo potvrzeno smazani: del==1
 * Pokud kontrolni otazka nebyla polozena, ke smazani nedoje a polozi se kontrolni otazka
 */
if(isset($par['del']) && $par['del']==1){
	$this->katErase($par['idkat']);
	$this->katList();
	return;
}
//
	echo "<h4>Smazat kategorie</h4>";	
	echo "Přejete si smazat kategorii \"".$par['kategorie'] ."\"?";
	echo "<p>";
	echo "<button onclick=\"window.location.replace('sprava.php?mod=9&met=katKategorieDel&idkat=". $par['idkat']."&del=1')\">";
	echo "Smazat";
	echo "</button> &nbsp;";

	
	echo "<button onclick=\"window.location.replace('sprava.php?mod=9&met=katList')\">";
	echo "Storno";
	echo "</button>";
	echo "</p>";
}
/**
 *  \brief Kontroluje, jestli je mozne smazat kategorii a pokud ano, smaze ji.;
 * Pred smazanim kategorie se musi proves kontrola, jestli v kategorii nejsou zarazene produkty,
 * nebo jestli nema podrizenou kategorii (potomka)
 */
private function katErase($idkat) {
	
	$products = $this->dbkatg->getproducts($idkat);
	$subcategory = $this->dbkatg->getsubcategory($idkat);
	if ((count($products) == 0) & (count($subcategory) == 0)) {
		$this->dbkatg->categoryerase($idkat);
		echo "<div class='info'>";
		echo "Kategorie byla smazána";
		echo "</div>";
		$this->katList();
		return;
	}
	
	if(count($products) > 0){
		echo"<div class='varovani'>Mazaná kategorie obsahuje produkty!</div> Přesuňte níže uvedené produkty do jiné kategorie.";
		echo "<ol>";
		foreach($products as $p){
			echo "<li style=\"list-style-type:decimal;color:#000000;\">".$p['nazev']."</li>";
		}
		echo "</ol>";
	}
	
	if(count($subcategory) > 0){
		echo"<div class='varovani'>Mazaná kategorie obsahuje podřízené kategorie!</div> Přesuňte níže uvedené kategorie pod jinou kategorii.";
		
		echo "<ol>";
		foreach($subcategory as $sc){
			echo "<li style=\"list-style-type:decimal;color:#000000;\">" . $sc['kategorie'] . "</li>";
		}
		echo "</ol>";
		
	}
	
	/*
	 */
}
}
?>


