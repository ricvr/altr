<?php 
/**
 * @file KategorieEditDb.php 
 *
 *  \brief    Soubor pro
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      30.8.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief  __KategorieEditDb__ class pro pristup do databaze k datum potrebnych pro spravu kategorii
 * \details   Detailnější popis
 */ 
class KategorieEditDb{
  private static $nazev;
  private $kat_popis = 200; // velikost pole _kategorie:popis

  public function __construct()
  {
   $this->dbh = DB_Connect::newConnect();
 	 $this->table_kategorie = TABLE_PREFIX . "_kategorie";
   $this->table_produkty = TABLE_PREFIX . "_produkty";
  	 
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{

    echo "<h3>Použita neznámá metoda: ",$funname,"</h3> Volaná s argumenty:";
    var_dump($arg);
    echo "<h3>Volání bez metody</h3>";

} 
/**
 *  \brief Pole se seznamem vsech kategorii;
 */
public function kategorielist() 
{
    $q = "SELECT id_kategorie, id_rodic, uroven, razeni, kategorie, popis
FROM " . $this->table_kategorie . " ORDER BY razeni,id_rodic,kategorie";
    
    $q = "SELECT k.id_kategorie, k.id_rodic, k.uroven, k.razeni, k.kategorie,r.kategorie AS rodic, k.popis
    FROM " . $this->table_kategorie . " k
    LEFT JOIN " . $this->table_kategorie . " r ON k.id_rodic=r.id_kategorie
    ORDER BY id_rodic,razeni,kategorie";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute();
    $data = $result->fetchall_assoc();
    return $data;	
}
/**
 *  \brief Ulozi nove udaje do tabulky _kategorie;
 */
public function kategorieupdate($kat) {
	$kat['popis']=substr($kat['popis'],0,$this->kat_popis);// zkopiruje se jen povoleny pocet znaku
	    $q = "UPDATE ". $this->table_kategorie ." SET
	    		kategorie = :1
	    		,id_rodic = :2
	    		,razeni =  :3
	    		,popis = :5
	    		WHERE id_kategorie = :4
	    		";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute($kat['kategorie'],$kat['idrodic'],$kat['razeni'],$kat['idkat'],$kat['popis']);
	    
	    return;
}
/**
 *  \brief Provede ulozeni nove kategori;
 */
public function kategorieinsert($kat) {
	$kat['popis']=substr($kat['popis'],0,$this->kat_popis);// zkopiruje se jen povoleny pocet znaku
		    $q = "INSERT INTO altr_kategorie(id_rodic, razeni, kategorie, popis) 
	    		VALUES (:1,:2,:3,:4) ";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute($kat['idrodic'],$kat['razeni'],$kat['kategorie'],$kat['popis']);
	    
	    return ;
}
/**
 *  \brief Vraci pocet produktu zarazenych primo v kategorii;
 *
 *  \param int $idkat ID kategorie, ktera se kontroluje
 *  \return array Seznam produktu v kategorii
 */
public function getproducts($idkat) {

	
	    $q = "SELECT pk_produkt,id_kategorie,nazev FROM ".$this->table_produkty ."
	    		 WHERE id_kategorie = :1";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute($idkat);
	    $data = $result->fetchall_assoc();
	    return $data;
}
/**
 *  \brief Vraci pocet podrizenych kategorii;
 *  \param int $idkat ID kategorie, ktera se kontroluje
 *  \return int Pocet subkategorii v kategorii
 */
public function getsubcategory($idkat) {
	
	    $q = "SELECT id_kategorie,id_rodic,kategorie FROM ".$this->table_kategorie ."
	    		WHERE id_rodic = :1";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute($idkat);
	    $data = $result->fetchall_assoc();
	    return $data;

}
/**
 *  \brief smaze kategorii z tabulky _kategorie;
 */
public function categoryerase($idkat) {
	    $q = "DELETE FROM ". $this->table_kategorie ." WHERE id_kategorie = :1";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute($idkat);
	    
	    return;
}
}
?>


