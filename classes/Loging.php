<?php 
/**
 * @file Loging.php Soubor s class Loging pro logovani zprav a ukladani iformaci
 * 
 * Obsahuje pouze definici class Loging a definici interface iLoging
 *
 * @version 0.1
 * @author RiC
 *
 */ 

/**
 * \brief Interfejs pro tridu loging.
 * \details Obsahuje deklarace verejnych metod, ktere jsou nasledne definovany v prislusne dride
 */ 
interface iLoging
{
  public function writeFile(); /**< Zapis chybove zpravy do souboru */ 
  public function writeDb(); /**< Zapis zpravy do databaze */
  public function setLogCode();  /**< Seter pro zapsani kodu chyby do slotu */ 
  public function setLogMessage();  /**< Nastaveni zpravy do slotu */ 
  public function setLogFile(); /**< Seter pro nastaveni souboru, kde vznikla zprava*/ 
  public function setLogLine(); /**< SETer pro nastaveni cisla radku, kde doslo ke zprave*/ 
}


// include_once "Options.php";

/**
 * \brief Class Loging pro zapis logu
 * \details Poutrebuje tridu \c Options s nastavenimi
 *
 * Pouziti:
 *
 * \code
  $log = new Loging(int $kodchyby,string $zprava, string $soubor, int $radek);
  $log->setFile('errordb.log');
  $log->writeDb();
  \endcode
 * @todo zobrazovani ulozenych zprav
 */ 
class Loging implements iLoging
{
  private $logmessage = ""; /**< Zprava, ktera ma byt ulozena do db|souboru */
  private $logfile = ""; 
  private $logline = ""; 
  private $logcode = ""; 
  private $file; /**< \brief slot pro ulozeni nazvu souboru pro zapis zprav */
  private $dbtable; /**< \brief tabulka pro ulozeni logovacich zprav */

  public function __construct($logcode=0,$message="",$logfile="",$logline=0)
  {
    $this->setLogCode($logcode);
    $this->setLogMessage($message);
    $this->setLogFile($logfile);
    $this->setLogLine($logline);
    $this->file = Options::$logFile;
    $this->dbtable = Options::$logTable; 
  }
  /**
   * nastaveni slotu 'logmessage'
   */ 
  public function setLogMessage($logmessage="")
  {
    $this->logmessage = $logmessage;
  }

  /**
   * nastaveni slotu 'logfile'
   */ 
  public function setLogFile($logfile="")
  {
    $this->logfile = $logfile;
  }

  /**
   * Nastaveni slotu 'logcode'
   */ 
  public function setLogCode($logcode=0)
  {
    $this->logcode = $logcode;
  }

  /**
   * Nastaveni slotu 'logline'
   */ 
  public function setLogLine($logline=0)
  {
    $this->logline = $logline;
  }

  /**
    * Nastavi hodnotu slotu $file
    * @param string $file nazev souboru 
    * @return object instance class 
   */
  public function setFile($file)
  {
    $this->file = $file;
    return $this;
  }

  /**
   * Zapis chyboveho hlaseni do souboru.
   *
   * Chybova zprava ulozena v $this->message bude ulozena do souboru,
   * ktery je v $this->file
   */  
  public function writeFile()
  {
    //$time = date('Y-m-d H:i:s,u',time());
    $dt = date_create();
    //var_dump($dt);
    //$time = $dt->format('Y-m-d H:i:s,u');
    $time = date_format($dt,'Y-m-d H:i:s');
    //$date = $dt->format(YY '-' MM '-' DD);
    //$time = $dt->format('t'? HH[:]MM[:] II frac);
    //var_dump($this->message);
    $handFile = fopen($this->file,'a+');
    $written = fwrite($handFile,"\n" . $time . " - " . $this->logfile . ', r.' . $this->logline . " || " . $this->logmessage);
    if(!$written){
      echo "<p>Nepodařilo se zapsat do souboru ",$this->file;
    }
    return $this;
  }
/**
 * Zapisuje chybovou zpravu do databaze
 *
 * Zapise hlasku do tabulky, v pripade, ze to skonci chybou zapise zpravu do souboru 
 *  i s upozornenim,  ze se nepodaril zapis do databaze
  * @param void  
  * @return $this 
  */
public function writeDb()
{
  try{
    $dbh = DB_Connect::newConnect(); 
    $q = "INSERT INTO " . $this->dbtable . " (code,message,file,line) 
      VALUES(" . $this->logcode . "
      ,\"" . $this->logmessage . "\"
      ,'" . str_replace("\\","\\\\",$this->logfile) . "'
      ," . $this->logline . ")";
    //echo "<hr><p>Ukladani do DATABAZE:<br> $q</p><hr>";
    $stmt = $dbh->prepare($q); 
    $result = $stmt->execute(); //provedeni dotazu 
  }catch(Exception $e){
    //$this->writeFile();
    $this->setLogMessage("Chyba db:" . $e->getMessage() . '||' . $this->logmessage);
    $this->writeFile();
  }
}
} 
?>

