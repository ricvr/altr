<?php 
/**
 * @file MenuKategorie.php
 *
 *  \brief    22 - class __MenuKategorie__ Stručný popis souboru
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief 22 - __MenuKategorie__ Vytvari menu z kategorii do kterych jsou zarazeny produkty
 * \details   Detailnější popis
 */ 
class MenuKategorie{
  private $dbIdx;

  public function __construct()
  {
    $this->dbIdx = new IndexDb();
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
  echo "<h3>Použita neznámá metoda: $funname",$arg[0],"</h3>";

}  
/**
  *  \brief Vraci polozky z jedne urovne. ID rodice teto urevne je parametrem funkce
  *  @param int $id ID rodice jehoz "deti"(polzky) budou vracen
  *  @return array Zaznamy z tab. _kategorie
  */
public function mkatPolozky($idrodic=1)
{ // BEGIN function
  //var_dump($idrodic);
  $kategorie = $this->dbIdx->kategorierodice($idrodic);
  //var_dump($kategorie);
  foreach($kategorie as $kat){
		echo "<a href='index.php?mod=23&amp;met=spshow&amp;kat=".$kat['kategorie']."&amp;idkat=".$kat['id_kategorie']."' title=\"".$kat['popis']."\" class='menubutton'>";
    echo $kat['kategorie'];
		echo "</a>";
  }
} // END function
}
?>


