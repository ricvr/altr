<?php 
/**
 * @file Obrazky.php
 *
 *  \brief    7 - Soubor s class pro praci s obrazky
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief 7 - Zpracovani obrazku produktu. Upload,...
 * \details   Detailnější popis
 */ 
class Obrazky{
 
  private $obrdb;
  private $idprodukt;

  public function __construct()
  {
    $this->obrdb = new ObrazkyDb();
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
  echo "<h3>Použita neznámá metoda: $funname</h3>";
  echo "Argumenty:<br>";
  var_dump($arg);

}

public function setIdProduct($val)
{ // BEGIN function
	$this->idprodukt = $val;
} // END function


/**
  *  \brief Zajistuje upload obrazku z formulre
  *
  * $_FILES[0]["obrFile]=> array(5) { name; type; tmp_name; error; size;}
  * Kody chyb pri uploadu
  *http://php.net/manual/en/features.file-upload.errors.php
  *  @param typ popis
  * $id; $idprodukt; $MAX_FILE_SIZE; $action;
  *  @return string vystup
  */
public function obrUpload($in)
{ // BEGIN function
	//var_dump($in);
  //echo "<br><br>";
  //var_dump($_FILES);
  // Kontrola velikosti souboru

/*
if($_FILES['obrFile']['error'] == 1){
  echo "<p>Příliš velký soubor ".$_FILES['obrFile']['name']."</p>";
  //header("Location:sprava.php?mod=6&met=prodEdit&id=".$idprodukt);

  return;
} elseif($_FILES['obrFile']['error'] > 0){
  echo "<p>Chyba při uploadu souboru.Kód chyby: " . $_FILES['obrFile']['error'] . "</p>";
  return;
} elseif($_FILES['obrFile']['size'] > MAX_VELIKOST_OBRAZKU){
    echo "<P>Obrazek je větší než povolených ".MAX_VELIKOST_OBRAZKU . " B;";
    return;
}
*/

$url_zpet =  "sprava.php?mod=6&met=prodEdit&id=".$in['idprodukt'];
$link_zpet =  "<p><a href='$url_zpet'>Zpět</a>";

/*
  Osetreni chyb pri uploadu obrazku
*/
if($_FILES['obrFile']['error'] > 0){ // je-li nejaka chyba, zpracuje se
  switch($_FILES['obrFile']['error']){
    case 1:
      echo "<p>Soubor s obrázkem je větší než serverem povolených  ".ini_get("upload_max_filesize")
      . "&nbsp;bytů (nasataveni 'upload_max_filesize' v php.ini).";
      //return;
      //break;
    case 2:
      echo "<p>Soubor s obrázkem je větší než ".MAX_VELIKOST_OBRAZKU . "&nbsp;bytů;";
      //return;
      //break;
    default:
      echo "<p>Chyba při uploadu souboru. Kód chyby: " . $_FILES['obrFile']['error'] . "</p>";
      echo $link_zpet;
      return;
  }
}

  // Kontrola správného typu souboru (povoleny pouze JPG a PNG)
  $obrFile=str_replace(";", "", $_FILES['obrFile']['tmp_name']);
  $imageSize = getImageSize($obrFile);

//   echo "<p>ImageSize:  ";
//   var_dump($imageSize);

  $typ_file=$imageSize['mime']; //"image/jpeg";

  switch($imageSize[2]){
  case IMAGETYPE_JPEG: //2 IMAGETYPE_JPEG
    $obr=imagecreatefromjpeg($_FILES['obrFile']['tmp_name']);
    break;
  case IMAGETYPE_PNG: // 3 IMAGETYPE_PNG
    $obr=imagecreatefrompng($_FILES['obrFile']['tmp_name']);
    break;
  default:
    echo "<p>Nepovolený typ souboru";
    echo $link_zpet;
    return;
  }


  // Ze stavajicich rozmeru spocitat nove velikosti pro obrazky  'maly' a 'stred'
  $sirka=imagesx($obr); // Zjistime puvodni rozmery obrazku
  $vyska=imagesy($obr);

  $maly_vyska = $vyska/($sirka/MALY_SIRKA); // vypocet nove vysky pro maly obrazek
  $stred_vyska = $vyska/($sirka/STRED_SIRKA); // vypocet nove vysky pro stredni obrazek


  //Vytvoreni noveho prazdneho obrazku s TrueColor - kvuli zachovani kvality
  $obr_maly =imagecreatetruecolor (MALY_SIRKA , $maly_vyska );
  $obr_stred =imagecreatetruecolor (STRED_SIRKA , $stred_vyska );


  /* Provedeni zmenseni obrazku - Resampling
   *  zmenseni obrazek se ulozi do $obr_maly | §obr_stred
   */
 imagecopyresampled ($obr_maly , $obr , 0 , 0 , 0 , 0 , MALY_SIRKA , $maly_vyska ,$sirka ,$vyska );
 imagecopyresampled ($obr_stred , $obr , 0 , 0 , 0 , 0 , STRED_SIRKA , $stred_vyska ,$sirka ,$vyska );

  /*
   * Nacteni noveho obrazku do bufferu a z buferu do docasneho souboru
   */
  ob_start();
  ImageJPEG($obr_maly); // Obrazek se odesle do bufferu
  $buff_maly = ob_get_contents(); // z bufferu se nacte do promenne
  ob_clean();   // vycisteni bufferu
  ImageJPEG($obr_stred); // Obrazek se odesle do bufferu
  $buff_stred = ob_get_contents(); // z bufferu se nacte do promenne
  ob_end_clean();   // uzavreni bufferu

  ImageDestroy($obr_maly);
  ImageDestroy($obr_stred);
/*
   $tmpf_maly = tempnam(sys_get_temp_dir(),"m"); // vytvoreni docasneho soubour, zacina pismenem 'm''
   $tmpf_stred = tempnam(sys_get_temp_dir(),"s"); // vytvoreni docasneho soubour, zacina pismenem 's''
*/
  $tmpf_maly = tempnam("tmp","m"); // vytvoreni docasneho soubour, zacina pismenem 'm'' adr. 'tmp' musi existovat
  $tmpf_stred = tempnam("tmp","s"); // vytvoreni docasneho soubour, zacina pismenem 's''

  if(LADENI){
  	echo "<br>TMP soubor maly: $tmpf_maly";
  	echo "<br>TMP soubor stred: $tmpf_stred";
  }
  $h_maly = fopen($tmpf_maly,"w");   // otevreni souboru pro zapis
  $h_stred = fopen($tmpf_stred,"w");   // otevreni souboru pro zapis

  $size_maly = fwrite($h_maly, $buff_maly);   // do docasneho souboru se zapise obrazek
  fclose($h_maly);

  $size_stred = fwrite($h_stred, $buff_stred);   // do docasneho souboru se zapise obrazek
  fclose($h_stred);

//   echo "<br>Velikosti MALY: $size_maly";
//   echo "<br>Velikosti STRED: $size_stred";
  /*
   * Z docasneho souboru se nacte do promenne a odtud se ulozi do db
  */
  $in_maly = addslashes(fread(fopen($tmpf_maly,"r"),$size_maly));
  $in_stred = addslashes(fread(fopen($tmpf_stred,"r"),$size_stred));

  $obr_nazev = $_FILES['obrFile']['name'];
  //$obrdb =  new ObrazkyDb();
  /*
  Osetreni nazvu souboru. Nejdrive najdu pozici poslední tecky (=urceni zacatku pripony)
  Zjistim delku CELEHO nazvu vcetene pripony a nasledne urcim jen nazev bez pripony.
  Nakonec zjistim priponu.
  Pomoci 'friendlyUrl()' vycistim nazev od pripadny pokusuu o utok :-)
  */

/*
refaktor --> obrNazevSouboru()
  $kodovani = ini_get("default_charset");
  $pozice_tecky = iconv_strrpos ( $_FILES['obrFile']['name'] , "." ,$kodovani );
  $delka_nazvu = iconv_strlen ($_FILES['obrFile']['name'] , $kodovani );
  $nazev = iconv_substr($_FILES['obrFile']['name'],0,$pozice_tecky,$kodovani);
  $pripona = iconv_substr($_FILES['obrFile']['name'],$pozice_tecky+1, $delka_nazvu-$pozice_tecky-1,$kodovani);

  $nazev_souboru = $this->friendlyUrl($nazev) . "." . $pripona;
*/
  $nazev_souboru = $this->obrNazevSouboru();
  $this->obrdb->obr_maly = $in_maly;
  $this->obrdb->obr_stred = $in_stred;
  $this->obrdb->obr_nazev = $nazev_souboru; //$obr_nazev;
  $this->obrdb->obr_typ = $typ_file;
  $this->obrdb->maly_velikost = $size_maly;
  $this->obrdb->maly_sirka = MALY_SIRKA;
  $this->obrdb->maly_vyska = $maly_vyska;
  $this->obrdb->stred_velikost = $size_stred;
  $this->obrdb->stred_sirka = STRED_SIRKA;
  $this->obrdb->stred_vyska = $stred_vyska;
  $this->obrdb->idprodukt = $in['idprodukt'];

  $this->obrdb->obrazek_ins();


//   echo "<br>".$_FILES['obrFile']['name'] . " - ".$nazev." / ".$pripona;

/*
  $cil = "products". DIRECTORY_SEPARATOR ."images". DIRECTORY_SEPARATOR
      .$in['idprodukt']."_" . $this->friendlyUrl($nazev) . "." . $pripona;
*/
  $cil = PRODUCT_IMAGES_DIR . $in['idprodukt']."_" . $nazev_souboru;


  move_uploaded_file($_FILES['obrFile']['tmp_name'],$cil);

  header('Location: '.$url_zpet);

} // END function

/**
  *  \brief Vraci nazev souboru, ktery bude pouzit do tabulky v db i pro soubor
  *  @param typ popis
  *  @return string vystup
  */
private function obrNazevSouboru()
{ // BEGIN function
  $kodovani = ini_get("default_charset");
  $pozice_tecky = iconv_strrpos ( $_FILES['obrFile']['name'] , "." ,$kodovani );
  $delka_nazvu = iconv_strlen ($_FILES['obrFile']['name'] , $kodovani );
  $nazev = iconv_substr($_FILES['obrFile']['name'],0,$pozice_tecky,$kodovani);
  $pripona = iconv_substr($_FILES['obrFile']['name'],$pozice_tecky+1, $delka_nazvu-$pozice_tecky-1,$kodovani);

  return $this->friendlyUrl($nazev) . "." . $pripona;

} // END function
/**
  *  \brief Odstrani zaznam s obrazkem z db "_obrazky"
  *  @param int $idObr ID obrazku, ktery se maze
  *  @return string vystup
  */
public function obrSmazat($idObr)
{ // BEGIN function
  echo "Mazaní obrázku";
  $this->obrdb->idobrazek = $idObr;
  $this->obrSmazatSoubor();
  $this->obrdb->obrazek_del();
} // END function

/**
  *  \brief 
  *  @param typ popis
  *  @return string vystup
  */
private function obrSmazatSoubor()
{ // BEGIN function
  $obr_info = $this->obrdb->obrazek_info();
  //var_dump($obr_info);
  $soubor = $obr_info['fk_produkt']. "_".$obr_info['nazev'];
  //echo "<br>Soubor: ",$soubor;
  unlink(PRODUCT_IMAGES_DIR . $soubor);
} // END function
/** Vytvoření přátelského URL. Tip #400 z knihy J.Vrány '1001 tipů a triků pro PHP'
* @param string řetězec v UTF-8, ze kterého se má vytvořit URL
* @return string řetězec obsahující čísla, znaky bez diakritiky, _ a -
*/
private function friendlyUrl($title) {
	$url = $title;
	$url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
	$url = trim($url, "-");
	$url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
	$url = strToLower($url);
	$url = preg_replace('~[^-a-z0-9_]+~', '', $url);
	return $url;
}
}
?>


