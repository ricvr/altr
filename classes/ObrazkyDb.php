<?php 
/**
 * @file ObrazkyDb.php
 *
 *  \brief    Prace s obrazky a s db
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo Po uložení obrázku odstranit dočasné soubory
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief Sprava obrazku v databazi
 * \details   Detailnější popis
 */ 
class ObrazkyDb{
  public $obr_maly; /**< */
  public $obr_stred; /**< */
  public $obr_nazev; /**< */
  public $obr_typ; /**< \brief Typ obrazku */
  public $maly_velikost; /**< \brief Velikost maleho obrazku */
  public $maly_sirka;
  public $maly_vyska;
  public $stred_velikost; /**< \brief Velikost stredniho obrazku */
  public $stred_sirka;
  public $stred_vyska;
  public $idprodukt; /**< \brief ID produktu, ke kteremu obrazek patri */
  public $idobrazek; /**< \brief PK obrazku, se kterym se pracuje */



  public function __construct()
  {
    $this->dbh = DB_Connect::newConnect();
    $this->table_obrazky = TABLE_PREFIX . "_obrazky";

  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
  echo "<h3>Použita neznámá metoda: $funname",$arg[0],"</h3>";

}  
/**
  *  \brief Provede vlozeni zmensenin obrazku do tab. _obrazky
  *  @param typ popis
  *  @return string vystup
  */
public function obrazek_ins()
{ // BEGIN function
  $q = "INSERT INTO ". $this->table_obrazky ."(fk_produkt,nazev,typ
          ,obr_maly,maly_vyska,maly_sirka,maly_velikost
          ,obr_stred,stred_vyska,stred_sirka,stred_velikost
          ) VALUES(
          ".$this->idprodukt."
          ,'".$this->obr_nazev."'
          ,'".$this->obr_typ."'
          ,'".$this->obr_maly."'
          ,'".$this->maly_vyska."'
          ,'".$this->maly_sirka."'
          ,'".$this->maly_velikost."'
          ,'".$this->obr_stred."'
          ,'".$this->stred_vyska."'
          ,'".$this->stred_sirka."'
          ,'".$this->stred_velikost."'
          )";
  $stmt = $this->dbh->prepare($q);
  $result = $stmt->execute();
  if(!$result){
    echo "<p>Nepodařilo se uložit obrazek";
  }
} // END function

/**
  *  \brief 
  *  @param typ popis
  *  @return string vystup
  */
public function obrazek_del()
{ // BEGIN function
    $q = "DELETE FROM ". $this->table_obrazky ." WHERE pk_obrazek=:1";
    echo "<br>$q <br>".$this->idobrazek ;
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($this->idobrazek);
    return;
} // END function


/**
  *  \brief Vraci zakladni info o obrazku z tab. "_obrazky"
  *  @param typ popis
  *  @return array fk_produkt,nazev,typ,pk_obrazek
  */
public function obrazek_info()
{ // BEGIN function
    $q = "SELECT fk_produkt,nazev,typ,pk_obrazek
          FROM ".$this->table_obrazky ." WHERE pk_obrazek= :1";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($this->idobrazek);
    $data = $result->fetchall_assoc();
    return $data[0];
} // END function

/**
  *  \brief Seznam obrazku, ktere se vazou ke konkretnimu produktu
  *  @param int $this->idprodukt
  *  @return string vystup
  */
public function obrazkyproduktu()
{ // BEGIN function
    $q = "SELECT pk_obrazek FROM ".$this->table_obrazky ." WHERE fk_produkt= :1";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($this->idprodukt);
    $data = $result->fetchall_assoc();
    return $data;
} // END function
}
?>


