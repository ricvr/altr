<?php 
/**
 * @file Opravneni.php 
 *
 *  \brief    Deklarace tridy \c Opravneni
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      17.12.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo Zachytit vyjimky pri ukladani do db 
 *  \todo Kontrola ččasového limitu od poslední akce
 *  \todo Kontrola oprávnění k provedení úlohy
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief Kontrola oprávnění k úlohám, kontrola časového limitu od poslení akce, ukládání dat do db
 * \details   Při vytváření instance se zadává PK uživatele, ID session, ID úlohy
 * @param int PK uživatele
 * @param string ID session
 * @param int ID úlohy
 */ 
class Opravneni{
  private $pk_uzivatel;
  private $id_session;
  private $id_ulaha;
  private $dbh; /**< Připojení do db */
  private $table_opravneni;
  private $table_prihlaseni;

  public function __construct($pk_uziv=0,$id_sess="",$id_uloha)
  {
    $this->dbh = DB_Connect::newConnect();
    $this->pk_uzivatel = $pk_uziv;
    $this->id_session = $id_sess;
    $this->setIdUloha($id_uloha);
    $this->table_opravneni = TABLE_PREFIX . "_opravneni";
    $this->table_prihlaseni = TABLE_PREFIX . "_prihlaseni";
  }
/**
  * \brief  SETer pro nastaveni hodnoty slotu.
  * \details
  * @param int ID ulohy
  * @return
 */
public function setIdUloha($iduloha)
{ // BEGIN
  $this->iduloha = $iduloha;
} // END function setIdUloha
/**
  * \brief Uklada data kontrole pristupu k ruznym uloham do tabulky \c prf_prihlaseni
  * \details Ukláda ID úlohy, ID session a PK uživatele. 
  * Do pole \c status uklada vysledek kontroly: 1=OK; -1=Zamítnuto.
  * Ukládá také čas ve formátu UNIX Timestamp
  * \enddetails
  * @param typ popis 
  * @return string vystup 
 */
  public function ulozprihlaseni($status=0)
  {
   
    $q = "INSERT INTO " . $this->table_prihlaseni . "
	        (fk_uzivatel,id_uloha,id_session,status,d_akce)
          VALUES(". $this->pk_uzivatel."
            ,$this->iduloha
            ,'".$this->id_session."'
            ,".$status."
            ,".Time().")
            ";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute();
    
  }
/**
  * \brief Ověřuje, jestli uživatel má právo na spuštění dané ulohy
  * \details Načítá údaje z tab. \c prf_opravneni a pokud najde pk uživatele i s ID ulohy, vrátí '1' jako, že má oprávnění.
  * Nekontroluje se timeout pro nečinost.
  * \enddetails
  *
  * @param žádné parametry.
  * @return int 1 - ma pristup k uloze
  * @return int -1 - nema pristup k uloze
 */
public function kontrola_prav()
{ // BEGIN
  if($this->pk_uzivatel <= 0 || $this->pk_uzivatel == ""){
    return -1; /* neni zadan PK ==> neni opravneni */
  }
  $q = "SELECT count(*) as pocet
        FROM " . $this->table_opravneni . "
        WHERE fk_uzivatel = " . $this->pk_uzivatel . "
              AND iduloha = " . $this->iduloha;
// echo "<p>$q </p>";
  //$dbh = DB_Connect::newConnect();
  $stmt = $this->dbh->prepare($q);
  $result = $stmt->execute();
  $data = $result->fetchall_assoc();
  //var_dump($data);
  if($data[0]['pocet']>0){
    $opravneni = 1;
  }else{
    $opravneni = -1;
  }
  $this->ulozprihlaseni($opravneni);
  return $opravneni;
} // END kontrola_prav
/**
  * \brief Kontrola, jestli uživatel nebyl moc dlouho NEAKTIVNI
  * \details Nacte time posledni akce a porovna domu, ktera od ni uplynula s \c Options::$timeout.
  *
  * @param
  * @return int 0 Timeout nevypršel
  * @return int -1 Timeout vypršel
 */
public function kontrola_timeout()
{ // BEGIN
  /* jestli neni nastaveno pk_uzivatele neni overen */
  if(!isset( $this->pk_uzivatel)|| $this->pk_uzivatel=="" ||  $this->pk_uzivatel<=0){
    return -1;
  }
  $q = "SELECT max(d_akce) as d_akce
        FROM ". $this->table_prihlaseni ." 
        WHERE fk_uzivatel= " . $this->pk_uzivatel . "
          AND id_session='" . $this->id_session . "'";
//   echo "$q";
  $stmt = $this->dbh->prepare($q);
  $result = $stmt->execute();
  $vystup = $result->fetch();
  $time_posledni = $vystup->d_akce;
  $time_necinnost= Time()-$time_posledni;
  if($time_necinnost <= Options::$timeout){
    return 0;
  }else{
    return -1;
  }
} // END function 
}
?>


