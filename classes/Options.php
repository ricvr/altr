<?php 
/**
 * @file Options.php
 * \brief Soubor s definici staticke tridy 'Options', ktera slouzi k nastaveni promnenych aplikace
 */ 
/**
 * \brief Trida se statickymi sloty s nastavenim programu
 * \details Trida 'Options' má vsechny vlastnosti|sloty jsou staticke. Slouzi k definovani vlastnosti platnych pro celou aplikaci
 *
 */ 
class Options
{
   public static $logFile = "error.log"; /**< \brief Nazev souboru kam se zapisuji chyby */
   public static $logTable = "altr_login"; /**< \brief Nazev tabulky v databazi, kam se zapisuji zpravy|chybove hlasky apod */
   public static $timeout = '1200'; /**< \brief Delka necinnosti v sekundach. Po uplynuti teto doby bude uzivatel odhlášen */
   
   /** \brief Pole s kódy a s názvy jednotlivých statusů. */
   public static $arrStatusy = array(
        2 => "Blokovaný"
    ,   8 => "Registrovaný"
    ,  32 => "Ověřený"
    , 128 => "Vedoucí"
    , 512 => "Správce"
    ,2048 => "Admin"
  );
}
?> 
