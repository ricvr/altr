<?php 
/**
 * @file Pages.php
 *
 *  \brief    25 - Zobrazeni "statických" stránek
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      27. 6. 2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo  
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief 25 - Zobrazeni "statických" stránek
 * \details   Umožňuje zobrazovat stranky vytvořené přímo v html a uložené v db.
 */ 
class Pages{
  
  public function __construct()
  {
    
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy  s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{

    //echo "<h3>Použita neznámá metoda: ",$funname,"</h3> Volaná s argumenty:";
    //var_dump($arg);
    //extract($arg);
    $page= $arg[0]['page'];
    
    
    switch ($page) {
    	case "altra":
    			$this->pgAltra();
    			break;
    	case "technologie":
    			$this->pgTechnologie();
   				break;
    	case "prodejci":
    			$this->pgProdejci();
   				break;
   		case "kontakty":
    			$this->pgKontakty();
  				break;
  		case "pruvodce":
    			$this->pgPruvodce();
  				break;
    				
    	default:
    		header("Location:index.php");
    	break;
    }

}  
/**
 * @brief 
 *
 * @param type name
 *
 * @return
 */
private function pgAltra() 
{
	//echo "Altra";
	?>
	<p>
	<span class="tucne">ALTRA</span> byla založena nadšenci, kteří se odhodlali k výrobě běžeckých bot se správnými  biomechanickými vlastnostmi blízkými přirozenému způsobu běhu.
</p>
<p>
Běžec myslí jinak. Běh vyčistí mysl, omlazuje tělo a uvolní duši. Víme to, protože my celý život běháme i závodíme na distancích od 100 metrů do 100 mil. Přesto jsme však během těch dlouhých let běhání nepoznali  správnou rovnováhu mezi tělesnými dispozicemi, technikou běhu a přírodou, ve které se běhá a  po více jak 30-ti letech v oboru jsme se rozhodli nabídnout běžcům zcela jinou konstrukci běžecké boty.
</p>
<p>
<span class="tucne">ALTRA</span> byla původně navržena s cílem umožnit  <span class="tucne kurziva">změnou techniky běhu</span>  další sportování  lidem s chronickým zraněním kolen a bolestmi zad.
</p>
<div class="citace">
<p>
Před lety, při sledování zákazníků, kteří si kupovali boty v naší běžecké speciálce v <span class="tucne">Rocky Mountains</span> , jsem si povšimnul znepokojujícího trendu běžců došlapovat při nesprávném držení těla a z příliž vysoké výšky , což bylo dané konstrukcí podešve většiny tehdy vyráběných bot. Když jsme zákazníka podrobili testu při běhu naboso, jeho styl se většinou ihned změnil k lepšímu.
</p>
<p>
Po zjištění, že zdrojem problémů je příliž vysoká patní část podešve jsme začali řešit i  důkladné tlumení při velké zátěži, stejně jako poměrně častou nadváhu některých běžců nebo problém správné pozice chodidla během celé fáze běžeckého kroku.
</p>
<p>
Za účelem potlačení těchto nedostatků byla vytvořena speciální platforma  <span class="tucne">Zero Drop</span> ™.
</p>
<p>
Už při prvním  testování  platformy  <span class="tucne">Zero Drop</span>&nbsp;™ byly patrné často neuvěřitelné výsledky 
mezi našimi zaměstnanci i  zákazníky a tato platforma umožnila spoustě lidí návrat ke sportování.
</p>
<p>
Na základě těchto poznatků se ALTRA postupně spojila s nejlepšími vývojáři v oboru, elitními běžci i odborníky na biomechaniku a začala jejich prostřednictvím přenášet tyto moderní poznatky a zkušenosti do nejširší běžecké komunity.
</p>
<p>
<span class="tucne">ALTRA Zero Drop</span>&nbsp;™, to jsou běžecké boty nejenom pro ultramaratonce  nebo triatlonisty, ale především <span class="tucne">boty pro správnou techniku běhu</span>  na jakoukoliv vzdálenost!
</p>
<p>
Tato unikátní konstrukce a design prokazatelně podněcují správnou techniku běhu a snižují rizika, plynoucí z dlouhodobé zátěže jako jsou bolesti zad a zranění kolen.
</p>
<p>
Platforma <span class="tucne">Zero Drop</span>&nbsp;™  také významně stabilizuje všechny fáze běžeckého kroku, zejména pak fázi odrazu …“
</p>
</div>
<p align="right">
…říká jeden ze zakládajících partnerů, pan <span class="tucne">Jeremy Howlett</span>
</p>
<?php 
	;
}
private function pgTechnologie()
{
	echo "Technologie";
	;
}

private function pgProdejci()
{
	echo "<h3>Obchodní partneři	</h3>";
	echo "<div id='prodejci'>";
	
	$prodejci = new Prodejci();
	$prodejci->prdShow();
	echo "</div>";
	
}

private function pgKontakty()
{
	//echo "<h3>Kontakty</h3>";
	echo "<div id='dovozce-kontakty'>";
	echo "<div class='zvyrazneni-hlavni'>";
	echo "Výhradní zastoupení pro Českou republiku a Slovensko:";
	echo "</div>";
	
	echo "<div class='zvyrazneni-vedlejsi'>";
	echo "ALTRA Česká republika";
	echo "</div>";

	echo "Uzavřená 16/7";
	echo "<br>";
	echo "568 02   Svitavy";
	echo "<br>";
	echo "IČO: 47564181";
	echo "<br>";
	echo "tel./fax: +420 461 619 957";
	echo "<br>";
	echo "e-mail: obchod@altrafootwear.cz - objednávky, dotazy, reklamace";
	echo "<br>";
	echo "e-mail: info@altrafootwear.cz";
	
	echo "</div>";
}

private function pgPruvodce()
{
	echo "Běžecký průvodce";
	?>
	
 <!-- 
	<embed width="100%"  name="plugin" id="plugin" 
		src="pdf/2014_Edu%20Booklet_wtestimonials-cz.pdf" 
		type="application/pdf" internalinstanceid="12" 
		title="Běžecký průvodce"style="height:40vh;">
	
	<iframe width="100%"  height="400" 
		src="pdf/2014_Edu%20Booklet_wtestimonials-cz.pdf" 
		type="application/pdf"
		title="Běžecký průvodce"style="height:35vh;"></iframe>
	-->
	<?php 
	//echo "<a class='media' href='pdf/2014_Edu Booklet_wtestimonials-cz.pdf'></a>";
}

}



?>