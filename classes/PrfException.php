<?php
/**
 * \file PrfException.php
 * \brief Soubor s definicí vlastní vyjímky pro potřeby PrcekFitness.cz
 */

/**
 * \brief Vlastní vyjímka pro potřeby aplikace 
 *
 * \details Podle kodu se vyjimka zpracuje
 *  10 - zpráva pro vypsání
 */
class PrfException extends Exception
{
  /*
   * Konstruktor vytvořen jen pro formu, aby tady byl vidět
   */
  public function __construct($message, $code = 0, Exception $previous = null){
    // Je nanejvys vhodne volat puvodni konstruktor
    parent::__construct($message, $code, $previous);
  }
  // custom string representation of object
  public function __toString() {
      return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  }
}
?>
