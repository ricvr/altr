<?php 
/**
 * @file Prihlaseni.php 
 *
 *  \brief    Class \c Prihlaseni : přihlášení uživatele, zpracovani a nastaveni prihlaseneho uzivatele
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.2.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *
 *
 */ 

/**
 * \brief #4 - Formulář pro přihlášení registrovaného uživatele, zpracování a nastavení přihlášeného uživatele
 * \details   
 */ 
class Prihlaseni{
  private static $nazev;
  private  $iduloha_prihlaseni = 4;

  public function __construct()
  {
  }

/**
 * \brief Provede odhlaseni aktulane prihlaseneho uzivatele
 * \details Provadi se to funkci unset($_SESSION['user']);
  * @param void 
  * @return void 
 */
public function prLogOff()
{
  $this->prUvolneniSession();
//   var_dump($msg);
  header('Location: sprava.php');
}
/**
  * \brief Vymaze data v SESSION['user'] a ID session
  * \details
  * @param typ popis
  * @return string vystup
 */
private function prUvolneniSession()
{ // BEGIN
  unset($_SESSION['user']);
  session_regenerate_id();
} // END function
/**
  * \brief Vola se v pripade, ze vyprsi timeout pro necinnost
  * \details
  * @param
  * @return
 */
public function prTimeOut()
{ // BEGIN
  $this->prUvolneniSession();
  return;
	throw new PrfException("V předcházejících " . Options::$timeout . " vteřinách nebyla zaznamenána žádná aktivita.<br>Byli jste odhlášeni.",10);

} // END function prTimeOut
/**
 * @brief Zpracování přihlašovacího formuláře z @c prFormular(). 
 *
 * \details Overuje prihlasovaci jmeno a heslo. 
 * Nacte prava uzivatele a ulozi je do  $_SESSION['user']['prava']
 * \enddetails
 *
 * @param string $nik nik
 * @param string $passw Heslo 
  * @return 
 */
public function prLogin($arg)
{
  echo"<h3 class='nadpis-ulohy'>Ověření přihlašovacích údajů</h3>";
  //var_dump($arg);
  extract($arg, EXTR_PREFIX_SAME, "wddx"); // $nik $passw
  /*
  echo "<br>".$nik;
  echo "<br>".$passw;
  
  $hesloHash = password_hash($passw, PASSWORD_DEFAULT);
  
  echo "<br>Zkušební výpis: " . $hesloHash;
  echo "<br>Prefix: " . TABLE_PREFIX;
  */
  $table_uzivatele = TABLE_PREFIX . "_uzivatele";
  $q = "SELECT pk_uzivatel, nik,heslo, jmeno,prijmeni,muz, verejne_jm,email,status 
        FROM $table_uzivatele
        WHERE nik='" . trim($nik) . "'"; 
        //WHERE nik='" . trim($nik) . "' AND heslo='".$hesloHash."'"; 
	//echo "<br>$q";        
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);  // Priprava dotazu
  $stmt_exec = $stmt->execute();  //  Provedeni dotazu
  $data = $stmt_exec->fetchall_assoc();
  if(count($data)<=0){
    echo"<div class='info'>";
    echo "Chybné přihlašovací jméno.";
    echo"</div>";
    return;
  }
  $user = $data[0];
  extract($user); // z asociovaneho pole se vytvori samostatne promene
  //kontrola spravnosti hesla
  $opr = new Opravneni($pk_uzivatel,session_id(),$this->iduloha_prihlaseni);
  $stavprihlaseni = 0; // inicializace
  //if(TRUE){
  if(password_verify($passw, $heslo)){
    $stavprihlaseni = 1; // uživatel je přihlášeni
    echo "<p>Heslo je správně</p>";
    echo "<br>$nik";
    echo "<br>$heslo";
    echo "<br>$jmeno";
    echo "<br>$prijmeni";
    echo "<br>Zobrazování jména: $verejne_jm";
    echo "<br>Muž: $muz";
    echo "<br>$email";
    $usr = new Uzivatele();
    $prava = $usr->dejPravaUzivatele($pk_uzivatel);// načtení pole s právy a linky
    $_SESSION['user'] = $data[0]; //pk_uzivatel, nik,heslo, jmeno,prijmeni,muz, verejne_jm,email,status
    $_SESSION['user']['prava'] = $prava;
    $opr->ulozprihlaseni($stavprihlaseni);
    //var_dump($_SESSION['user']);
    header('Location: sprava.php');
  }else{
    $stavprihlaseni = -1; // uživatel zadal chybne heslo
    $opr->ulozprihlaseni($stavprihlaseni);
    echo "<p>ŠPATNÉ heslo!</p>";
  }
}
/**
  * \brief Formulář pro přihlášení uživatele
  * \details Formulář je zpracován v @c prLogin
  * @param 
  * @return string vystup 
  */
public function prFormular()
{
echo"<h3 class='nadpis-ulohy'>Přihlášení uživatele</h3>";
?>

<form name='f1'method='POST'action='sprava.php'>
<input type='hidden'name='mod'value='4'>
<input type='hidden'name='met'value='prLogin'>
<fieldset style="width:50%;margin:0 auto;text-align:center;">
<legend>Přihlášení uživatele</legend>
<br>
 <label for='nik'>Přezdívka</label>
<input type="text"name="nik"size="20" autofocus id="nik"/>
<br>
 <label for='passw'>Heslo</label>
<input type="password"name="passw"size="20"id="passw"/>
<p align="center">
<input type="submit" value="Přihlásit"/>
</p>
</fieldset>
</form>
<?php
}
}
?>


