<?php 
/**
 * @file Prodejci.php
 *
 *  \brief    8 Soubor se spravou seznamu obchodnich partneru a prodejcu
 *   * \details   Detailnější popis
 *       
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      29. 6. 2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo  
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief  8 Sprava seznamu partnerů/prodejců
 * \details   Detailnější popis
 */ 
class Prodejci{
	private $dbProd;
  
  public function __construct()
  {
  	//$this->dbIdx = new IndexDb();
  	$this->dbProd = new ProdejciDb();
  	
  	if(isset($_SESSION['user']['pk_uzivatel'])){
  		$this->pk_uzivatel = $_SESSION['user']['pk_uzivatel'];
  	}else{
  		$this->pk_uzivatel = 0;
  	}
  	$this->opravneni = new Opravneni($this->pk_uzivatel,session_id(),0);
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy  s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
	if(empty($funname)){
    $this->prdList();
	}else{
    echo "<h3>Použita neznámá metoda: ",$funname,"</h3> Volaná s argumenty:";
		var_dump($arg);
	}
   // echo "<h3>Volání bez metody</h3>";

}  
/**
 * @brief Zobrazi seznam prodejců ve verejne prezentaci. Zobrazeni je v dlazdicich
 * 
 * Struktura array s udaji o prodejci
 * pk_prodejce
 * prodejce
 * adresa
 * obec
 * telefon
 * web
 * email
 * 
 * @param type name
 *
 * @return
 */
public function prdShow() 
{
	$data = $this->dbProd->prodejci();
	//var_dump($data);
	foreach ($data as $prodejce){
		echo "<div class='prodejce'>";
		echo "<div class='prodejce-nazev'>".$prodejce['prodejce']."</div>";
		echo "<div class='prodejce-adresa'>".$prodejce['adresa']."</div>";
		echo "<div class='prodejce-obec'>".$prodejce['obec']."</div>";
		echo "<div class='prodejce-telefon'>".$prodejce['telefon']."</div>";
		echo "<div class='prodejce-web'>".$prodejce['web']."</div>";
		echo "<div class='prodejce-email'>".$prodejce['email']."</div>";
		echo "</div>";
		
	}
}
/**
 * @brief Zobrazi seznam prodejcu z tabulky \c _prodejci 
 *
 * @return
 */
public function prdList() 
{
	$data = $this->dbProd->prodejci();
	//var_dump($data);
	$this->prdNovy();
	echo "<h3> Seznam obchodních partnerů a prodejců</h3>";
	
	/*
	 * Zahlavi seznamu
	 */
		echo "<div class='prodejce zahlavi'>";
		echo "<div class='prodejce-ikony'>";
		//   echo "<img src='img/krizek.gif' width='16'> &nbsp; ";
		//echo "<a href='$url_edit".$prodejce['pk_prodejce']."'>";
		//echo "<img src='img/tuzka.gif' width='20' title='$napoveda_edit'></a>";
		echo "&nbsp;";
		echo "</div>";
		
		echo "<div class='prodejce-obec'>Obec</div>";
		echo "<div class='prodejce-nazev'>Prodejce</div>";
		echo "<div class='prodejce-adresa'>Adresa</div>";
		echo "<div class='prodejce-telefon'>Telefon</div>";
		echo "<div class='prodejce-email'>E-mail</div>";
		echo "<div class='prodejce-web'>WWW stránky prodejce</div>";
		echo "</div>";
	
	$url_edit = "sprava.php?mod=8&met=prdEdit&id=";
	$napoveda_edit = "Editace údajů prodejce";
	$counter = 0;
	$zvyrazneni = "";
	
	/*
	 * Zobrazeni vlastnich dat seznamu prodejcu
	 */
	foreach ($data as $prodejce){
		if(($counter%2)>0){
			$zvyrazneni = "";
		}else{
			$zvyrazneni = "zvyrazneni";
		}
		$counter++;
		
		echo "<div class='prodejce $zvyrazneni'>";
		echo "<div class='prodejce-ikony'>";
		//   echo "<img src='img/krizek.gif' width='16'> &nbsp; ";
		echo "<a href='$url_edit".$prodejce['pk_prodejce']."'>";
		echo "<img src='img/tuzka.gif' width='20' title='$napoveda_edit'>";
		echo "</a>&nbsp;";
		echo "</div>";
		
		echo "<div class='prodejce-obec'>".$prodejce['obec']."</div>";
		echo "<div class='prodejce-nazev'>".$prodejce['prodejce']."</div>";
		echo "<div class='prodejce-adresa'>".$prodejce['adresa']."</div>";
		echo "<div class='prodejce-telefon'>".$prodejce['telefon']."</div>";
		echo "<div class='prodejce-email'>".$prodejce['email']."</div>";
		echo "<div class='prodejce-web'>".$prodejce['web']."</div>";
		echo "</div>";
		
	}
}
/**
 *  \brief Vytvori formular a tlacitka pro vlozeni noveho prodejce;
 */
private function prdNovy() {
	echo "<button id='btnnovy'style='float:right;' onClick=\"document.getElementById('novyprodejce').style.display = 'block';this.style.display = 'none';\">Nový prodejce</button>";
	
	
	echo "<div id='novyprodejce' style='display:none;'>";
	echo "<fieldset id='fsnovyprodejce' style='background-color:#8ccfff;margin: 1% 0;'>";
	echo "<legend  style='background-color:#8ccfff;font-weight:bold;'> Formulář pro nového prodejce</legend>";
	echo "<form name='fnew' method='POST' action='sprava.php?'>";
	echo "<input type='hidden'name='mod' value='8'>";
	echo "<input type='hidden'name='met' value='prdNovyins'>";
	$this->prdPrvkyFormulare();
	echo "<div style='display:flex;justify-content:center;'>";
  echo "<input type='submit'value='Uložit'>";
  echo "<input type='reset' value='Storno' onclick=\"document.getElementById('novyprodejce').style.display = 'none';document.getElementById('btnnovy').style.display = 'block'\">";
  echo "</form>";
 
	echo "</div>";
	echo "</fieldset>";
	echo "</div>"; //id novyprodejce
}
/**
 *  \brief Vlozi data noveho prodejce;
 */
public function prdNovyins($param) {
	$this->opravneni->setIdUloha(IDULOHA_PRODEJCI);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění k vkládání nového prodejce",10);
	}
	$this->dbProd->prodejceins($param);
	$this->prdList();
}
/**
 *  \brief Editace dat prodejce;
 *  
 *  \param $id ID prodejce, ktery bude editovan
 */
public function prdEdit($param) {
	$this->opravneni->setIdUloha(IDULOHA_PRODEJCI);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění k editaci seznamu prodejců",10);
	}
	//var_dump($param);
	$id_prodejce = $param['id'];
	$this->dbProd->id_prodejce=$id_prodejce;
	$prodejce = $this->dbProd->data_prodejce();
	//var_dump($prodejce);
	$this->prdJSfunkce();
	echo "<h3>Editace dat prodejce</h3>";
	echo "<fieldset id='fsedit' style='background-color:#7BBEFF;margin: .5% 0;'>";
	echo "<legend style='background-color:#7bbeff;font-weight:bold;'> &nbsp; Úprava dat prodejce " . $prodejce['prodejce'] ."  &nbsp; </legend>";
	
	echo "<form name='fedit' method='POST' action='sprava.php?'>";
	echo "<input type='hidden'name='mod' value='8'>";
	echo "<input type='hidden'name='met' value='prdUpdate'>";
	echo "<input type='hidden'name='id' value='$id_prodejce'>";
	
	$this->prdPrvkyFormulare($prodejce);
	//$this->prdPrvkyFormulare();
	
	echo "<div style='display:flex;justify-content:center;'>";
  echo "<input type='submit'value='Uložit změny'>";
  echo "<input type='button'value='Smazat prodejce' onclick='eraseprodejce();'>";
  echo "</form>";
  echo "</fieldset>";
	echo "</div>";
	$this->prdList();
}
/**
 *  \brief ;
 */
private function prdJSfunkce() {
	?>
	<script  type="text/javascript">

	function eraseprodejce(){
		if(confirm("Opravdu smazat?")){
			window.location.href="sprava.php?mod=8&met=prdErase&id="+document.forms["fedit"]["id"].value;
		}
	}
	</script>
	<?php 
}
/**
 *  \brief Zpracuje formular pro editaci prodejce. Formular je vytvoren v 'prdEdit()';
 */
public function prdUpdate($param) {
	$this->opravneni->setIdUloha(IDULOHA_PRODEJCI);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění k editaci seznamu prodejců",10);
	}
//	var_dump($param);	
	$this->dbProd->id_prodejce=$param['id'];
	$this->dbProd->prodejceupd($param);
	$this->prdList();
}
/**
 *  \brief ;
 */
public function prdErase($param) {
echo "<h3>Odstranění prodejce ze seznamu</h3>";
$this->dbProd->id_prodejce=$param['id'];
$this->dbProd->prodejcedelete();
$this->prdList();
}
/**
 *  \brief Zobrazeni vsech prvku formulare. Oteviraci a ukoncovaci znacka formulare je vlozena ve volajici metode
 */
private function prdPrvkyFormulare($prodejce = array()) {
	//var_dump($prodejce);
	?>
	<style>
	.form-container{
		display:flex;
		flex-direction:column;
		width:50%;
		margin: auto;
		/*justify-content:center;*/
	}
	.form-polozka{
		margin: 1% 0;
	}
	.form-popis{
		float:left;
		width:20%;
		margin:auto 1px;
	}
	.form-hodnota{
	}
	.form-container input{
		width:70%;
	}
	</style>
	<?php 
	echo "<div class='form-container'>";
	
	echo "<div class='form-polozka'>";
	echo "<div class='form-popis'>Prodejce</div>";
  echo "<input type='text'name='prodejce'value='".(isset($prodejce['prodejce'])?$prodejce['prodejce']:"")."' autofocus>";
	echo "</div>";
	
	echo "<div class='form-polozka'>";
	echo "<div class='form-popis'>Obec</div>";
  echo " <input type='text'name='obec'value='".(isset($prodejce['obec'])?$prodejce['obec']:"")."'>";
	echo "</div>";
	
  echo "<div class='form-polozka'>";
  echo "<div class='form-popis'>Adresa</div> ";
  echo "<input type='text'name='adresa'value='".(isset($prodejce['adresa'])?$prodejce['adresa']:"")."'>";
	echo "</div>";
	
  echo "<div class='form-polozka'>";
  echo "<div class='form-popis'>Telefon</div> ";
  echo "<input type='text'name='telefon'value='".(isset($prodejce['telefon'])?$prodejce['telefon']:"")."'>";
	echo "</div>";
	
  echo "<div class='form-polozka'>";
  echo "<div class='form-popis'>E-mail</div>";
  echo " <input type='email'name='email'value='".(isset($prodejce['email'])?$prodejce['email']:"")."'>";
	echo "</div>";
	
  echo "<div class='form-polozka'>";
  echo "<div class='form-popis'>WWW stránky</div>";
  echo " <input type='text'name='web'value='".(isset($prodejce['web'])?$prodejce['web']:"")."'>";
	echo "</div>";
	
  echo "</div>"; // container
}
}



?>