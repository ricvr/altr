<?php
/**
 * @file IndexDb.php
 *
 *  \brief    Prace s databazi pro zobrazovani produktu
 * \details   Detailnější popis
 *
 *  \author    RiC
 *
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre
 *  \bug
 *  \warning
 *  \copyright RiC
 *  \todo Po uložení obrázku odstranit dočasné soubory
 *
 * Libovolně dlouhý podrobný popis
 *
 */

/**
 * \brief SQL dotazy pro prezentacni web
 * \details
 */
class ProdejciDb{
	private $dbh;
	public $id_prodejce; 
	//private $table_obrazky;
	//private $table_kategorie;
	//private $table_produkty;


	public function __construct()
	{
		$this->dbh = DB_Connect::newConnect();
		/*
		$this->table_obrazky = TABLE_PREFIX . "_obrazky";
		$this->table_kategorie = TABLE_PREFIX . "_kategorie";
		$this->table_produkty = TABLE_PREFIX . "_produkty";
		*/
		$this->table_prodejci = TABLE_PREFIX . "_prodejci";
	}
	/**
	 * @brief Seznam všech prodejců
	 *
	 * @param type null
	 *
	 * @return array Seznam prodejců
	 */
	public function prodejci()
	{
		$q = "SELECT
		pk_prodejce
		,prodejce
		,adresa
		,obec
		,telefon
		,web
		,email
		FROM $this->table_prodejci
		ORDER BY obec,prodejce";
		 
		$stmt = $this->dbh->prepare($q);
		$result = $stmt->execute();
		$data = $result->fetchall_assoc();
		return $data;;
	}
	/**
	 * @brief Data jednoho prodejce. ID prodejce se vklada do slotu 'id_prodejce'
	 *
	 * @param type null
	 *
	 * @return array Seznam prodejců
	 */
	public function data_prodejce()
	{
		$q = "SELECT
		pk_prodejce
		,prodejce
		,adresa
		,obec
		,telefon
		,web
		,email
		FROM $this->table_prodejci
		WHERE pk_prodejce= :1
		";
		 
		$stmt = $this->dbh->prepare($q);
		$result = $stmt->execute($this->id_prodejce);
		$data = $result->fetchall_assoc();
		return $data[0];
	}
/**
 *  \brief Ulozeni zmenenych dat prodejce;
 */
public function prodejceupd($param) {
	/*
	$pk_prodejce=$param["id"];
	$prodejce=$param["prodejce"];
	$obec=$param["obec"];
	$adresa=$param["adresa"];
	$telefon=$param["telefon"];
	$email=$param["email"];
	$web=$param["web"];
	*/
  $q = "UPDATE $this->table_prodejci SET
  				prodejce = :1
  				,adresa = :2
  				,obec = :3
  				,telefon = :4
  				,web = :5
  				,email = :6
  			WHERE pk_prodejce = $this->id_prodejce
  				";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute(
	    		$param["prodejce"]
	    		,$param["adresa"]
	    		,$param["obec"]
	    		,$param["telefon"]
	    		,$param["web"]
	    		,$param["email"]
	    		);
	    //$data = $result->fetchall_assoc();
	    return ;
}
/**
 *  \brief Odstraneni prodejce z tabulky \c _prodejci. ID prodejce se ulozi do slotu "id_prodejce";
 */
public function prodejcedelete() {
	    $q = "DELETE FROM $this->table_prodejci
	    			WHERE pk_prodejce = :1";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute($this->id_prodejce);
	    return;
}
/**
 * @brief Vklada data noveho prodejce
 *
 * @param type name
 *
 * @return
 */
public function prodejceins($param) 
{
	    $q = "INSERT INTO $this->table_prodejci(prodejce,adresa,obec,telefon,web, email)
	    	VALUES(
  				:1
  				,:2
  				,:3
  				,:4
  				,:5
  				,:6
	    	)";
	    $stmt = $this->dbh->prepare($q);
	    $result = $stmt->execute(
	    		$param["prodejce"]
	    		,$param["adresa"]
	    		,$param["obec"]
	    		,$param["telefon"]
	    		,$param["web"]
	    		,$param["email"]
	    		);
	    return;
}
}
?>
