<?php 
/**
 * @file Produkty.php 
 *
 *  \brief    6 - soubor _Produkty.php_ Vlozeni noveho produktu a uprava produktu
 * \details   
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      31.5.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo Overeni ve fn prodSeznam, ze uzivatel ma na ni právo
 *  \todo Funkci pro mazani produktu z tabulky - krizek v seznamu produktu
 *
 * 
 *
 */ 

/**
 * \brief  6 - class _Produkty_ Vlozeni noveho produktu a uprava produktu
 * \details   
 */ 
class Produkty{
  private $opravneni;
  private $dbprodukty;
  private $kategorie; /**< \brief Array se seznamem kategorii */
  

  public function __construct()
  {
   
    $this->dbprodukty = new ProduktyDb();

    if(isset($_SESSION['user']['pk_uzivatel'])){
      $this->pk_uzivatel = $_SESSION['user']['pk_uzivatel'];
    }else{
      $this->pk_uzivatel = 0;
    }
    $this->opravneni = new Opravneni($this->pk_uzivatel,session_id(),0);

  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
  echo "<h3>Použita neznámá metoda: $funname </h3>";
  var_dump($arg);

}

/**
  *  \brief Zobrazeni seznamu produktu, umoznuje pristup k editaci produktu
  *  @param typ popis
  *  @return string vystup
  */
public function prodSeznam()
{ // BEGIN function
$this->opravneni->setIdUloha(IDULOHA_PRODUKTY_EDITACE);
if($this->opravneni->kontrola_prav()< 0){
	throw new PrfException("Nemáte opravnění k zobrazení seznamu",10);
}

//pk_produkt,id_kategorie,nazev,popis_kratky,cenakc,cenacizi,ui.nik as nik_vlozil,p.dins, uu.nik as nik_upravil ,p.dupdate
  $produkty = $this->dbprodukty->seznam();
  //var_dump($produkty);

echo "<h3>Seznam produktů</h3>";
/*
  Zahlavi seznamu
*/
    echo "<div class='sez-produkt zahlavi'>";

    echo "<div class='sez-ikony'>";
    echo " &nbsp; ";
    echo "</div>";

    echo "<div class='sez-nazev zahlavi'>";
    echo "Název produktu";
    echo "</div>";

    echo "<div class='sez-popkratky zahlavi'>";
    echo "Krátký popis";
    echo "</div>";


    echo "<div class='sez-cenakc zahlavi'>";
    echo "Cena";
    echo "</div>";


    echo "<div class='sez-usr-upravil zahlavi'>";
    echo "Uživatel a datum poslední úpravy";
    echo "</div>";

    echo "<div class='sez-usr-ins zahlavi'> ";
    echo "Uživatel a datum vložení";
    echo "</div>";

    echo "</div>";

/*
  Vypsani vlastniho seznamu produktu
*/
  $url_edit = "sprava.php?mod=6&met=prodEdit&id=";
  $counter = 1;
  foreach($produkty as $prod){
    if(($counter%2)>0){
      $zvyrazneni = "";
    }else{
      $zvyrazneni = "zvyrazneni";
    }
    $counter++;

    $copy_napoveda = "Zkopíruje údaje a vytvoří z nich nový produkt.";
    $edit_napoveda = "Editace údajů produktu, přidání obrázků";
    echo "<div class='sez-produkt $zvyrazneni'>";

    echo "<div class='sez-ikony'>";
 //   echo "<img src='img/krizek.gif' width='16'> &nbsp; ";
    echo "<a href='$url_edit".$prod['pk_produkt']."'>";
    echo "<img src='img/tuzka.gif' width='20' title='$edit_napoveda'>";
    echo "</a>&nbsp;";
    echo "<a href='sprava.php?mod=6&met=prodKopie&id=". $prod['pk_produkt'] ."' class='tooltip'>";
    echo "<img src='img/copy.png' width='20'title='$copy_napoveda'>";
    echo "</a>";
    if (isset($prod['skryty'])){
    	echo " <img src='img/oko-cara.gif' width='16'title='Produkt nebude vidět v prezentaci'>";
    }else {
    	echo " <img src='img/nic.gif' width='16'title=''>";
    }
    echo "</div>";

    echo "<div class='sez-nazev'>";
    echo "<a href='$url_edit".$prod['pk_produkt']."'>";
    echo $prod['nazev'];
    echo "</a>";
    echo "</div>";

    echo "<div class='sez-popkratky'>";
    echo $prod['popis_kratky'];
    echo "</div>";


    echo "<div class='sez-cenakc'>";
    echo $prod['cenakc'].",00 Kč";
    echo "</div>";


    echo "<div class='sez-usr-upravil'>";
    echo $prod['nik_upravil']."<br>";
    echo $prod['dupdate'];
    echo "</div>";

    echo "<div class='sez-usr-ins'>";
    echo $prod['nik_vlozil']."<br>";
    echo $prod['dins'];
    echo "</div>";

    echo "</div>";
  }
} // END function

/**
  *  \brief Handler pro form z metody prodForm. Provadi ulozeni dat o novém produktu do db.
  *  @param array $prod Pole s jednotlivymi polozkami popisu produktu
  *  katgr; nazev; kratky; dlouhy; cenakc; cenacizi; eshopurl; eshoptext; detail1; detail2;
  *   @endparam

  *  @return string vystup 
 */
public function prodFrmHandler($prod)
{
  echo "<h3>Uložení nového produktu</h3>";
//   var_dump($prod);
  //$db = new ProduktyDb();

  if(!isset($prod['cenakc']) ||  $prod['cenakc'] <= 0){
    $prod['cenakc'] = 0;
  }
  if(!isset($prod['cenacizi']) || $prod['cenacizi'] <= 0){
    $prod['cenacizi'] = 0;
  }

  $vlozeno_id = $this->dbprodukty->produkt_ins($prod);
  //echo "<p>Vložené ID: ",$vlozeno_id;
  header("Location:sprava.php?mod=6&met=prodEdit&id=".$vlozeno_id);
  return;
}
/**
  *  \brief JS funkce pro odsstraneni produktu
  *  @param typ popis
  *  @return string vystup
  */
private function prodJSodstranit()
{ // BEGIN function
	
  ?>
  <script>
  function odstranit(idprod){
    if(!confirm("Pro odstranění produktu klikněte na 'OK' ")){
      return;
    }
    window.location.replace("sprava.php?mod=6&met=prodOdstranit&id="+idprod);
  }
  </script>
  <?php
} // END function

/**
  *  \brief Odstraneni zaznamu produktu. Volano JS funkci "odstranit()" > $this->prodJSodstranit(). Spousteno tl. ve formulari.
  *  @param typ popis
  *  @return string vystup
  */
public function prodOdstranit($in)
{ // BEGIN function
  $this->opravneni->setIdUloha(IDULOHA_PRODUKTY_EDITACE);
  if($this->opravneni->kontrola_prav()< 0){
      throw new PrfException("Nemáte opravnění k odstranění produktu",10);
  }


	//var_dump($in);
  $idprodukt = intVal($in['id']);

// Nacteni seznamu obrazku vztahujici se k jednomu produktu
  $obrdb = new ObrazkyDb();
  $obrdb->idprodukt = $idprodukt;
  $sez_obr = $obrdb->obrazkyproduktu();
  var_dump($sez_obr);
 

// Odstraneni obrazku z tab. "_obrazky" a smazani souboru
  $obr = new Obrazky();
  foreach($sez_obr as $key => $idobr){
//     echo "<br>";
//     var_dump($idobr['pk_obrazek']);
    $obr->obrSmazat($idobr['pk_obrazek']);
  }

  //$obrdb->odstranitObrazky($idprodukt); // odstraneni obrazku z tabulky "_obrazky"
  $this->dbprodukty->setIdProdukt($idprodukt); // Odstraneni zaznamu produktu
  $this->dbprodukty->odstranit();

  header("Location: sprava.php?mod=6&met=prodSeznam");

} // END function
/**
  *  \brief Zobrazeni formulare pro _editaci produktu_. Zobrazuje data produktu.
  *   Obsluha formulare: 'prodEditHandler()'
  *  @param array $in['id'] ID produktu, ktery se bude editovat
  *  @return string vystup
  */
public function prodEdit($in)
{ // BEGIN function
  $this->opravneni->setIdUloha(IDULOHA_PRODUKTY_EDITACE);
  if($this->opravneni->kontrola_prav()< 0){
      throw new PrfException("Nemáte opravnění k editaci produktu",10);
  }
  $this->prodJSodstranit();


  /* Kontrola, jestli je volano po ulozeni zmen produktu.
     Z fn "prodEditHandler($in)" se vola s priznakem $upd
   */
  $upd=isset($in['upd'])?intVal($in['upd']):0;
  if($upd>0){
    echo "<div class='info'>";
    echo "Změny byly uloženy.";
    echo "</div>";
  }
  $this->JS_menu();
  echo "<h3>Úpravy produktu</h3>";

  //echo "<br>Editovaný produkt: " . $in['id'],"<br>";

  $produkty = $this->dbprodukty->produkt($in['id']);
//   var_dump($produkty);
  echo " <form name='produkt' id='produkt' method='POST' action='sprava.php' onsubmit='return kontrola(this);'> ";
  echo "<input type='hidden' name='mod' value='6'>";
  echo "<input type='hidden' name='met' value='prodEditHandler'>";
  echo "<input type='hidden' name='idprodukt' id='idprodukt' value='". $in['id'] ."'>";

  $this->prodMenuKategorie($produkty['id_kategorie']);
  $this->prodFormPrvky($produkty);
  echo " </form> ";
  echo "<div id='obrazky'style='width:100%;clear:both;'>";
  echo "<h4 align='center'>Obrázky produktu</h4>";
  $this->prodObrazky($in['id']);
  echo "</div><!-- id=obrazky -->";
} // END function
/**
  *  \brief Zajistuje vytvoreni formulare pro upload obrazku
  *  @param int $id ID produktu, pro ktery se budou uploudovat obrazky
  *  @return string vystup
  */
public function prodObrazky($id)
{ // BEGIN function
?>
<form name="obr-upload" action="sprava.php" method="post" enctype="multipart/form-data">
<fieldset id="obrazky-form">
  <legend>Vložení obrázku produktu</legend>
<input type="hidden" name="mod" value="7">
<input type="hidden" name="met" value="obrUpload">
<input type="hidden" name="id" value="1">
<input type="hidden" name="idprodukt" value="<?php echo $id ?>">
<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo MAX_VELIKOST_OBRAZKU ?>">
<input type="hidden" name="action" value="true">
<div style="width:35%;float:left;">
Povoleny jsou formáty JPEG/JPG a PNG

<br>Maximální povolená velikost obrázku: <?php echo MAX_VELIKOST_OBRAZKU; ?>&nbsp;bytů
</div>

Najít obrázek<input type="file" name="obrFile">
&nbsp;&nbsp;
<input type="submit" value="Uložit obrázek">
</fieldset>
</form>

<?php

$this->prodVlozeneObrazky($id);
} // END function
/**
  *  \brief Zobrazi nahledy vlozenych  obrazku k editovanemu produktu
  *  @param typ popis
  *  @return string vystup
  */
public function prodVlozeneObrazky($idprod)
{ // BEGIN function
	// ziskat seznam obr., jejich ID
  $this->dbprodukty->setIdProdukt($idprod);
  $obrazky = $this->dbprodukty->obrazky();
  //var_dump($obrazky);
  // vygenerovat odkazy
  foreach($obrazky as $obr){
    $delObr = "sprava.php?mod=6&amp;met=prodSmazatObr&amp;id=".$obr['pk_obrazek'] ."&amp;idprod=$idprod";
    echo "<div class='editobrazek'>";
    echo "<img src='vypsat_obr.php?id=".$obr['pk_obrazek']."&amp;vel=0' width='100%'>";
    echo "<br/>(".$obr['pk_obrazek'].") ".$obr['nazev'];
    echo "<br/><a href='$delObr'><img src='img/krizek.gif' width='16px;'>Smazat obrázek</a>";
    echo "</div>";
  }
  //echo"Konec obrazku";
} // END function
/**
  *  \brief 
  *  @param typ popis
  *  @return string vystup
  */
public function prodSmazatObr($in)
{ // BEGIN function
  $idobr = intVal($in['id']);
  $idprod = intVal($in['idprod']);
	$obr = new Obrazky();
  $obr->obrSmazat($in['id']);
  header("Location:sprava.php?mod=6&met=prodEdit&id=".$idprod);
} // END function
/**
  *  \brief Obsluha formulare pro editaci produktu *prodEdit()->prodFormPrvky()*. Zajistuje ulozeni editovanych udaju do db
  *  @param typ popis
  *  @return string vystup
  */
public function prodEditHandler($in)
{ // BEGIN function
  $this->opravneni->setIdUloha(IDULOHA_PRODUKTY_VLOZENI);
  if($this->opravneni->kontrola_prav()< 0){
      throw new PrfException("Nemáte opravnění k editaci produktu",10);
  }
//   echo "<p>";
   var_dump($in);
   // Osetreni checkboxu pro skryvani produktu
   /*
   if (isset($in['skryty'])) {
   	echo "<br/>SKRÝT";
   }else {
   	echo "<br/>NEskrýt";
   }
   return;*/
  $this->dbprodukty->produkt_upd($in);
  header("Location:sprava.php?mod=6&met=prodEdit&id=".$in['idprodukt']."&upd=1");


} // END function
/**
  *  \brief Formular pro vlozeni noveho produktu. Je zpracovan prodFrmHandler()
  *  @param  
  *  @return  
  */
public function prodForm()
{
  $this->opravneni->setIdUloha(IDULOHA_PRODUKTY_VLOZENI);
  if($this->opravneni->kontrola_prav()< 0){
      throw new PrfException("Nemáte opravnění ke vkladaní produktů",10);
  }
  
  echo "<h3>Vložení produktu</h3>";
  $this->JS_menu();
  echo " <form name='produkt' id='produkt' method='POST' action='sprava.php' onsubmit='return kontrola(this);'> ";
  echo "<input type='hidden' name='mod' value='6'>";
  echo "<input type='hidden' name='met' value='prodFrmHandler'>";

  $this->prodMenuKategorie();
  $this->prodFormPrvky();

echo " </form> ";

return;
}
/**
  *  \brief Inputy pro zadani dat noveho produktu
  *  @param array $prod Nazvy klidu odpovidaji nazvum atributu v tabulce _produkty
  *  @return string vystup 
 */
private function prodFormPrvky($prod = array())
{
//var_dump($prod);
echo "    <div id='formular'> ";
echo "   <fieldset id='prodnovy'> ";
echo "     <legend>&nbsp;Karta produktu&nbsp; ";
echo "     </legend> ";
echo "     <div class='polozka'> ";
echo "  <label for='nazev'>Název&nbsp;* ";
echo "  </label> ";
echo "  <input type='text' id='nazev' name='nazev' autofocus value='".(isset($prod['nazev'])?$prod['nazev']:'')."' style='width:20em;'> ";
echo "     </div> ";
echo "     <div class='polozka' style='padding:10 0;'> ";
echo "    <label for='kratky' >Krátký popis ";
echo "    </label> ";
echo "    <textarea id='kratky' name='kratky' style='width:15em;height:5em;' placeholder='Stručný a krátký popis (max. 200 zn)'>";
echo (isset($prod['popis_kratky'])?$prod['popis_kratky']:'')."</textarea>";
$this->prodInsCKEdit("kratky",400);
echo "     </div> ";
echo "     <div class='polozka'> ";
echo "    <label for='dlouhy'>Dlouhý popis ";
echo "    </label> ";
echo " ";
echo "<textarea id='dlouhy' name='dlouhy' style='width:25em;height:8em;' placeholder='Detailní popis '>";
echo (isset($prod['popis_dlouhy'])?$prod['popis_dlouhy']:'')."</textarea> ";

$this->prodInsCKEdit("dlouhy",600);
echo "     </div> ";
echo "     <div class='polozka'> ";
echo "  <label for='cenakc' >Cena Kč ";
echo "  </label> ";
echo "  <input type='text' id='cenakc' name='cenakc' style='width:5em;' value='".(isset($prod['cenakc'])?$prod['cenakc']:'')."'> ";
echo "     </div> ";
echo "     <div class='polozka'> ";
echo "  <label for='cenacizi'>Cena € ";
echo "  </label> ";
echo "  <input type='text' id='cenacizi' name='cenacizi' style='width:5em;' value='".(isset($prod['cenacizi'])?$prod['cenacizi']:'')."'> ";
echo "     </div> ";
echo "     <div class='polozka'> ";
echo "  <label for='eshopurl'>Odkaz na eshop</label> ";
echo "  <input type='url' id='eshopurl' name='eshopurl' style='flex:auto;' value='".(isset($prod['eshop_url'])?$prod['eshop_url']:'')."'> ";
echo "     </div> ";
echo "     <div class='polozka'> ";
echo " ";
echo "    <label for='eshoptext'>Text odkazu</label> ";
echo " ";
echo "  <input type='text' id='eshoptext' name='eshoptext' style='width:7em;' value='".(isset($prod['eshop_text'])?$prod['eshop_text']:'')."'> ";
echo "     </div> ";
echo "     <div class='polozka'> ";
echo " ";
echo "    <label for='detail1'>Hmotnost [g] ";
echo "    </label> ";
echo " ";
echo "  <input type='text' id='detail1' name='detail1' style='width:5em;' value='".(isset($prod['hmotnost'])?$prod['hmotnost']:'')."'> ";
echo "     </div> ";

echo "     <div class='polozka'> ";
echo " ";
echo "    <label for='detail2' >Velikosti ";
echo "    </label> ";
echo " ";
echo "  <input type='text' id='detail2' name='detail2' style='width:7em;' value='".(isset($prod['velikosti'])?$prod['velikosti']:'')."'> ";
echo "     </div> ";

echo "     <div class='polozka'> ";
echo " ";
echo "    <label for='detail3' >Tlumení ";
echo "    </label> ";
echo " ";
echo "  <input type='text' id='detail3' name='detail3' style='width:7em;' value='".(isset($prod['detail3'])?$prod['detail3']:' ')."'> ";
echo "     </div> ";

echo "     <div class='polozka'> ";
echo " ";
echo "  <input type='checkbox' id='skryty' name='skryty' style='' ".(isset($prod['skryty'])?'CHECKED':'')."> ";
echo "  <label for='skryty' >Skrýtí produktu ";
echo "    </label> ";
echo (isset($prod['skryty'])?' &nbsp; <strong>Skrytý </strong>':'');
echo "     </div> ";
echo "   </fieldset> ";


echo "<div id='edit-log'>";
echo "<span class='edit-popisek'>Poslední úprava: </span>".(isset($prod['nik_upravil'])?$prod['nik_upravil']:'');
echo " &nbsp; ".(isset($prod['dupdate'])?$prod['dupdate']:'');
echo "<br><span class='edit-popisek'>Vložil: </span>".(isset($prod['nik_vlozil'])?$prod['nik_vlozil']:'');
echo " &nbsp; ".(isset($prod['dins'])?$prod['dins']:'');
echo "</div>";


echo "<p align='right'>";
if((isset($prod['pk_produkt'])?$prod['pk_produkt']:0) > 0){
	echo "<span style='float:left;'><a href='sprava.php?mod=6&met=prodKopie&id=". $prod['pk_produkt'] ."'>";
	echo "<img src='img/copy.png' width='20'title='Vytvoření kopie z tohoto produktu'> Vložit kopii</a>";
	echo "</span>";
}
echo "<input type='submit' value=' Uložit ' '>";
if((isset($prod['pk_produkt'])?$prod['pk_produkt']:0) > 0){
  echo "  <input type='button' value=' Odstranit produkt '  onClick='odstranit(document.produkt.idprodukt.value);'>";
}
echo "</p>";

//echo "   </fieldset> ";


echo "</div> <!-- id formular -->";
}
/**
  *  \brief Vytvari menu, ve kterem se vybira kategorie pro zarazeni produktu
  *  @param int $idkat ID kategorie, ktera bude předvyplněna 
  *  @return string vystup 
 */
private function prodMenuKategorie($id_checked=0)
{
  $this->kategorie = $this->dbprodukty->kategorie_menu();
  echo "<div id='kategorie'>";
  //echo "<ul class='kat-menu'>";
  $this->prodPodkategorie(1,$id_checked);
  //echo "</ul>";
  echo "</div>";
  return;
}
/**
  *  \brief Rekurzivni metoda pro vypsani podkategorii
  *  @param int $iduzel ID uzlu, jehož podkategorie (potomci) sevypisuji. 
  *  @return string vystup 
  */
public function prodPodkategorie($iduzel,$id_checked)
{
  $idrodic = $iduzel;
  echo "<ul class='kat-menu'>";
  foreach($this->kategorie as $kat){
    if($kat['id_rodic']!=$idrodic){
      continue;
    }
    /*
     echo "<br>";
    for ($x=0;$x<=$counter;$x++){
      echo "&nbsp;&nbsp;";
    }
    */
    echo "<li>";
    if($kat['id_kategorie']==$id_checked){
      $checked='checked';
    }else{
      $checked='';
    }
    echo "<input type='radio' name='katgr' value='". $kat['id_kategorie'] ."' $checked>";
    echo $kat['kategorie'] ;
    echo "</li>";
    echo "<li>";
    $this->prodPodkategorie($kat['id_kategorie'],$id_checked);
    echo "</li>";
  }
  echo "</ul>";
return;
}
/**
 *  \brief Vytvori kopii produktu, kterou vlozi do tab produkty a otevre ji k editaci ve formulari.;
 */
public function prodKopie($param) {
	//var_dump($param);
	
	$this->opravneni->setIdUloha(IDULOHA_PRODUKTY_VLOZENI);
	if($this->opravneni->kontrola_prav()< 0){
		throw new PrfException("Nemáte opravnění ke vkladaní produktů",10);
	}
	
	echo "<h3>Nový produkt vytvořený kopií</h3>";
	$this->JS_menu();
	echo " <form name='produkt' id='produkt' method='POST' action='sprava.php' onsubmit='return kontrola(this);'> ";
	echo "<input type='hidden' name='mod' value='6'>";
	echo "<input type='hidden' name='met' value='prodFrmHandler'>";
	
	$produkty = $this->dbprodukty->produkt($param['id']);
	$produkty['pk_produkt'] = null;
	$produkty['nazev'] = "KOPIE-".$produkty['nazev'];
	$this->prodMenuKategorie($produkty['id_kategorie']);
	$this->prodFormPrvky($produkty);
	
	echo " </form> ";
	
	
}
/**
  *  \brief jQuery pro oznacovani vybrane kategorie v menu
  *  @param typ popis 
  *  @return string vystup 
  */
public function JS_menu()
{
?> 
 <script>

  $(document).ready(function(){
          /* klik na radio s nazvem "katgr" je svazano s funkci, ktera
             vsechny prepinace zbavi zvyrazneni a tomu, na ktery se kliklo
             nastavi tridu zvyrazneni
           */
           $('[name="katgr"]').on('click',function(){
             $('[name="katgr"]').parent().removeClass('oznac-kategorii');
             $(this).parent().addClass('oznac-kategorii');
           });
     
      });

  function kontrola(form){
    //alert('Kontrola data');

    if(!kon_kategorie(form)){
      return false;
    }

    if(!kon_nazev(form)){
      return false;
    }
    return true;
  }

  /*
  Kontrola nazvu produktu
  */
  function kon_nazev(form){
    var nazev = form.nazev.value;
    var x = nazev.trim();
    //alert('Délka názvu: '+nazev.length+" x:"+x.length + "\n"+nazev.trim());
    if(nazev.length <=0){
      alert('Nenízadaný název produktu.');
      return false;
    }
    form.nazev.value = x;

    return true;
  }
  /*
  Kontrola vybrání kategorie
  */
  function kon_kategorie(form)
  {
	  var kat = 0;
	  for (i=0; i<form.katgr.length; i++){
		  if (form.katgr[i].checked){
	  		kat = form.katgr[i].value;
			  //alert('Kontrola: '+ kat+"\n");
		  }
	  }
	  //alert('Kontrola: '+ kat+"\n"+form.katgr);
	    if(kat > 0){
	      //alert("Větší než 0 "+kat);
	      return true;
	    }else{
	      alert("Není vybrané zařazení produktu");
	      return false;
	    }

	  }
	  
</script>
<?php
return;
}
/**
 * @brief Vlozi volani javascriptoveho editoru CKEdit
 *
 * @param string Nazev elementu, ktery bude editovan
 *
 * @return
 */
private function prodInsCKEdit($name,$sirka = 400) 
{
echo "<script>
CKEDITOR.replace( '$name', {
	uiColor: '#14B8C4',
	width: $sirka,
	toolbar: [
			[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
			[ 'FontSize', 'TextColor', 'BGColor' ]
	]
});

	</script>
";
	
}
}
?>


