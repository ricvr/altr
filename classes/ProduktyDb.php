<?php 
/**
 * @file ProduktyDb.php 
 *
 *  \brief    Stručný popis souboru
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief Trida pro pristup do databaze
 * \details   Detailnější popis
 */ 
class ProduktyDb{
  private $dbh;
  private $query;
  private $produkty;
  private $idprodukt; /**< @brief ID produktu, ktery se kterym se pracuje*/

  public function __construct()
  {
    $this->dbh = DB_Connect::newConnect();
    $this->table_kategorie = TABLE_PREFIX . "_kategorie";
    $this->table_produkty = TABLE_PREFIX . "_produkty";
    $this->table_uzivatele = TABLE_PREFIX . "_uzivatele";
    $this->table_obrazky = TABLE_PREFIX . "_obrazky";

  }
  public function setquery($q){
    $this->query = $q;
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{
  echo "<h3>Použita neznámá metoda: $funname<br>",$arg[0],"</h3>";

}

/**
  *  \brief Nastaveni slotu 'idprodukt'
  */
public function setIdProdukt($val)
{ // BEGIN function
	$this->idprodukt = $val;
} // END function


/**
  *  \brief Vraci array se vsemi produkty pro seznam produktu v administraci
  *  @param typ popis
  *  @return string vystup
  */
public function seznam()
{ // BEGIN function
    $q = "SELECT pk_produkt,id_kategorie,nazev,popis_kratky,cenakc,cenacizi,ui.nik as nik_vlozil,p.dins, uu.nik as nik_upravil ,p.dupdate,p.skryty
          FROM ". $this->table_produkty ." p
            LEFT JOIN ". $this->table_uzivatele ." ui ON ui.pk_uzivatel = user_ins
            LEFT Join ". $this->table_uzivatele ." uu ON uu.pk_uzivatel = user_upd
          ORDER BY nazev";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute();
    $data = $result->fetchall_assoc();
    return $data;
} // END function
 

/**
  *  \brief 
  *  @param typ popis
  *  @return string vystup
  */
public function obrazky()
{ // BEGIN function
    $q = "SELECT pk_obrazek,nazev FROM ".$this->table_obrazky." WHERE fk_produkt= :1";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($this->idprodukt);
    $data = $result->fetchall_assoc();
    return $data;
} // END function
/**
  *  \brief Seznam kategorii pro vytvoreni menu
  *  @param typ popis 
  *  @return string vystup 
  */
public function kategorie_menu()
{
  $q = "SELECT uroven,id_rodic,id_kategorie,kategorie,popis
FROM ". $this->table_kategorie ."
ORDER BY uroven, id_rodic, razeni"; 

  //echo "<p>$q</p>";
  $stmt = $this->dbh->prepare($q);
  $result = $stmt->execute();
  $data = $result->fetchall_assoc();
  return $data;
}
 /**
   *  \brief Vlozi novy produkt do tab. altr_produkty
   *  @param typ popis
   *  katgr; nazev; kratky; dlouhy; cenakc; cenacizi; eshopurl; eshoptext; detail1; detail2;
   *
   * detail1 = hmotnost,
   * detail2 = velikosti
   *  @return int ID produktu, ktery byl vlozen. ">0" = OK; "0"=neco je spatne
   */
 public function produkt_ins($prod)
 { // BEGIN function
    extract($prod); // nazvy promeny viz  @param
    $cenamena = "€";
    $q = "INSERT INTO ". $this->table_produkty ."
    (id_kategorie,nazev,popis_kratky, popis_dlouhy,detail1,detail2,cenakc,cenacizi,cenamena
    ,eshop_url,eshop_text,user_ins,detail3)
    VALUES('$katgr','$nazev','$kratky','$dlouhy','$detail1','$detail2',$cenakc,$cenacizi,'$cenamena'
            ,'$eshopurl','$eshoptext',". $_SESSION['user']['pk_uzivatel'] .",'$detail3')";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute();
    return $result->insert_id;
 } // END function
/**
  *  \brief Vraci data o produkutu z tabulky _produkty
  *  
  *  @param int $id ID produktu, jehoz data se hledaji
  *  @return array Pole s daty z tabulky
  *
    $q = "SELECT pk_produkt,id_kategorie,nazev,popis_kratky,cenakc,cenacizi,ui.nik as nik_vlozil,p.dins, uu.nik as nik_upravil ,p.dupdate,p.skryty
          FROM ". $this->table_produkty ." p
            LEFT JOIN ". $this->table_uzivatele ." ui ON ui.pk_uzivatel = user_ins
            LEFT Join ". $this->table_uzivatele ." uu ON uu.pk_uzivatel = user_upd
          ORDER BY id_kategorie,nazev";
  *
  */
public function produkt($id)
{ // BEGIN function
	  $q = "SELECT pk_produkt,id_kategorie,nazev,popis_kratky,popis_dlouhy,detail1 as hmotnost, detail2 as velikosti,detail3
          ,cenakc,cenacizi,cenamena,eshop_url,eshop_text,ui.nik as nik_vlozil,p.dins, uu.nik as nik_upravil ,p.dupdate,p.skryty
          FROM ".$this->table_produkty." p
            LEFT JOIN ". $this->table_uzivatele ." ui ON ui.pk_uzivatel = p.user_ins
            LEFT JOIN ". $this->table_uzivatele ." uu ON uu.pk_uzivatel = p.user_upd
          WHERE pk_produkt= :1";
   // echo "<p>$q</p>";
   $stmt = $this->dbh->prepare($q);
   $result = $stmt->execute($id);
   return $result->fetch_assoc();
} // END function
/**
  *  \brief Ulozi zmeny v info o produktu. Vlozi ID uzivatele, ktery update proved
  *  
  *  Pokud je nastaven CHECKBOX ve formulari, bude do atributu _skryty_ vlozeno aktualni datum.
  *  Nebude-li nastaven CHECBOX, bude do atributu _skryty_ vlozena hodnota NULL
  *  
  *  @param array $prod Pole s daty produktu, ktere se budou updatovat do tabulky
  *   idprodukt; katgr; nazev; kratky; dlouhy; cenakc; cenacizi; eshopurl; eshoptext; skryty; detail1; detail2;
  *  @return string vystup
  */
public function produkt_upd($prod)
{ // BEGIN function
  extract($prod); // sasmostatne promene viz komentar @param
    $q = "UPDATE ".$this->table_produkty."
    SET
      id_kategorie = :1
      ,nazev = :2
      ,popis_kratky = :3
      ,popis_dlouhy = :4
      ,cenakc = $cenakc
      ,cenacizi = $cenacizi
      ,eshop_url = :5
      ,eshop_text = :6
      ,detail1 = :7
      ,detail2 = :8
      ,detail3 = :9
      ,user_upd = ".$_SESSION['user']['pk_uzivatel']."
      ,dupdate = CURRENT_TIMESTAMP
      " . (isset($prod['skryty'] )?',skryty = CURRENT_TIMESTAMP':',skryty = NULL ') . "
    WHERE pk_produkt= $idprodukt";
    //echo "<br>$q<br>";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($katgr,$nazev,$kratky,$dlouhy,$eshopurl,$eshoptext,$detail1,$detail2,$detail3);
    //$data = $result->fetchall_assoc();
} // END function

/**
  *  \brief Odstrani zaznam produktu z tabulky "_produkty"
  *  @param int $id ID produkut je ve slotu idprodukt
  *  @return string vystup
  */
public function odstranit()
{ // BEGIN function
    $q = "DELETE FROM ". $this->table_produkty ." WHERE pk_produkt = :1";
    $stmt = $this->dbh->prepare($q);
    $result = $stmt->execute($this->idprodukt);
    return;
} // END function
}
?>


