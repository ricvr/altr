<?php
/**
 * @file ShowProducts.php
 *
 *  \brief    23 -Soubor s \c class __ShowProducts__ 
 * \details   Detailnější popis
 *         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */

/**
 * \brief 23 - __ShowProducts__ Zobrazovani produktu
 * 
 * \details Nacteni potomku/predku
 * 
 * Zobrazuje rodokmen kategorie nebo produktu.
 
 * Vyberem kategorie se zobrazi vsechny produkty, ktere do ni patri, vcetne produktu, ktere patri do podrizenych kategorii
 *  
 * Pouziva class __IndexDB__ pro nacteni dat z db. 
 * \enddetails
 */
class ShowProducts {
	private $kategorievsechny; /**< @brief Array se vsemi zaznamy z tabulky _kategorie */
	private $kategorie; /**< @brief Array Udaje o jedne kategorie */
	private $rodokmen; /**< \brief Obsahuje PREDKY|POTOMKY urcite kategorie. Zalezi cim se naplni */
	/**
	 * 
	 */
	public function __construct() {
		$this->dbIdx = new IndexDb ();
		//$this->rodokmen = array();
		$this->kategorievsechny = $this->dbIdx->kategorievse ();
		
	}
	
	/**
	 * Přetížená funkce.
	 * Automaticky bude volat hlavní metodu třídy 'Index' s argumenty, se kterými byla volána.
	 *
	 * @param int $idkat
	 *        	ID kategorie, jejiz produkty se maji zobrazovat
	 * @return
	 *
	 */
	public function __call($funname, $arg) {
		echo "<h3>Použita neznámá metoda: ", $funname, "</h3> Volaná s argumenty:";
		var_dump ( $arg );
		echo "<h3>Volání bez metody</h3>";
	}
	/**
	 *  @brief Nacte data o kategorii z array $this->kategorievsechny;
	 *  
	 *  @param int $idkat ID hledane kategorie
	 */
	private function spGetKategorii($idkat) {
		foreach($this->kategorievsechny as $kategorie){
			if($kategorie['id_kategorie']==$idkat){
				$this->kategorie = $kategorie;
				break;
			}
		}
		
	}
	/**
	 * \brief Vstupní metoda pro zpracovavajici pozadavek na zobrazeni vybrane kategorie. ID kategorie se predava jako parametr.
	 * 
	 * @param int $kat Nazev kategorie, ktera je zobrazovana 
	 * @param int $idkat	ID kategorie, ktera se zobrazuje
	 * @return string vystup
	 */
	public function spShow($in) { // BEGIN function
	  // echo "Zobrazení produktu<br>";
	   //var_dump($in);
	   
		$this->spGetKategorii($in ['idkat']);
	   /* nalezeni "rodokmenu" zobrazovane kategorei
	    */
		$predkove = $this->spPredkove ( $in ['idkat'] );
		//var_dump($this->kategorievsechny);
		/* 
		 * Vypsani rodokmenu zobrazovane kategorie (predky)
		 */
		$_SESSION['seo']['title']= TAG_TITLE_COMMON . " ".$this->kategorie['kategorie'];
		$_SESSION['seo']['description'] = $this->kategorie['popis'];
		echo "<div id='predci'>";
		$this->spVypsatPredky($in['idkat']);
		$this->spVypsatSourozence($in['idkat']);
		echo "</div>"; // id='potomci'>";
		
		
		if(POPIS_KATEGORIE){
			echo "<div id='kategorie-popis'>";
			
			foreach($this->kategorievsechny as $kategorie){
				if($kategorie['id_kategorie']==$in['idkat']){
					//echo "<span class='zvyrazneni'>".$in['kat']."</span> - ";
					echo $kategorie['popis'] ;
					break;
				}
			}
			echo "</div>";
		}
		/*
		 * Vypsani podrizenych kategorii (potomku)
		 */ 
		
		echo "<div id='potomci'>";
				$this->spVypsatPotomky($in['idkat']);
		echo "</div>";// id='potomci'>";
		
		/*
		 * Nalezeni vsech produktu, ktere patri do kategorie nebo potomku
		 */
		$this->spShowTile($in ['idkat']);
	} // END function

	/**
	 *  \brief Vypise sourozence vybrane kategorie;
	 */
	private function spVypsatSourozence($idkat) {
		
		//echo "<p style='text-align:center;'><span id='sourozenci'>";
		$this->spGetKategorii($idkat);
		//echo "Sourozenci - ";
		$prvni = true;
		foreach ($this->kategorievsechny as $kat){
			if($prvni){
				echo"|";
				$prvni=false;
			}
			if(($kat['id_rodic']==$this->kategorie['id_rodic']) && 
					($kat['id_kategorie']!=$this->kategorie['id_kategorie'])){
				echo "<a class='button-sourozenci' href=index.php?mod=23&amp;met=spshow&amp;kat=" 
						. urlencode($kat['kategorie']) . "&amp;idkat=" . $kat['id_kategorie'] . " title='".$kat['popis'] ."'>";
				echo $kat['kategorie'];
				echo "</a>|";
			}
		}
		//var_dump($this->kategorie);
		echo "</span>";
		//echo "</p>";
	}
	/**
	 * @brief Zobrazi prime potomky kategorie
	 *
	 * @param int $idkat ID kategorie, jejiz prime potomky se vypisuji
	 *
	 * @return
	 */
	public function spVypsatPotomky($idkat) 
	{
		$kategorie = $this->dbIdx->kategorierodice($idkat);
		foreach ( $kategorie as $kat ) {
			echo " &nbsp; <span class=''>";
			echo "<a class='button-potomci' href=index.php?mod=23&amp;met=spshow&amp;kat=" 
					. urlencode($kat ['kategorie']) . "&amp;idkat=" . $kat ['id_kategorie'] . " title='".$kat['popis'] ."'>";
			echo $kat ['kategorie'];
			echo "</a>";
			echo "</span> &nbsp; ";
		}
	}
	/**
	 * @brief Vypisuje Predky (rodokmen) kategorie, jejiz ID je v parametru. Vypisuji se jako linky
	 *
	 * @param int $idkat ID kategorie, jejiz predkove se vypisuji.
	 *
	 * @return
	 */
	public function spVypsatPredky($idkat) 
	{
			foreach ( $this->rodokmen as $kat ) {
			if (isset ( $kat ['kategorie'] )) {
				echo "<span>";
				if ($kat['id_kategorie']!=$idkat){
					echo "<a class='button-predci' href=index.php?mod=23&amp;met=spshow&amp;kat=" 
							. urlencode($kat ['kategorie']) . "&amp;idkat=" . $kat ['id_kategorie'] . " title='".$kat['popis']."'>";
					//echo $kat ['kategorie'] . "</a> \\ ";
					echo $kat ['kategorie'] . "</a> <img src='img/separator1.gif' width='15px' /> ";
				}else {
					//echo $kat ['kategorie'] . " \\ ";
					echo $kat ['kategorie'];
					//echo " <img src='img/separator2.gif' width='15px' /> ";
				}
				echo "</span> ";
			}
		}
		//$this->spVypsatSourozence($idkat);
	}
/**
 * @brief Vyhleda vsechny produkty, ktere patri do konkretni kategorie, 
 * nebo do nektere z jejich potomku. Musi vyhledat vsechny potomky (kategorie).
 * 
 * \details Načte kategorie-potomky a pak z tabulky _produkty pro ne vyselektuje produkty.
 * 
 * Data kazdeho produktu se pak zobrazi Tiles::tilGetTile()
 * 
 * @param int $idkat ID kategorie, jejiz potomci a produkty se budou vypisovat
 */
	private function spShowTile($idkat)
	{
		$tiles = new Tiles();
		$tiles->setCategoryId($idkat);
		$tiles->tilProductes();
		
		$pocet = $tiles->NumberOfProducts();
		echo("<br>Produktů: $pocet");
		
		echo "<div id='tiles-all'>";
		if ($pocet<=0){
			echo("<div class='info'>Kategorie neobsahuje produkty</div>");
			return;
		}
		$tiles->tilShowTiles();
		echo"</div>"; // id='tiles-all'>");
		
	}

/**
	 * @brief Spousteci fn pro rekurzivni hledani "rodokmenu" kategorie. Volam fn spRodic
	 * Hleda predky (=nadrazene urovne v hiearchickem cleneni produktu)
	 * 
	 * @param int $idkat ID kategorie, jejiz rodokmen se hleda
	 *        	
	 * @return string vystup
	 */
	public function spPredkove($idkat) 
	{ 
	//	$this->kategorievsechny = $this->dbIdx->kategorievse ();
		$this->rodokmen [] = $this->spRodic ( $idkat );
		$this->spVycistitRodokmen();
		return;
	} // END function
	/**
	 * \brief Prochazi rekurzi pole kategorii a hleda rodice kategorie
	 * 
	 * @param int $idkat
	 *        	ID kategorie, jejiz rodic se hleda
	 * @return string vystup
	 */
	private function spRodic($idkat) { // BEGIN function
		foreach ( $this->kategorievsechny as $kat ) {
			if ($kat ['id_kategorie'] == $idkat) {
				if ($idkat == 1)
					return; // Nejvyssi uroven, dalsi se uz nehleda
				$this->rodokmen [] = $this->spRodic ( $kat ['id_rodic'] );
				return $kat;
			}
		}
		return;
	} // END function
	/**
	 * @brief  Vycisteni $this->predkove od kategorie "root"
	 */
	private function spVycistitRodokmen() {
		foreach ($this->rodokmen as $kat){
			if (isset ( $kat ['id_kategorie'] )) {
				$x[] = $kat;
			}
		}
		$this->rodokmen = $x;
		return;
	}
	
}
?>


