<?php 
/**
 * @file Tiles.php 
 *
 *  \brief    Soubor s class __Tiles__, ktera vytvari jednu dlazdici-informaci o produktutručný popis souboru
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      17.6.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo popis, co se má ještě udělat 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief _Nema pridelene cislo_ Vytvari jednu dlazdici o produktu
 * 
 * \details   Class neni volana z parametram v http pozadavku a proto nema cislo. 
 * Volaji ji jednotlive metody v class, ktere nejakym zpusobem zobrazuji produkty.
 */ 
class Tiles{

  private $productes; /**< \brief Array se vsemi produkty k zobrazeni */
  private $pictures; /**< \brief Array s daty obrazku produktu */
  private $dbIdx; /**< pripojeni do db*/
  private $kategorievse; /**< @brief Array z tabulky _kategorie */
  private $rodokmen; /**< \brief Obsahuje PREDKY|POTOMKY urcite kategorie. Zalezi cim se naplni */
  private $categoryId; /**< \brief ID kagegorie, která se zpracovává*/
  

  public function __construct()
  {
    $this->dbIdx = new IndexDb();
  }

/**
 * Přetížená funkce. Automaticky bude volat hlavní metodu třídy  s argumenty, se kterými byla volána.
 *
  * @param array Pole polí s argumenty, se kterými byla volána neznámá metoda
  * @return 
 */
public function __call($funname,$arg)
{

    echo "<h3>Použita neznámá metoda: ",$funname,"</h3> Volaná s argumenty:";
    var_dump($arg);
    echo "<h3>Volání bez metody</h3>";

}  
/**
 * \brief Do slotu _productes_ vlozi seznam vsech produktu
 * @param unknown $param
 */
public function setProductes($param) 
{
	$this->productes = $param;
}
/**
 * @brief SETer pro nastaveni kategorie 
 *
 * @param int $idCat
 *
 * @return
 */
public function setCategoryId($idCat) 
{
	$this->categoryId = $idCat;
}
/**
 * @brief Vrací počet produktů nalezených v kategorii a v jejich potomcích
 *
 * @return int
 */
public function NumberOfProducts() 
{
	return count($this->productes);
}
/**
 * @brief Vstupni metoda, pro zajisteni zobrazeni dlazdic
 * 
 * \details Před zavoláním musí být vybrány všechny produkty,jejíchž dlaždice se mají zobrazovat - array \c $this->productes;
 * Pole se prochází a pro každý produkt se vytvoří dlaždice
 * 
 * Použití: 
 * \code
 * 	$tiles = new Tiles();
		$tiles->setCategoryId($idkat);
		$tiles->tilProductes();
		
		$pocet = $tiles->NumberOfProducts();

		echo "<div id='tiles-all'>"; // Rodičovský DIV pro dlaždice. Měl by mít  	display:flex;	flex-wrap:wrap;
		$tiles->tilShowTiles();
		echo"</div>"; // id='tiles-all'> Konec rodičovského divu);

 * \endcode
 * 
 * \todo Předělat tak, aby metoda vracela string, ve kterém info budou, nebo 
 * @param type name
 *
 * @return
 */
public function tilShowTiles() 
{
	// Nacist seznam obrazku k produktum
	$list_productes = $this->tilGetProductes();
	$this->pictures = $this->dbIdx->takepictures($list_productes); // nactecni seznamu obrazku

	foreach ($this->productes as $pr){
		$link = "<a href='index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pr['pk_produkt']."&amp;idkat=".$pr['id_kategorie']."'title='Zobrazení detailů produktu'>";
		
		echo "<div class=tile>";
		$idPic = $this->getPictureID($pr['pk_produkt']);
		//echo("<br>ID obr: $idPic");
	if (!PHONE){
		echo "<div class='tile-pic'>";
		
		echo $link;
		echo "<img src='vypsat_obr.php?id=".$idPic."&amp;vel=0'title='".$pr['nazev']."' alt='".$pr['nazev']."'>";
		echo "</a>";
		echo "</div>"; // class=tile-pic>";
	}
		echo "<h3 class='tile-nazev'>";
		echo $link;
		echo $pr['nazev'];
		echo "</a>";
		echo "</h3>";
	
		echo "<div class='tile-popkratky'>";
		echo $pr['popis_kratky'];
		echo "</div>";
	
		echo "<div class='tile-cena'>";
		echo $pr['cenakc'].",00&nbsp;Kč";
		echo "</div>";
		
		echo "<div class='tile-link'>";
		echo $link;
		echo "Detaily";
		echo "</a>";
		//echo "<button onClick=\"window.location.assign('index.php?mod=24&amp;met=dtDetail&amp;idprod=".$pr['pk_produkt']."&amp;idkat=".$pr['id_kategorie']."')\">";
		//echo "Detaily</button>";
		echo "</div>";
		
		echo "</div>"; // class=tile>";
	}
	
}
/**
 * @brief Prochazi pole s daty obrazku ($this->pictures) a vraci ID prvniho obrazek produktu. Pri nenalezeni vraci '0'
 *
 * @param int $idpr ID produktu, jehoz obrazek se hleda
 *
 * @return int >0 = ID obrazku
 * @return int 0 = obrazek nebyl nalezen
 */
private function getPictureID($idpr) 
{
	foreach ($this->pictures as $pic){
		if($pic['fk_produkt']==$idpr)
			return $pic['pk_obrazek'];
	}
	return 0;
}
/**
 * @brief Vyhleda a do _$this->productes_ ulozi produkty kategorie a jejich potomků
 *
 * @param int $this->productes
 * @return
 */
public  function tilProductes() 
{
		$this->tilPotomci($this->categoryId);
		//echo "<p>IdKat: $idkat<br>";
		//var_dump($this->rodokmen);
		//$prvni = true;
		$list_idkat = $this->categoryId;
		
		/*
		 * Pokut jsou nejaci dalsi potomci kategorie, zaradime je do listu
		 */
		if(count($this->rodokmen) > 0){
			foreach ($this->rodokmen as $k){
				$list_idkat .= ",". $k['id_kategorie'];
			}
		}
		//echo "<br>List_IdKat: $list_idkat  // ";
		$this->productes = $this->dbIdx->produktes($list_idkat); // Nacteni vsech produktu
	
}
/**
 * @brief Vraci vsechny potomky prochazene kategorie
 *
 * @param int $idkat ID kategorie, jejíž potomci se hledaji
 *
 * @return
 */
public function tilPotomci($idkat)
{
	$this->kategorievse = $this->dbIdx->kategorievse();
	$this->rodokmen = array();

	foreach ($this->kategorievse as $kat) {
		if ($kat['id_rodic']==$idkat) {
			$this->rodokmen[] = $kat;
			$this->tilPotomek($kat['id_kategorie']);
		}
	}
}
/**
 * @brief Rekurzi prochazi kategorie a hleda potomka. Vsechny potomky uklada od $this->rodokmen
 *
 * @param int $idkat ID rodicovske kategorie, jejiz potomky hledam
 *
 * @return
 */
private function tilPotomek($idkat)
{
	foreach ($this->kategorievse as $kat) {
		if ($kat['id_rodic'] == $idkat ) {
			$this->rodokmen[] = $kat;
			$this->tilPotomek($kat['id_kategorie']);
		}
	}
	return ;
}
/**
 * @brief Vytvori retezec pro frazi 'IN' v SELECTU pro vyber obrazku
 *
 * @param 
 *
 * @return string Retezce pro frazi 'IN' v sql selectu
 */
private function tilGetProductes() 
{
	//var_dump($this->productes);
	$prvni = true;
	$list = "";
	foreach ($this->productes as $prod) {
		if($prvni){
			$list = $prod['pk_produkt'];
			$prvni = false;
		}
		$list .= ",".$prod['pk_produkt'];
	}
	return $list;
}

}
?>


