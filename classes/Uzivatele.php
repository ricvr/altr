<?php 
/**
 * @file Uzivatele.php 
 *
 *  \brief    Soubor s definici třídy Uzivatele
 * \details   Detailnější popis
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      24.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 * \brief #3 - Třída pro správu uživatelů: \c Registrace, Přidělování práv, Editace dat.
 * \details Přihlašování, registrace, změna osobních údajů. Práva jsou uživatelům přidělována v metodach \c uzZmenaPrav() a \c uzPripojPrava() podle statusu uzivatele.
 * Práva pro jednotlivé statusy jsou uloženy v polích ve slotech třídy Uzivatele
 * \enddetails
 *
 *  \todo Použití potvrzovacího emailu při registraci nového uživatele. Až ve verzi, kdy bude třeba ověřené identity
 * \todo Na závěr udělat mazání uživatele, až budou známy všechny vazby -> \c uzDelete()
 */ 
class Uzivatele{
 
  private $nik; /**< \brief Přezdívka */
 private $jmeno;/**< \brief Jméno uživatele */
 private $prijmeni;/**< příjmení */
 private $muz;/**< Určení pohlaví muž|žena */
 private $verejne_jm;/**< Povolení zveřejňovat celé jméno */
 private $email;/**< email uživatele */
 private $heslo;/**< první heslo */
 private $heslo2;/**< Opakované zadání hesla pro kontrolau */
 private $pk_uzivatel;
 private $opravneni;
 private $table_opravneni;
 private $table_menu;
 private $table_ulohy;
 private $table_uzivatele;

 /** \brief Pole s ID uloh, ke kterým má opravnění uživatel \c Registrovany */
 private $arrRegistrovany = array(
   9  /* ulohy pro neregistrovane */
   ,IDULOHA_UZIVATEL_UPRAVA_UDAJU
 ); 

 /** \brief Pole s ID uloh, ke kterým má opravnění uživatel \c Overeny */
 private $arrOvereny = array(
   IDULOHA_PRODUKTY_VLOZENI
   ,IDULOHA_PRODUKTY_SEZNAM
   ,IDULOHA_PRODUKTY_EDITACE
 	 ,IDULOHA_PRODEJCI
 ); 

 /** \brief Pole s ID uloh, ke kterým má opravnění uživatel \c Vedouci */
 private $arrVedouci = array(
 ); 

 /** \brief Pole s ID uloh, ke kterým má opravnění uživatel \c Spravce */
 private $arrSpravce = array(
   IDULOHA_UZIVATELE_SEZNAM /* Seznam uzivatelu */
   ,IDULOHA_UZIVATEL_EDITACE /* Editace uzivatele */
   ,IDULOHA_NASTAVENI_HESLA /* Nastaveni hesla uzivatele */
   ,IDULOHA_UZIVATEL_NOVY /* Vytvoreni noveho uzivatele */
 		,IDULOHA_KATEGORIE_SPRAVA /**< brief Sprava kategorii, do kterych se radi produkty */
 ); 

 /** \brief Pole s ID uloh, ke kterým má opravnění uživatel \c Admin */
 private $arrAdmin = array(
   8
 ); 


  public function __construct()
  {
     
    if(isset($_SESSION['user']['pk_uzivatel'])){
      $this->pk_uzivatel = $_SESSION['user']['pk_uzivatel'];
    }else{
      $this->pk_uzivatel = 0;
    }
    $this->opravneni = new Opravneni($this->pk_uzivatel,session_id(),0);
    $this->table_opravneni = TABLE_PREFIX . "_opravneni";
    $this->table_menu = TABLE_PREFIX . "_menu";
    $this->table_ulohy = TABLE_PREFIX . "_ulohy";
    $this->table_uzivatele = TABLE_PREFIX . "_uzivatele";
  }

/**
  * \brief Vstupní metoda třídy
  * @param typ popis 
  * @return string vystup 
  */
public function index()
{
echo"<h3 class='nadpis-ulohy'>Správa uživatelů</h3>";
return;
}
/**
  * \brief Metoda zobrazující formulář pro přihlášení uživatele
  * @param 
  * @return  
  */
public function login()
{
echo"<h3 class='nadpis-ulohy'>Přihlášení uživatele</h3>";

return;
}
/**
  * \brief Zobrazení formuláře pro vyplnění údajů nového uživatele
  * \brief Je zpracován funkcí \c uzNovyUzivatel()
  * @param typ popis 
  * @return string vystup 
  */
public function uzNovy()
{
  $this->opravneni->setIdUloha(IDULOHA_UZIVATEL_NOVY);
  if($this->opravneni->kontrola_prav()< 0){
      throw new PrfException("K vytvoření nového uživatele nemáte oprávnění",10);
  }

$this->jsRegFormular();
echo"<h3 class='nadpis-ulohy'>Vytvoření nového uživatele</h3>";
$this->uzFormNovy();
return;
}

/**
 * \brief Zpracovává formulář \c registrace vytvořený metodou \c regformular
 *
  * @param string $nik Přezdívka 
  * @param string $jmeno Jméno uživatele 
  * @param string $prijmeni Příjmení uživatele
  * @param string $ukaz-jmeno \c checkbox povolující zveřejňování celého jména
  * @param string $email  Email uživatele, na který by měl být posílán potrvrzovací email
  * @param string $heslo Zadané heslo
  * @param string $heslo2 Kopie hesla zadaná uživatelem pro potvrzení
  * 
  * @return string vystup 
 */
public function uzNovyUzivatel($arg)
{
var_dump($arg);
extract($arg, EXTR_PREFIX_SAME, "wddx");

// kontrola povoleni zverejnovani jmena
if(!isset($verejne_jm)){
  $verejne_jm = 0;
}elseif (isset($verejne_jm)){
  $verejne_jm = 1;
}
// kontrola pohlavi muz|zena
if(!isset($muz)){
  $muz = 0;
}elseif (isset($muz)){
  $muz = 1;
}
/*
echo "<br>$nik";
echo "<br>$jmeno";
echo "<br>$prijmeni";
echo "<br>Zobrazování jména: $verejne_jm";
echo "<br>Muž: $muz";
echo "<br>$email";
echo "<br>$heslo";
echo "<br>$heslo2";
*/
$this->nik=$nik;
$this->jmeno = $jmeno;
$this->prijmeni = $prijmeni;
$this->verejne_jm = $verejne_jm;
$this->muz = $muz;
$this->email = $email;
$this->heslo = trim($heslo);
$this->heslo2 = trim($heslo2);

// Kontrola ukládaných dat
$errMessage = "";
$dataOK = false;
if(!$this->checknik($this->nik)){
  $errMessage .= "<br>Přezdívka '" . $this->nik . "' je již použita"; 
  $dataOK = false;
}else{
  $errMessage .= "<br>Přezdívka '" . $this->nik . "' je PŘIJATA."; 
  $dataOK = true;
}
if(!$this->checkHeslo()){
  $errMessage .= "<br>Chybné heslo. Nebylo zadáno, nebo byla chybně zadáno kontrolní heslo";
  $dataOK = false;
}else{
  $errMessage .= "<br>Heslo je vpořádku.";
  $dataOK = true;
}
if(!$dataOK){
  echo "<h4>$errMessage</h4>";
}else{
  $this->insNovyUzivatel();
}
echo "Uživatel $this->jmeno $this->prijmeni ($this->nik) byl zaregistrován.";
return;
}
/**
  * Kratky popis
  * @param typ popis 
  * @return string vystup 
 */
public function insNovyUzivatel()
{
  $hesloHash = password_hash($this->heslo, PASSWORD_DEFAULT);
  $q = "INSERT INTO ". $this->table_uzivatele ."(nik, jmeno, prijmeni,muz,verejne_jm, heslo, email, status)
    VALUES(
       :1 
      ,:2
      ,:3
      ,:4
      ,:5
      ,:6
      ,:7
      ,:8
    )"; 
  try{
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);
  $result = $stmt->execute(
     $this->nik
    ,$this->jmeno
    ,$this->prijmeni
    ,$this->muz
    ,$this->verejne_jm
    ,$hesloHash
    ,$this->email
    ,8
  );
  /* Zjisteni PK nove vlozeneho uzivatele*/
  $pkUser = $result->dbh->insert_id;
  $this->uzZmenaPrav($pkUser,STATUS_REGISTROVANY);
  $data = $result->result;
  }catch (Exception $e){
    trigger_error('Chyba při ukládání nového uživatele do db. '.$q);
    echo "Při registraci došlo chybě. Zkuste to prosím  později.";
    var_dump($e);

  }
}
/**
 * \brief Kontroluje správné zadání hesla.
 * \details Jestli je heslo zadané a jestli souhlasí obě kopie
 * Hesla načítá ze slotů.
 *
  * @param  
  * @return true Heslo je zadáno a potvrzení hesla je stejné jako originál
  * \return false Heslo nebylo zadáno, nebo potvrzení neodpovídá heslu 
 */
private function checkHeslo()
{
  if((strlen($this->heslo) > 0)
    && ($this->heslo === $this->heslo2)){
      return true;
    }
      return false;
}
/**
 * \brief Kontrola jedinečnosti 'nik' (přezdívky).
 * \details Je volána \c ajx_checknik.php 
 *
  * @param string $nik NIK, který se hledá v db popis 
  * @return boolen true NIK není použitý 
  * @return boolen fale NIK je použitý 
 */
public function checknik($nik)
{
  if(strlen(trim($nik))<=0){
    return false;
  }
$dbh = DB_Connect::newConnect();
$stmt = $dbh->prepare("SELECT nik FROM ". $this->table_uzivatele ." WHERE nik=:1");
$result = $stmt->execute($nik); // provedeni dotazu 
$data = $result->result;
//var_dump($result->result);
if($data->num_rows > 0){
  return false;
}else{
  return true;
}
$vysledek = $result->fetch();  // vytvoreni objektu s vracnymi hodnotami dotazu 
$vysledek->last();  // prechod na posledni zaznam dotazu 
echo "Hodnota ID: ",$vysledek->nik;    // vypsani pole 'id'
//echo "Text: ",$vysledek->text;   // vypsani pole 'text' 
  
   return false;
  }
/**
 *  \brief JS funkce pro formulář \c regformular.
 *
 * \details jQuery pro kontrolu jedinečnosti hesla
 *
  * \details fn \c kontrolahesla() pro porovnani opakovaného zadání hesla. Heslo musí být zadané a nesmí to být mezery.
  *
  * @param bez parametru  
  * @return boolen true - pokud opakované heslo souhlasí
  * @return boolen false - pokud opakované heslo nesouhlasí, nebo nebylo zadáno 
 */
private function jsRegFormular()
{
  echo "\n";
?>
  <script>

  $(document).ready(function(){
    $('#nik').change(function(){
      //alert("Změna");
      var nik = $("#nik").val();
      $.ajax({
        url:'ajax/ajx_checknik.php?nik='+nik
          ,success: function(data){
            $("#checknik").html(data);
          }
      });
    });
  });
  

  function kontrolahesla(){
  var heslo=registrace.heslo.value.trim();
  var heslopotvrzeni =registrace.heslo2.value.trim();
  //alert(heslo + "\n"+heslopotvrzeni + heslo.length);
  if(heslo.length <= 0){
    registrace.imgheslo.src="img/nic.gif";
    return false;
  }
  if(heslo == heslopotvrzeni){
    registrace.imgheslo.src="img/fajfka2.gif";
    return true;
  }else{
    registrace.imgheslo.src="img/krizek.gif";
    return false;
  }
  }

  function kontrolaemailu(){
    return true; // Kontrola emailu je zrusena
    var email=registrace.email.value.trim();
    //alert("emial:"+email+"\n"+email.length);
    if(email.length <= 0){
      return false;
    }else{
      return true;
    }


  }

  function kontrolanik(){
    var nik=registrace.nik.value.trim();
    if(nik.length <= 0){
      return false;
    } else{
      return true;
    }
  }

   function kontrolaodeslani(){ 
    if(!kontrolanik()){
      alert("Přezdnívka musí být vyplněna");
      return false;
    }else if(!kontrolaemailu()){
      alert("Email musí být zadán ve správném tvaru.");
      return false;
    } else if(!kontrolahesla()) {
      alert("Heslo musí být zadané.\nKontrola hesla musí být shodná s heslem.");
      return false;
    }else{
    //alert("Odeslání");
      return true;
    }
   }
  </script>
<?php
}
/**
  * \brief Zobrazení formuláře pro registraci nového uživatele
  * \details Je zpracován metodou \c uzNovyUzivatel
  * 
  */
private function uzFormNovy()
{
?>
<form name='registrace' method='post' action='sprava.php' onSubmit='return kontrolaodeslani();'>
<input type='hidden' name='mod' value='3'>
<input type='hidden' name='met' value='uzNovyUzivatel'>
<fieldset style="width:50%;margin:0 auto;">
<legend>Registrace nového uživatele</legend>

  <br>
  <label  for='nik'>Přezdívka *</label>
  <input type='text' name='nik' autofocus id='nik'>
 <span id='checknik'></span>
  <p>
  <label for='jmeno'>Jméno</label>
  <input type='text' name='jmeno'  id='jmeno'>
  
  <label for='prijmeni'>Příjmení</label>
  <input type='text' name='prijmeni'  id='prijmeni'>
  <br>
  <label for='verejne_jm'>Zobrazovat jméno a příjmení</label>
  <input type='checkbox' name='verejne_jm'  id='verejne_jm'>
  </p>
  <label for='muz'>Muž</label>
  <input type='checkbox' name='muz'  id='muz'>
  <p>
  <label for='email'>E-mail *</label>
  <input type='email' name='email' placeholder='např. jan.novak@email.cz' id='email'>
  <br>
  <label for='heslo'>Heslo *</label>
  <input type='password' name='heslo' placeholder='bezpečné heslo' id='heslo'autocomplete='off' onBlur='kontrolahesla();'>
  </p>
  <br>
  <label for='heslo2'>Potvrzení heslo *</label>
  <input type='password' name='heslo2' placeholder='Opakovat heslo' id='heslo2' autocomplete='off' onKeyUp='kontrolahesla();'> <span id='kontrolahesla'><img src="img/nic.gif" name="imgheslo" class='ikona'></span>
<p align='right'>
  <input type='submit' value='Uložit'>
</p>
 

</fieldset>
</form>
<?php
}
/**
 * \brief Načte prava přihlášeného uživatele z db
 * \details Načítá z tabulek \c prf_opravneni a \c prf_ulohy údaje o úlohách, ke kterým má uživatel práva. Vše vrací v poli.
 *
  * @param int $pk_uzivatel Primární klíč uživatele 
  * @return array Pole, kde každá položka obsahuje číslo úlohy 
 */
public function dejPravaUzivatele($pk_uzivatel)
{
//echo "<br>PK Uživatele: $pk_uzivatel";
  $q = "SELECT o.fk_uzivatel
              ,u.uloha
              ,u.idmenuskup
              ,m.skupina
              ,u.idclass
              ,u.iduloha
              ,u.link_text
              ,u.link
        FROM ". $this->table_opravneni ." o
        	LEFT JOIN ". $this->table_ulohy ." u ON o.iduloha=u.iduloha
            LEFT JOIN ". $this->table_menu ." m ON u.idmenuskup=m.pk_skupmenu
        WHERE o.fk_uzivatel = $pk_uzivatel
        ORDER BY m.poradi,u.idmenuskup,u.iduloha";
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);
  $result = $stmt->execute();
  $data = $result->fetchall_assoc();
  return $data;
  //var_dump($data);
}
/**
  * \brief Zobrazuje seznam uživatelů
  *
  * \details
  * @param typ popis
  * @return string vystup
 */
public function uzSeznam($arg="")
{ // BEGIN
  $this->opravneni->setIdUloha(IDULOHA_UZIVATELE_SEZNAM);
  if($this->opravneni->kontrola_prav()< 0){
    throw new PrfException("K seznamu uživatelů nemáte přístup",10);
  }
  extract($arg); // $iduziv
  if(!isset($iduziv) || $iduziv < 0){
    $iduziv = 0;
  }
  $q = "SELECT
          pk_uzivatel
          , nik, jmeno
          , prijmeni, muz
          , verejne_jm, email
          , heslo, status
          , dins, d_deaktiv
        FROM ". $this->table_uzivatele ." 
        ORDER BY nik";
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);
  $result = $stmt->execute();
  $uziv = $result->fetchall_assoc();
  /* Je-li volana editace uzivatele, vypise se formular s daty uzivatele */
    if($iduziv > 0){
      foreach($uziv as $row){
        if($row['pk_uzivatel']==$iduziv)
          $this->uzFormEdit($row);
      }
    }
  /* Vypsání hlavičky seznamu */
    echo "<div class='uzivatel'>";
    echo "<div class='uziv-ikony uziv-zahlavi'>";
    echo "&nbsp;";
    echo "</div>";
    echo "<div class='uziv-nik uziv-zahlavi'>";
    echo "Přezdívka";
    echo "</div>";
    echo "<div class='uziv-jmeno uziv-zahlavi'>";
    echo "Jméno";
    echo "</div>";
    echo "<div class='uziv-email uziv-zahlavi'>";
    echo "Email";
    echo "</div>";
    echo "<div class='uziv-status uziv-zahlavi'>";
    echo "Status";
    echo "</div>";
    echo "<hr style='clear:both;'>";
    echo "</div>"; //class=uzivatel
  foreach($uziv as $row){
    echo "<div class='uzivatel'>";
    echo "<div class='uziv-ikony'>";
    echo "<a href='sprava.php?mod=3&met=uzDelete&amp;iduziv=".$row['pk_uzivatel']."'>";
    echo "<img src='img/krizek.gif'class='ikona'title='Odstranit uživatele'>";
    echo "</a>";
    echo " ";
    echo "<a href='sprava.php?mod=3&met=uzSeznam&iduziv=".$row['pk_uzivatel']."'>";
    echo "<img src='img/tuzka.gif'class='ikona'title='Editace uživatele'>";
    echo "</a>";
    //echo "Ikony";
    echo "</div>";
    echo "<div class='uziv-nik'>";
    echo $row['nik'];
    echo "</div>";
    echo "<div class='uziv-jmeno'>&nbsp;";
    echo $row['jmeno'] . " " . $row['prijmeni'];
    echo "</div>";
    echo "<div class='uziv-email'>&nbsp;";
    echo $row['email'];
    echo "</div>";
    echo "<div class='uziv-status'>&nbsp;";
    echo Options::$arrStatusy[$row['status']];
    echo "</div>";
    echo "<hr style='clear:both;'>";
    echo "</div>"; //class=uzivatel
  }

} // END function 
/**
  * \brief Metoda pro smazání uživatele
  * @param int $iduziv ID odstraňovaného uživatele 
  * @return void 
 */
public function uzDelete($arg)
{
  echo "<h3 class='nadpis-ulohy'>Smazání uživatele</h3>";
  extract($arg);
  //var_dump($arg);
  echo "<br>ID uživatele:".$iduziv;
  echo "<br/>Funkce bude dokončena až jako poslední, kvůli kontrole závislostí  odstraňovaného uživatele v db";
    return;
}
/**
  * \brief Vytvoří html nabidku SELECT statusů
  * @param INT $status Číslo statusu
  *
  * @return string HTML kód nabídky SELECT
 */
private function statusNabidka($status)
{
  echo "<select name='status'>";
  foreach(Options::$arrStatusy as $kod => $nazev){
    echo "<option value='$kod'";
    if($status == $kod) {
      echo " selected='selected' ";
    }
    echo ">" . $nazev . "</option>";
  }
  echo "</select>";
}
/**
 * \brief Zobrazí formulář s daty editovaného uživatele. Zpracování probíhá v metodě \c uzUpdUzivatel
  * @param array $arrUsr Pole s daty uživatele 
  * @return void
  */
private function uzFormEdit($arrUsr)
{
  $this->opravneni->setIdUloha(IDULOHA_UZIVATEL_EDITACE);
  if($this->opravneni->kontrola_prav()< 0){
    throw new PrfException("K editaci uživatele nemáte přístup",10);
  }
  //var_dump($arrUsr);
  extract($arrUsr);
  //$this->jsRegFormular(); /* JS funkce pro formular */
?>
<script>
  function kontrolahesla(){
   
  var heslo=nastavheslo.heslo.value.trim();
  var heslopotvrzeni =nastavheslo.heslo2.value.trim();
  //alert(heslo + "\n"+heslopotvrzeni + heslo.length);
  if(heslo.length <= 0){
    nastavheslo.imgheslo.src="img/nic.gif";
    return false;
  }
  if(heslo == heslopotvrzeni){
    nastavheslo.imgheslo.src="img/fajfka2.gif";
    return true;
  }else{
    nastavheslo.imgheslo.src="img/krizek.gif";
    return false;
  }
    
  }
</script>
<form name='edit' method='post' action='sprava.php'onSubmit='return confirm("Uložit změny?");'>
<input type='hidden'name='mod'value='3'>
<input type='hidden'name='met'value='uzUpdUzivatel'>
<input type='hidden'name='iduziv'value='<?php echo $pk_uzivatel; ?>'>
<fieldset style="width:50%;margin:0 auto;">
<legend>Editace uživatele <?php echo $jmeno." ".$prijmeni; ?></legend>

  <br>
  <label  for='nik'>Přezdívka </label>
  <input type='text' name='nik' id='nik'value="<?php echo $nik; ?>"readonly>

  <br/>
  <label for='status'>Status</label>
  <!--input type='text' name='status'  id='status'value="<?php //echo Options::$arrStatusy[$status]; ?>"-->
  <?php echo $this->statusNabidka($status); ?>
  <p>
  <label for='jmeno'>Jméno</label>
  <input type='text' name='jmeno'  id='jmeno'value="<?php echo $jmeno; ?>">
  
  <br/>
  <label for='prijmeni'>Příjmení</label>
  <input type='text' name='prijmeni'  id='prijmeni'value="<?php echo $prijmeni; ?>">
  <br>
  <label for='verejne_jm'>Zobrazovat jméno a příjmení</label>
  <input type='checkbox' name='verejne_jm'  id='verejne_jm' <?php if($verejne_jm > 0) echo "CHECKED"; ?>>
  </p>
  <label for='muz'>Muž</label>
  <input type='checkbox' name='muz'  id='muz' <?php if($muz>0) echo "CHECKED"; ?>>
  <p>
  <label for='email'>E-mail </label>
  <input type='email' name='email' placeholder='např. jan.novak@email.cz' id='email'value="<?php echo $email; ?>">
 
<p align='right'>
  <input type="button" value="Storno" onClick="location.replace('sprava.php?mod=3&met=uzSeznam');">
 &nbsp; 
  <input type='submit'value='Uložit změny'>
</p>
 

</fieldset>
</form>
<form name='nastavheslo' method='post' action='sprava.php'onSubmit='return confirm("Uložit heslo?");'>
<input type='hidden'name='mod'value='3'>
<input type='hidden'name='met'value='uzNastavHeslo'>
<input type='hidden'name='pkuser'value='<?php echo $pk_uzivatel; ?>'>
<fieldset style="width:50%;margin:0 auto;">
<legend>Nastaveni nového hesla uživatele <?php echo $jmeno." ".$prijmeni; ?></legend>

  <label for='heslo'>Heslo </label>
  <input type='password' name='heslo' placeholder='bezpečné heslo' id='heslo'autocomplete='off'onBlur='kontrolahesla();'>
  </p>
  <br>
  <label for='heslo2'>Potvrzení heslo *</label>
  <input type='password' name='heslo2' placeholder='Opakovat heslo' id='heslo2'autocomplete='off'onKeyUp='kontrolahesla();'> <span id='kontrolahesla'><img src="img/nic.gif"name="imgheslo" class='ikona'></span>
<p align='right'>
  <input type="button" value="Storno" onClick="location.replace('sprava.php?mod=3&met=uzSeznam');">
 &nbsp; 
  <input type='submit'value='Uložit heslo'>
</p>
 

</fieldset>
</form>
<?php
  return; 
} // konec funkce formuláře
/**
  * \brief Vytvoří datovou část sql příkazu INSERT pro vložení opravnění
  * @param int $iduziv ID uzivatele
  * @param array $arrPrava Pole s ID úloh, 
  * @return string vystup 
 */
private function uzPripojPrava($iduziv,$arrPrava)
{
  $data = "";
  if(count($arrPrava) > 0){
    $prvni = true;
    foreach($arrPrava as $iduloha){
      if(!$prvni){
        $data.=",";
      }else{
        $prvni = false;
      }
      $data.= "(";
      $data.="$iduziv,$iduloha";
      $data.=")";
    }
  }
  return $data;
}
/**
  * \brief Na základě statusu provede přidělení práv uživateli.
  * @param int $status Přidělený status uživateli 
  * @return 
 */
private function uzZmenaPrav($iduziv, $status)
{
  $qDel = "DELETE FROM ". $this->table_opravneni ." WHERE fk_uzivatel = :1"; 
  $qIns = "INSERT INTO ". $this->table_opravneni ."(fk_uzivatel,iduloha)VALUES";
  
  if($status >= STATUS_REGISTROVANY && (count($this->arrRegistrovany)>0)){
    $qIns.= $this->uzPripojPrava($iduziv,$this->arrRegistrovany);
  }
  
  if($status >= STATUS_OVERENY && (count($this->arrOvereny)>0)){
    $qIns.= "," . $this->uzPripojPrava($iduziv,$this->arrOvereny);
  }
  
  if($status >= STATUS_VEDOUCI && (count($this->arrVedouci)>0)){
    $qIns.= "," .  $this->uzPripojPrava($iduziv,$this->arrVedouci);
  }
  
  if($status >= STATUS_SPRAVCE && (count($this->arrSpravce)>0)){
    $qIns.= "," .  $this->uzPripojPrava($iduziv,$this->arrSpravce);
  }
  
  if($status >= STATUS_ADMIN && (count($this->arrAdmin)>0)){
    $qIns.= "," .  $this->uzPripojPrava($iduziv,$this->arrAdmin);
  }

 // echo "<br>Dotaz: ".$qIns;
try{
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($qDel);
  $result = $stmt->execute($iduziv);
}
catch(Exception $e){
  throw new PrfException("Změna oprávnění.\nDošlo k chybě při odstraňování starých práv. " . $e->getMessage(),10);
}
try{
  if($status >= STATUS_REGISTROVANY){
    $stmtIns = $dbh->prepare($qIns);
    $resultIns = $stmtIns->execute();
  }
}
catch(Exception $e){
  throw new PrfException("Změna oprávnění.\nDošlo k chybě při vkládání práv. " . $e->getMessage(),10);
}
}
/**
  * \brief Zpracovává formulář z metody \c uzFormEdit(). Provádí uložení do db
  * @param array $arg Obsahuje klíče:  nik jmeno prijmeni status verejne_jm muz email heslo heslo2
  * @return  
  */
public function uzUpdUzivatel($arg)
{
  $this->opravneni->setIdUloha(IDULOHA_UZIVATEL_EDITACE);
  if($this->opravneni->kontrola_prav()< 0){
    throw new PrfException("K ukládání editovaných dat uživatele nemáte přístup",10);
  }
//var_dump($arg);
extract($arg);/* nik jmeno prijmeni verejne_jm muz email heslo heslo2
  iduziv,nik,status, jmeno,prijmeni, verejne_jm, muz, email, heslo, heslo2,  
 */
  if(isset($muz)){
    $muz=1;
  }  else{
    $muz=0;
  }
if (isset($verejne_jm)){
  $verejne_jm = 1;
}else{
  $verejne_jm = 0;
}
  $q = "UPDATE ". $this->table_uzivatele ." SET
        jmeno = :1,
        prijmeni = :2,
        muz = :3,
        verejne_jm = :4,
        email = :5,
        status = :6
        WHERE pk_uzivatel = :7";
try{ 
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);
  $result = $stmt->execute($jmeno, $prijmeni, $muz, $verejne_jm, $email, $status, $iduziv);
  $data = $result->result;
}catch(Exception $e){
  if (LADENI) var_dump($e);
   throw new PrfException("Nepodařilo se uložit data uživatele",10);
}
try{
  $this->uzZmenaPrav($iduziv,$status);
}catch(Exception $e){
  if (LADENI) var_dump($e);
   throw new PrfException("Nepodařilo se změnit práva uživatele",10);
}
header("Location:" . $_SERVER['HTTP_REFERER']);
}
/**
  * \brief Obsluhuje formular z 'uzFormEdit()'.Ulozi nove heslo nastavene administratorem.
  * @param  $pkuser; $heslo; $heslo2;
  * @return string vystup 
  */
public function uzNastavHeslo($arg)
{
  $this->opravneni->setIdUloha(IDULOHA_NASTAVENI_HESLA);
  if($this->opravneni->kontrola_prav()< 0){
      throw new PrfException("Ke změně hesla uživatele nemáte oprávnění",10);
  }
  //var_dump($arg);
  //var_dump($_SERVER);
  extract($arg);/* $pkuser; $heslo; $heslo2; */
  $this->heslo = $heslo;
  $this->heslo2 = $heslo2;
  try{
  if(!$this->checkHeslo()){
    echo"<div class='info'>";
    echo "Heslo nebylo zadáno nebo druhé zadání hesla neodpovída prvnímu.";
    echo"</div>";
  }
  $q = "UPDATE ". $this->table_uzivatele ." SET
        heslo = :1
        WHERE pk_uzivatel = :2"; 
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);
  $hesloHash = password_hash($this->heslo, PASSWORD_DEFAULT);
  $result = $stmt->execute($hesloHash,$pkuser);
  $data = $result->result;
  }catch(Exception $e){
    echo"<div class='varovani'>";
    echo "Nepodařilo se uložit změněné heslo";
    echo"</div>";
    throw new Exception($e);
  }
  echo"<div class='info'>";
  echo "Heslo uloženo.<br>";
  echo "<button onClick='location.replace(\"sprava.php?mod=3&met=uzSeznam&iduziv=".$pkuser."\");'>Pokračovat</button>";
  echo"</div>";

}

}
?>


