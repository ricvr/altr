<?php 
/**
 * @file Uzivatelzmeny.php 
 *
 *  \brief    #5 - Soubor s definici třídy pro uživatele, aby si mohli editovat vlastní údaje
 * \details   Úprava emailu, jména, příjmení, hesla, ...
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      1.1.2016
 *  \pre     
 *  \bug     
 *  \warning 
 *  \copyright RiC
 *  \todo Změna hesla
 *  \todo uprava udajů uživatelem 
 *
 * Libovolně dlouhý podrobný popis
 *
 */ 

/**
 *  \brief    #5 - Třída pro uživatele, aby si mohli editovat vlastní údaje
 * \details   Úprava emailu, jména, příjmení, hesla, ...
 */ 
class Uzivatelzmeny{
  private $pk_uzivatel;
  private $opravneni;
  private $table_uzivatele;
  

  public function __construct()
  {
    $this->table_uzivatele = TABLE_PREFIX . "_uzivatele";
    $this->pk_uzivatel = $_SESSION['user']['pk_uzivatel'];
    $this->opravneni = new Opravneni($this->pk_uzivatel,session_id(),0);
  $this->opravneni->setIdUloha(IDULOHA_UZIVATEL_UPRAVA_UDAJU);
  if($this->opravneni->kontrola_prav()< 0){
    throw new PrfException("Nemáte přístup k úpravě vlastních údajů.",10);
  }
    
  }

/**
 * \brief Vstupni metoda tridy. VYtvori nabidku, ze ktere se vybira co chce uzivatel menit
 *
  * @param *** 
  * @return *** 
 */
  public function uuVlastniEditace()
  {
    echo"<div class='nadpis-ulohy'>";
    echo "Nastavení";
    echo"</div>";

    echo "<p align='center'>";
    echo "<span style='margin:0 10%;'><a href='?mod=5&met=uuFrmUdaje&pk=".$this->pk_uzivatel . "'>Změna údajů</a></span>";
    echo "<span style='margin:0 10%;'><a href='?mod=5&met=uuZmenaHesla&pk=".$this->pk_uzivatel . "'>Změna hesla</a></span>";
    echo "</p>";
    
  }  
  /**
   * \brief Vytvori formular pro upravu udaju uzivatelem. Vola se z 'uzVlastniEditace()'. Zpracovani v metode 'uuUlozZmeny()'.
   *
    * @param typ popis 
    * @return string vystup 
    */
public function uuFrmUdaje($arg)
{
    echo"<div class='nadpis-ulohy'>";
    echo "Úprava údajů";
    echo"</div>";
    //var_dump($arg);
    extract($arg); /* $pk */
    $user = $this->uuDataUzivatele($pk);
?>
<form name='uprava' method='post' action='sprava.php'onSubmit='return kontrolaodeslani();'>
<input type='hidden'name='mod'value='5'>
<input type='hidden'name='met'value='uuUlozZmeny'>
<input type='hidden'name='pkuser'value='<?php echo $pk; ?>'>
<fieldset style="width:50%;margin:0 auto;">
<legend>Změna údajů</legend>

  <br>
  <label  for='nik'>Přezdívka *</label>
  <?php echo "<strong>",$user['nik'],"</strong>" ?>
  <p>
  <label for='jmeno'>Jméno</label>
  <input type='text' name='jmeno'  id='jmeno'value='<?php echo $user['jmeno'] ?>'>
  
  <label for='prijmeni'>Příjmení</label>
  <input type='text' name='prijmeni'  id='prijmeni'value='<?php echo $user['prijmeni'] ?>'>
  <br>
  <label for='verejne_jm'>Zobrazovat jméno a příjmení</label>
  <input type='checkbox' name='verejne_jm'  id='verejne_jm'<?php if($user['verejne_jm']==1)echo "CHECKED"; ?>>
  </p>
  <label for='muz'>Muž</label>
  <input type='checkbox' name='muz'  id='muz'<?php if($user['muz']==1)echo "CHECKED" ?>>
  <p>
  <label for='email'>E-mail </label>
  <input type='email' name='email' placeholder='např. jan.novak@email.cz' id='email'value='<?php echo $user['email'] ?>'>
  </p>
 
<p align='right'>
  <input type='button'value='Storno'onClick='location.replace("sprava.php")'>
 &nbsp; &nbsp;
  <input type='submit'value='Uložit změny'>
</p>
</fieldset>
</form>
<?php    
}
/**
  * \brief Ulozi zmeny provede ve formulari 'uuVlastniEditace()'
  * @param $pkuser; $jmeno; $prijmeni; $verejne_jm; $muz; $email; 
  * @return string vystup 
 */
public function uuUlozZmeny($arg)
{

$this->opravneni->setIdUloha(IDULOHA_UZIVATEL_UPRAVA_UDAJU);
if($this->opravneni->kontrola_prav()< 0){
    throw new PrfException("Ke změně údajů nemáte oprávnění.",10);
}
//var_dump($arg);
extract($arg); /* $pkuser; $jmeno; $prijmeni; $verejne_jm; $muz; $email; */

/* Kontrola a pripadne nastaveni formularovych prvku CHECKBOX */
if(!isset($verejne_jm)){
  $verejne_jm = 0;
}else{
  $verejne_jm = 1;
}
if(!isset($muz)){
  $muz = 0;
}else{
  $muz = 1;
}

$errLine = __LINE__;
$q = "UPDATE " . $this->table_uzivatele . " SET
            jmeno = :1
            ,prijmeni = :2
            ,muz = :3
            ,verejne_jm = :4
            ,email = :5
            WHERE pk_uzivatel = :6"; 
try{
$dbh = DB_Connect::newConnect();
$stmt = $dbh->prepare($q);
$errLine = __LINE__;
$result = $stmt->execute($jmeno,$prijmeni,$muz,$verejne_jm,$email,$pkuser);
$errLine = __LINE__;
echo"<div class='info'>";
echo "Údaje byly uloženy.<br>Změny se projeví až po příštím přihlášení.";
echo"</div>";
}catch(Exception $e){
  if(LADENI){
    echo "<p>Ukladani změn uživatele:ř.č. $errLine<br>";
    var_dump($e);
  }
  throw new PrfException("Nepodařilo se uložit provedené změny v údajích uživatele",10);
}

}
/**
  * \brief Vklada JS funkci pro kontrolu hesla
  * @param
  * @return 
 */
private function jsKontrolaHesla()
{
?> 
<script> 
 function kontrolahesla(){
  var heslo=zmenahesla.heslo.value.trim();
  var heslopotvrzeni =zmenahesla.heslo2.value.trim();
  //alert(heslo + "\n"+heslopotvrzeni + heslo.length);
  if((heslo.length <= 0)&&(heslopotvrzeni <=0)){
    zmenahesla.imgheslo.src="img/nic.gif";
    return false;
  }
  if(heslo == heslopotvrzeni){
    zmenahesla.imgheslo.src="img/fajfka2.gif";
    return true;
  }else{
    zmenahesla.imgheslo.src="img/krizek.gif";
    return false;
  }
  }
 function kontrolaodeslani(){
    if(!kontrolahesla()) {
      alert("Heslo musí být zadané.\nKontrola hesla musí být shodná s heslem.");
      return false;
    }
 }
</script> 
<?php  
}
/**
  * \brief Formular pro zmenu hesla uzivatelem. Vola se z 'uzVlastniEditace()'. Zpracovává metoda 'uuUlozitNoveHeslo()' 
  * @param typ popis 
  * @return string vystup 
  */
public function uuZmenaHesla($arg)
{
  $this->jsKontrolaHesla();
    echo"<div class='nadpis-ulohy'>";
    echo "Změna hesla";
    echo"</div>";
    //var_dump($arg);
    extract($arg);/* $pk */
?>
<form name='zmenahesla' method='post' action='sprava.php'onSubmit='return kontrolaodeslani();'>
<input type='hidden'name='mod'value='5'>
<input type='hidden'name='met'value='uuUlozitNoveHeslo'>
<input type='hidden'name='pkuser'value='<?php echo $pk; ?>'>
<fieldset style="width:50%;margin:0 auto;">
<legend>Změna hesla uživatele "<?php echo "<strong>",ucfirst(strtolower($_SESSION['user']['nik'])),"</strong>" ?>"</legend>
 <!-- <label  for='nik'>Přezdívka *</label> -->
  <?php //echo "<strong>",$_SESSION['user']['nik'],"</strong>" ?>
  <p>
  <br>
  <label for='heslo'>Staré heslo </label>
  <input type='password' name='stareheslo' id='stareheslo'>
  <br>
  <label for='heslo'>Nove heslo </label>
  <input type='password' name='noveheslo' placeholder='bezpečné heslo' id='heslo'autocomplete='off'onBlur='kontrolahesla();'onKeyUp='kontrolahesla();'>
  </p>
  <br>
  <label for='heslo2'>Opakování nového hesla </label>
  <input type='password' name='heslo2' placeholder='Opakovat heslo' id='heslo2'autocomplete='off'onKeyUp='kontrolahesla();'> 
<span id='kontrolahesla'><img src="img/nic.gif"name="imgheslo" class='ikona'></span>
<p align='right'>
  <input type='submit'value='Uložit nové heslo'>
</p>
 

</fieldset>
</form>
<?php    
}
/**
  * \brief Zpracovava formular pro zmenu hesla, ktery se vytvari v uuZmenaHesla()
  * @param  $pkuser; $stareheslo; $noveheslo; $heslo2;
  * @return 
  */
public function uuUlozitNoveHeslo($arg)
{
  $this->opravneni->setIdUloha(IDULOHA_UZIVATEL_UPRAVA_UDAJU);
  if($this->opravneni->kontrola_prav()< 0){
      throw new PrfException("Nemáte opravnění ke změně svého hesla",10);
  }
  //var_dump($arg);
  extract($arg); /* $pkuser; $stareheslo; $noveheslo; $heslo2; */
  //Načtení stávajících dat uživatele
  $arrUzivatel = $this->uuDataUzivatele($pkuser);
  //var_dump($arrUzivatel);
  //
  //Kontrola starého hesla
  $stareHesloHash = password_hash($stareheslo,PASSWORD_DEFAULT);
  if(!password_verify($stareheslo,$arrUzivatel['heslo'])){
    echo"<div class='varovani'>";
    echo "Špatné staré heslo.<br>Změna hesla nebyla provedena";
    echo"</div>";
    return;
  }
  //Kontrola nového hesla
  if($noveheslo!==$heslo2){
    echo"<div class='varovani'>";
    echo "Nebylo ověřeno nové heslo.<br>Kontrolní zadání hesla neodpovídá novému heslu.";
    echo"</div>";
    return;
  }
  //Uložení nového hesla
  $q = "UPDATE " . $this->table_uzivatele . " SET heslo=:1 WHERE pk_uzivatel=:2"; 
  $newHesloHash = password_hash($noveheslo,PASSWORD_DEFAULT);
  try{
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);
  $result = $stmt->execute($newHesloHash,$pkuser);
  $data = $result->result;

  echo"<div class='info'>";
  echo "Změna hesla byla úspěšně provedena.";
  echo"</div>";
  }catch(Exception $e){
    //var_dump($e);
    throw new PrfException("Nepodařilo se uložit změnu hesla",10);
  }
}
/**
  * \brief Vraci zaznam jednoho uzivatele  z tabulky \c prf_uzivatele
  * @param int $pkusr
  * @return array Cely zaznam uzivatele vystup 
  */
private function uuDataUzivatele($pk)
{
  $q = "SELECT pk_uzivatel, nik, jmeno, prijmeni, muz, verejne_jm, email,heslo, status
        FROM " . $this->table_uzivatele . " WHERE pk_uzivatel=:1"; 
  $dbh = DB_Connect::newConnect();
  $stmt = $dbh->prepare($q);
  $result = $stmt->execute($pk);
  $data = $result->fetch_assoc();
  return $data;

}
}
?>


