-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `altra` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci */;
USE `altra`;

DROP TABLE IF EXISTS `altr_login`;
CREATE TABLE `altr_login` (
  `pk_message` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL COMMENT 'Cislo,kod chyby',
  `message` text COLLATE utf8_czech_ci COMMENT 'Text zpravy',
  `file` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL COMMENT 'Soubor, kde hlaska vznika',
  `line` int(11) DEFAULT NULL COMMENT 'Cislo radku',
  `d_ins` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`pk_message`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `altr_menu`;
CREATE TABLE `altr_menu` (
  `pk_skupmenu` int(11) NOT NULL,
  `skupina` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(2550) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(11) NOT NULL,
  PRIMARY KEY (`pk_skupmenu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Názvy jednotlivých skupin v menu';

INSERT INTO `altr_menu` (`pk_skupmenu`, `skupina`, `popis`, `poradi`) VALUES
(1,	'Domu',	'Úvodní menu pro všechny uživatele',	10),
(2,	'Produky',	'Veškeré práce spojené s produky, vkládání a úpravy.',	20),
(3,	'Sprava',	'Správa stránek podle oprávnění',	30),
(4,	'Administrace',	'Přístup pro administrátora.',	40);

DROP TABLE IF EXISTS `altr_opravneni`;
CREATE TABLE `altr_opravneni` (
  `pk_opravneni` int(11) NOT NULL AUTO_INCREMENT,
  `fk_uzivatel` int(11) NOT NULL,
  `iduloha` int(11) NOT NULL,
  `dins` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dval` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_opravneni`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Vazebni tabulka pro nastaveni opravneni uzivatelu';

INSERT INTO `altr_opravneni` (`pk_opravneni`, `fk_uzivatel`, `iduloha`, `dins`, `dval`) VALUES
(22,	1,	13,	'2016-05-29 19:22:28',	NULL),
(21,	1,	12,	'2016-05-29 19:22:28',	NULL),
(20,	1,	10,	'2016-05-29 19:22:28',	NULL),
(19,	1,	4,	'2016-05-29 19:22:28',	NULL),
(18,	1,	11,	'2016-05-29 19:22:28',	NULL),
(17,	1,	9,	'2016-05-29 19:22:28',	NULL),
(10,	2,	11,	'2016-05-29 19:10:50',	NULL),
(9,	2,	9,	'2016-05-29 19:10:50',	NULL),
(23,	1,	8,	'2016-05-29 19:22:28',	NULL),
(35,	3,	11,	'2016-05-29 20:44:59',	NULL),
(34,	3,	9,	'2016-05-29 20:44:59',	NULL);

DROP TABLE IF EXISTS `altr_prihlaseni`;
CREATE TABLE `altr_prihlaseni` (
  `pk_prihlaseni` int(11) NOT NULL AUTO_INCREMENT,
  `fk_uzivatel` int(11) NOT NULL,
  `id_uloha` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `id_session` char(200) COLLATE utf8_czech_ci NOT NULL,
  `d_akce` int(11) NOT NULL,
  PRIMARY KEY (`pk_prihlaseni`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `altr_ulohy`;
CREATE TABLE `altr_ulohy` (
  `pk_uloha` int(11) NOT NULL AUTO_INCREMENT,
  `uloha` varchar(30) COLLATE utf8_czech_ci DEFAULT NULL,
  `idmenuskup` int(11) DEFAULT NULL,
  `idclass` int(11) NOT NULL DEFAULT '0',
  `iduloha` int(11) NOT NULL,
  `link_text` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `link` varchar(300) COLLATE utf8_czech_ci NOT NULL,
  `pozn` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`pk_uloha`),
  UNIQUE KEY `iduloha_index` (`iduloha`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `altr_ulohy` (`pk_uloha`, `uloha`, `idmenuskup`, `idclass`, `iduloha`, `link_text`, `link`, `pozn`) VALUES
(1,	'Ověřování uživatelů',	3,	3,	4,	'Seznam uživatelů',	'sprava.php?mod=3&amp;met=uzSeznam',	''),
(2,	'Správa uživatelů',	4,	0,	8,	'Správa uživatelů',	'',	''),
(3,	'Úvodní stránka',	1,	0,	9,	'Úvod',	'sprava.php',	''),
(4,	'Editace uživatele',	0,	0,	10,	'---',	'---',	'Volá se ze seznamu uživatelů'),
(5,	'Změna osobních údajů',	NULL,	5,	11,	'',	'',	' Zmena udaju uzivatelem'),
(6,	'Změna hesla uživateli',	NULL,	3,	12,	'',	'',	'Nastavení nového hesla uživatele správcem nebo administrátorem'),
(7,	NULL,	3,	0,	13,	'Vytvořit nového uživatele',	'sprava.php?mod=3&amp;met=uzNovy',	'');

DROP TABLE IF EXISTS `altr_uzivatele`;
CREATE TABLE `altr_uzivatele` (
  `pk_uzivatel` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `prijmeni` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `muz` int(1) NOT NULL DEFAULT '0',
  `verejne_jm` int(1) NOT NULL DEFAULT '0',
  `email` varchar(60) COLLATE utf8_czech_ci NOT NULL,
  `heslo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `dins` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `d_deaktiv` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_uzivatel`),
  UNIQUE KEY `nik_UNIQUE` (`nik`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `altr_uzivatele` (`pk_uzivatel`, `nik`, `jmeno`, `prijmeni`, `muz`, `verejne_jm`, `email`, `heslo`, `status`, `dins`, `d_deaktiv`) VALUES
(1,	'admin',	'Administrator',	'Hlavní',	0,	0,	'',	'$2y$10$cPo7MFLYl67Mrzy6Q8bVWODhW.ufXGc54amvH0558OUnivjCtlraq',	2048,	'2016-05-29 14:57:35',	NULL),
(2,	'pavel',	'Pavel',	'Pavelkovič',	0,	0,	'jan.novak@sen.cz',	'$2y$10$AHnnkJNGmcZHomxa1D9RaePGHokbGpBhvm5ojmZMV7CRPLEkkmXU2',	32,	'2016-05-29 17:40:56',	NULL),
(3,	'jirka',	'',	'',	0,	0,	'',	'$2y$10$4fwxABX1NTqICLpbcaKiHOYKV60azuzaN/DBE7MOwDRxgQBOaTivm',	8,	'2016-05-29 20:32:19',	NULL);

-- 2016-05-30 17:52:21
