<?php 
/**
 * 
 * @file index.php 
 *
 *  \brief     Úvodní skript projektu. Zde vše začíná i končí :-)
 * \details V tomto souboru jsou volány třídy, které budou provádět jednotlivé funkce a vytvářejí tak vlastní obsah stránky
*         
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      22.5.2016
 *  \pre     --- 
 *  \bug     ---
 *  \warning ---
 *  \copyright 
 * 
 *
 * Chyby jsou zachyceny funkcí \c ObsluhaChyb(), která využije class \c Loging a zapíše ji do db.
 *
 * Začátek stránky je vytvořen voláním funkce \c hlavicka(), která obsahu je část \c <head> stranky.
 *
 * Následně je pomocí odpovídajících třídy vytvořen vlastní obsah stránky a na závěr je vložen soubor \c zapati.php, 
 * který (překvapivě) obsahuje záptatí stránky.
 *
 */ 
/**
 *
 */ 
session_start();

ob_start(); // bufering zacatku skriptu

//set_include_path(dirname (__FILE__));
set_include_path(get_include_path().";/classes");
date_default_timezone_set('Europe/Prague');
set_error_handler("ObsluhaChyb");	

/**
 * @brief Detekce zarizeni: PHONE|TABLET|COMPUTER
 */
require_once "MobileDetect".DIRECTORY_SEPARATOR ."Mobile_Detect.php";
$detect = new Mobile_Detect;
	
if($detect->isMobile() && !$detect->isTablet()){
	define ("PHONE",true);
	define("COMPUTER", false);
	define ("TABLET",false);
}elseif ($detect->isMobile() && $detect->isTablet()){
	define ("TABLET",true);
	define("COMPUTER", false);
	define ("PHONE",false);
}else {
	define("COMPUTER", true);
	define ("TABLET",false);
	define ("PHONE",false);
}

include 'konstanty.php';

include_once 'HttpPozadavek.php';
/*
if(!isset($_SESSION['user']['pk_uzivatel'])){
  $_SESSION['user']['pk_uzivatel']=0;
}
*/
/**
 */ 
if(($_SERVER['SERVER_ADDR']=="127.0.0.1") OR ($_SERVER['SERVER_ADDR']=="::1"))
{
    //
    // Nastavení při spuštění na lokalním serveru 

    // nastaveni zobrazovani chyb pro vyvoj
    ini_set('display_errors', '1');
    error_reporting(E_ALL);

  }else{
    //
    // Pro jiny nez lokalni server

    // zakaz zobrazeni chyb
    ini_set('display_errors', '0');
    error_reporting(E_ALL ^ E_NOTICE);

    // obsluha chyb vlastni funkci
    set_error_handler("ObsluhaChyb");	
  }
  $out_zacatek = ob_get_contents();
  ob_end_clean();
/*  
hlavicka();
*/
  ob_start();
// nahodny vyber pozadi stranky
if(BODY_BACKGROUND_IMAGE && (!PHONE)){
	$files=glob("img".DIRECTORY_SEPARATOR ."background".DIRECTORY_SEPARATOR ."*.*" );
	$i = rand(0,count($files)-1);
	$bgimage = str_replace(DIRECTORY_SEPARATOR,"/",$files[$i]);
	echo "<body style='background-image:url(\"$bgimage\");'class='mybody'>";
}else{
  echo "<body>";
}
googleanalytics(); // merici kod	

echo "<div id='obal-zahlavi'>";
echo "<div id='zahlavi'>&nbsp;";
//echo "<h1>Nabídka firmy Altra</h1>";
echo "</div>";
echo "</div>"; // id='obal-zahlavi'";

echo "<div id='obal'>";
navigace();
uvodnihlavicka();
echo "<div id='obsah' class='clearfix'>";
/* 
 * Blok try kde se porovadi vlastni logika programu 
 */
try{
		//ob_start();
      $httppoz = new HttpPozadavek();

      // kontrola pouzite metody a jeji nastaveni
      if (count($_GET)>0){
        $httppoz->setFormMetoda('GET');
      }elseif(count($_POST)>0){
        $httppoz->setFormMetoda('POST');
      }else{
        //echo"<br>Volání bez parametru";
      }
      $httppoz->ctiParametry();
      //echo "<br>";
      //var_dump($httppoz->argumenty);

      //echo "<h3>Method: ".$httppoz->getMetoda()."</h3>";

      /* Pokude vyprsel timout, je nastavena promena a vypise se zprava
       * Není-li nastavena, vykona se volana uloha *
       */
      if(isset($timeout_msg)){
        echo $timeout_msg; /* Pokud vyprsel timetou, zde se vypise zprava */
      }else{
        $class = mod2class($httppoz->getClassNo());
        call_user_func(array($class,$httppoz->getMetoda()),$httppoz->argumenty);
        $out_obsah = ob_get_contents();
        ob_end_clean();
        echo $out_zacatek;
        if(isset($_SESSION['seo']['title'])){
        	$title = $_SESSION['seo']['title'];
        }else{
        	$title = TAG_TITLE_GENERAL;
        }
        if(isset($_SESSION['seo']['description'])){
        	$description = $_SESSION['seo']['description'];
        }else{
        	$description = TAG_DESCRIPTION;
        }
        unset($_SESSION['seo']);
        hlavicka($title,$description);
        echo $out_obsah;
        /*
        echo "<p>";
        echo $_SESSION['seo']['title'];
        echo "</p>";
        echo "<p>";
        echo $_SESSION['seo']['description'];
        echo "</p>";
        */
      }
}
catch(PrfException $prfex){
  if($prfex->getCode()==10){
    //echo $prfex->getMessage();
    echo "<div class='varovani'>" . $prfex->getMessage()."</div>";
  }
}catch(Exception $e){
 
    obsluhaChyb($e->getCode(),$e->getMessage(),  $e->getFile(), $e->getLine());
    if(LADENI){
      echo "<p>";
      var_dump($e);
      echo "</p>";
    }
    echo "<div class='varovani'>Na serveru nastala nepředvídaná situace.</div>";
    //header("Location:index.php?msg='Byli jste odhlášení, nebyla zaznamenána žádná aktivita.'");
}
echo "</div>"; // id='obsah'
echo "<div id='navigace-zapati'>";
navigace_odkazy();
echo "</div>"; // id='navigace-zapati'
//echo "</div><!-- #obal -->";
echo"  <div id='obal-zapati'>";
echo"  <div id='zapati'>";
include "zapati.php";
echo"  </div> <!-- div zapati -->";
echo"  </div> <!-- div obal-zapati -->";
echo "</div><!-- #obal -->";
echo "</body>";
echo "</html>";

/**
  *  \brief 
  *  @param typ popis
  *  @return string vystup
  */
function uvodnihlavicka()
{ // BEGIN function
/*
 * Nahodny vyber baneru 
 */
echo "<div id='hlavicka-uvodni'>";
if(!PHONE){
	$files=glob("img".DIRECTORY_SEPARATOR ."banery".DIRECTORY_SEPARATOR ."*.*" );
	$i = rand(0,count($files)-1);
	echo "<img alt='AltraFootWear.cz' src='". $files[$i] ."' />";
}
	
echo "<div id=\"menu\" >";

$menu =  new MenuKategorie();
$menu->mkatPolozky();
  
echo " </div> <!-- id  menu-->";
echo "</div><!-- id hlavicka-uvodni -->";

} // END function
/**
  *  \brief 
  */
function navigace()
{ // BEGIN function
?>
<div id="odkazy" style="">
  <div style="text-align: center;" id="logo">
  <a href="index.php"><img src="img/altra-logo.svg"  title="Úvodní stránka" style="width:100%;"></a>
  </div>
<?PHP if (!PHONE) navigace_odkazy(); ?>
</div>
<!-- id odkazy -->
<?php
} // END function

function navigace_odkazy(){
	?>
  <div class="odkazy-item" >  
  <a href='index.php?mod=25&page=altra' title="O firmě Altra">Altra</a>
  </div>

  <div class="odkazy-item" >
  <a href='index.php?mod=25&page=prodejci' title="Seznam partnerských obchodů"> Prodejci </a>
  </div>

  <div class="odkazy-item" >  
  <a href='index.php?mod=25&page=kontakty'  title="Výhradní zastoupení">Kontakty</a>
  </div>
  
  <div class="odkazy-item">  
  <a href='pdf/2014_Edu Booklet_wtestimonials-cz.pdf'  title="Průvodce zdravým běháním" target="_new" >Běžecký průvodce</a>
  </div>
	<?php 
}
/**
 * \brief Prevede cislo \c mod (modul) na instanci objektu, ktery vraci.
 * \details Pokud bude predano nezname cislo, bude vracena instance class Uvod
 *
 *
 * @param int $mod cislo tridy, jez bude vracena jako objekt
 * @return objekt tridy, jejiz cislo dostala jako parametr
 */
 function mod2class($mod)
  {

    switch($mod){
    case 21:
      return new Index;
      break;
    case 22:
      return new MenuKategorie;
      break;
    case 23:
      return new ShowProducts;
      break;
    case 24:
      return new Details;
      break;
    case 25:
      return new Pages;
      break;
      default:
      return new Index; /* neznama trida, zobrazi se uvod */
    }
  }

    /**
     * \brief Hlavicka \c head cele stranky
     * \details Sem lze zapisovat vse, co je potreba nacist a lement <head>...</head>
     * Doctype je nastaven pro HTML5
     */
function hlavicka($title,$descript){
  echo"<!DOCTYPE html>\n";
echo"<html>\n";
echo"<head>\n";
echo"<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n";
echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<meta name=\"keywords\" content=\"" . TAG_KEYWORDS . "\">";
echo "<meta http-equiv='Expires' content='".GMDate('D, d m Y H:i:s')." GMT'>\n";
echo"<link rel='stylesheet' type='text/css' href='styles/index.css'>\n";
//echo "<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>";
//echo "\n<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>";
echo "\n<script src='jquery.js'></script>";
JS_skripty();
echo"<title>$title</title>\n";
echo "<meta name=\"description\" content=\"$descript \">";
echo"</head>\n";
}

function JS_skripty()
{ // BEGIN function JS_skripty
?>
	<script>
	/*
$(document).ready(function(){
    $("#menu a ").click(function(){
    	var href = $(this).attr("href");
		$(this).siblings("div").hide();
		$(this).siblings("a").removeClass("menu-item-selected");
		$(this).children("a").removeClass("menu-item-selected");
		$(this).addClass("menu-item-selected");
		$(href).show();
    });

});
*/
$(document).ready(function(){
	$("#odkazy > div").mouseover(function(){
		$(this).addClass("odkazy-zvyrazneni");
		});

	$("#odkazy > div").mouseout(function(){
		
		$(this).removeClass("odkazy-zvyrazneni");
		});

});
</script>
<?php

} // END function JSskripty


/**
 * Vlastni ovladac (handler) pro zpracovani neosetrenych chyb|stavu.
 *
 * Nazev funkce je prirazen pomoci funkce \c set_error_handler
 * \code
 * set_error_handler("ObsluhaChyb");	
 * \endcode
 *
 * @param integer $errno  Cislo chyby 
 * @param string $errstr Chybova zprava 
 * @param string $errfile Jmeno souboru, kde se chyba projevila
 * @param integer $errline Cislo radku, kde se chyba projevila
 * @return string vystup 
 */
function obsluhaChyb($errno, $errstr, $errfile, $errline)
{
  if(LADENI){
    echo "<p>Byla zachycena chyba: <br>Cislo: $errno <br> $errstr <br>Soubor: $errfile <br>r. $errline";
  }
  $severity = '';
  switch($errno){
  case E_NOTICE:
    $severity = 'NOTICE';
    break;
  case E_WARNING:
    $severity = 'WARNING';
    break;
  case E_ERROR:
    $severity = 'FATAL';
    break;
  case E_USER_NOTICE:
    $severity = 'NOTICE';
    break;
  case E_USER_WARNING:
    $severity = 'WARNING';
    break;
  case E_USER_ERROR:
    $severity = 'FATAL';
    break;
  default:
    $severity = 'uknown';
  }
  $log = new Loging($errno,$severity . ' - ' . $errstr, $errfile, $errline);
  $log->setFile(ERROR_FILE);
  $log->writeDb();
  //header("HTTP/1.0 500 Internal Server Error");
    //return;
  //throw new Exception();
}
/**
  * \brief Vypise linky pro prihlaseni a registraci, nebo k odhlasení a info o prihlasenem uzivateli
  * @param typ popis 
  * @return string vystup 
 */
 function loginInfo()
{
  if((!isset($_SESSION['user']))||($_SESSION['user']['pk_uzivatel']<=0)){
    echo "<a href=index.php?mod=4&amp;met=prFormular>Přihlášení</a>";
    echo " | <a href=index.php?mod=3&amp;met=registrace>Registrace</a>";
  }else{
    echo "<a href='index.php?mod=5&met=uuVlastniEditace&pk=".  $_SESSION['user']['pk_uzivatel'] ."'>";
    echo "<img src='img/ozub-kolo.png'height='16px'/>";
    echo "</a> ";
    echo $_SESSION['user']['nik'];
    echo " (" . statusnazev($_SESSION['user']['status']) . ") ";
    echo " | <a href=index.php?mod=4&amp;met=prLogOff>Odhlašení</a>";
  }
}
 /**
  * 
  * Spousti se pri vytvareni instance tridy jejiz declarace nebyla nalezena.
  *
  * @param string $className Nazev tridy, ktera nebyla nalezena 
  * @return void
  */

  function __autoload($className)
{
  $file = 'classes' . DIRECTORY_SEPARATOR . $className . '.php';
  include_once $file;
return;
}
/**
  *  \brief Vraci slovni nazev statusu uzivatele
  *  @param int $status Cislo statusu 
  *  @return string Status slovy 
  */
 function statusnazev($idstatus)
{
  switch($idstatus){
  case 2: 
    return "Neregistrovaný";
    break;
  case 8: 
    return "Registrovaný";
    break;
  case 32: 
    return "Ověřený";
    break;
  case 128: 
    return "Vedoucí";
    break;
  case 512: 
    return "Správce";
    break;
  case 2048: 
    return "Admin";
    break;
  default:
    return "- - -";
    break;
  }

}
/**
 * @brief Google Analytic pro AltraFootwear.cz
 * pro altrafootwear3|gmail
 */
function googleanalytics() {
	?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-87089627-1', 'auto');
  ga('send', 'pageview');

</script>	<?php 
}
?>

