INSERT INTO `altr_prodejci` (`pk_prodejce`, `prodejce`, `adresa`, `obec`, `telefon`, `web`, `email`) VALUES
(1,	'Trisport',	'',	'Svitavy',	' +420 461 619 957',	'www.trisportshop.cz ',	'info@trisportshop.cz'),
(2,	'AM-RUN s.r.o.',	'T. G. Masaryka 1107 ',	'Frýdek Místek',	'+420 605 282 560',	NULL,	NULL),
(3,	'Albie Praha',	'',	'Praha',	'+420 604 380 880',	NULL,	NULL),
(4,	'Runningshop Kočárník Praha',	'',	'Praha',	'+420 777 853 244',	NULL,	NULL),
(5,	'Peaksport Litomyšl',	'',	'Litomyšl',	'+420 603 153 935',	NULL,	NULL),
(6,	'Koloasport Pardubice',	'',	'Pardubice',	'+420 777 633 545',	NULL,	NULL),
(7,	'Tomáš Goder',	'',	NULL,	'+420 730 519 124',	NULL,	NULL),
(8,	'IPAX s.r.o., Trenčianské Teplice',	'',	'Trenčianské Teplice, Slovensko',	'+421 902 791 214',	NULL,	NULL);
