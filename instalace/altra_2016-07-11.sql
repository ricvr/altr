-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vygenerováno: Pon 11. čec 2016, 18:10
-- Verze MySQL: 5.7.13-log
-- Verze PHP: 5.6.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `altra`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_kategorie`
--

DROP TABLE IF EXISTS `altr_kategorie`;
CREATE TABLE IF NOT EXISTS `altr_kategorie` (
  `id_kategorie` int(11) NOT NULL AUTO_INCREMENT,
  `id_rodic` int(11) NOT NULL,
  `uroven` int(11) NOT NULL,
  `razeni` char(10) COLLATE utf8_czech_ci NOT NULL,
  `kategorie` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id_kategorie`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_login`
--

DROP TABLE IF EXISTS `altr_login`;
CREATE TABLE IF NOT EXISTS `altr_login` (
  `pk_message` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL COMMENT 'Cislo,kod chyby',
  `message` text COLLATE utf8_czech_ci COMMENT 'Text zpravy',
  `file` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL COMMENT 'Soubor, kde hlaska vznika',
  `line` int(11) DEFAULT NULL COMMENT 'Cislo radku',
  `d_ins` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`pk_message`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=65 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_menu`
--

DROP TABLE IF EXISTS `altr_menu`;
CREATE TABLE IF NOT EXISTS `altr_menu` (
  `pk_skupmenu` int(11) NOT NULL,
  `skupina` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(2550) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(11) NOT NULL,
  PRIMARY KEY (`pk_skupmenu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Názvy jednotlivých skupin v menu';

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_obrazky`
--

DROP TABLE IF EXISTS `altr_obrazky`;
CREATE TABLE IF NOT EXISTS `altr_obrazky` (
  `pk_obrazek` int(11) NOT NULL AUTO_INCREMENT,
  `fk_produkt` int(11) NOT NULL,
  `nazev` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `typ` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `obr_maly` mediumblob,
  `maly_vyska` int(11) DEFAULT NULL,
  `maly_sirka` int(11) DEFAULT NULL,
  `maly_velikost` int(11) DEFAULT NULL,
  `obr_stred` longblob,
  `stred_vyska` int(11) DEFAULT NULL,
  `stred_sirka` int(11) DEFAULT NULL,
  `stred_velikost` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_obrazek`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_opravneni`
--

DROP TABLE IF EXISTS `altr_opravneni`;
CREATE TABLE IF NOT EXISTS `altr_opravneni` (
  `pk_opravneni` int(11) NOT NULL AUTO_INCREMENT,
  `fk_uzivatel` int(11) NOT NULL,
  `iduloha` int(11) NOT NULL,
  `dins` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dval` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_opravneni`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Vazebni tabulka pro nastaveni opravneni uzivatelu' AUTO_INCREMENT=132 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_popisy`
--

DROP TABLE IF EXISTS `altr_popisy`;
CREATE TABLE IF NOT EXISTS `altr_popisy` (
  `pk_popis` int(11) NOT NULL AUTO_INCREMENT,
  `fk_produkt` int(11) NOT NULL,
  `vlastnost` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  `hodnota` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(11) NOT NULL,
  PRIMARY KEY (`pk_popis`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_prihlaseni`
--

DROP TABLE IF EXISTS `altr_prihlaseni`;
CREATE TABLE IF NOT EXISTS `altr_prihlaseni` (
  `pk_prihlaseni` int(11) NOT NULL AUTO_INCREMENT,
  `fk_uzivatel` int(11) NOT NULL,
  `id_uloha` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `id_session` char(200) COLLATE utf8_czech_ci NOT NULL,
  `d_akce` int(11) NOT NULL,
  PRIMARY KEY (`pk_prihlaseni`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=110 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_prodejci`
--

DROP TABLE IF EXISTS `altr_prodejci`;
CREATE TABLE IF NOT EXISTS `altr_prodejci` (
  `pk_prodejce` int(11) NOT NULL AUTO_INCREMENT,
  `prodejce` char(100) COLLATE utf8_czech_ci NOT NULL,
  `adresa` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `obec` char(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` char(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `web` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`pk_prodejce`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_produkty`
--

DROP TABLE IF EXISTS `altr_produkty`;
CREATE TABLE IF NOT EXISTS `altr_produkty` (
  `pk_produkt` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategorie` int(11) DEFAULT NULL,
  `nazev` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `popis_kratky` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis_dlouhy` text COLLATE utf8_czech_ci,
  `cenakc` int(11) DEFAULT '0',
  `cenacizi` int(11) DEFAULT '0',
  `cenamena` char(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `eshop_url` varchar(500) COLLATE utf8_czech_ci DEFAULT NULL,
  `eshop_text` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `dins` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_ins` int(11) DEFAULT NULL,
  `user_upd` int(11) DEFAULT NULL,
  `dupdate` timestamp NULL DEFAULT NULL,
  `detail1` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `detail2` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `detail3` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `detail4` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`pk_produkt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_ulohy`
--

DROP TABLE IF EXISTS `altr_ulohy`;
CREATE TABLE IF NOT EXISTS `altr_ulohy` (
  `pk_uloha` int(11) NOT NULL AUTO_INCREMENT,
  `uloha` varchar(30) COLLATE utf8_czech_ci DEFAULT NULL,
  `idmenuskup` int(11) DEFAULT NULL,
  `idclass` int(11) NOT NULL DEFAULT '0',
  `iduloha` int(11) NOT NULL,
  `link_text` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `link` varchar(300) COLLATE utf8_czech_ci NOT NULL,
  `pozn` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`pk_uloha`),
  UNIQUE KEY `iduloha_index` (`iduloha`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `altr_uzivatele`
--

DROP TABLE IF EXISTS `altr_uzivatele`;
CREATE TABLE IF NOT EXISTS `altr_uzivatele` (
  `pk_uzivatel` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `prijmeni` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `muz` int(1) NOT NULL DEFAULT '0',
  `verejne_jm` int(1) NOT NULL DEFAULT '0',
  `email` varchar(60) COLLATE utf8_czech_ci NOT NULL,
  `heslo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `dins` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `d_deaktiv` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_uzivatel`),
  UNIQUE KEY `nik_UNIQUE` (`nik`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
