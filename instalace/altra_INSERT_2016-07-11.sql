SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


INSERT INTO `altr_kategorie` (`id_kategorie`, `id_rodic`, `uroven`, `razeni`, `kategorie`, `popis`) VALUES
(1, 0, 1, '', 'Root', 'Nejvyšší úroveň'),
(2, 1, 2, 'a', 'Boty', 'Boty pro všechny'),
(3, 1, 2, 'b', 'Běžecký textil', 'Vše na běhání'),
(4, 1, 2, 'c', 'Doplňky', 'Nabídka pro všechny'),
(5, 2, 3, '', 'Muži', ''),
(6, 2, 3, '', 'Ženy', ''),
(7, 6, 3, '', 'Stability', ''),
(8, 6, 3, '', 'Neutral', ''),
(9, 5, 3, '', 'Neutral', ''),
(10, 5, 3, '', 'Stability', ''),
(11, 5, 3, '', 'Trail', ''),
(12, 5, 3, '', 'EverDay', ''),
(14, 6, 3, '', 'Everday', ''),
(13, 6, 4, '', 'Trail', '');

INSERT INTO `altr_login` (`pk_message`, `code`, `message`, `file`, `line`, `d_ins`, `text`) VALUES
(62, 0, 'uknown - Table ''altra.altr_prodejci'' doesn''t exist || SELECT \r\n	    		pk_prodejce\r\n	    		,prodejce\r\n	    		,adresa\r\n	    		,obec\r\n	    		,telefon\r\n	    		,web\r\n	    		,email\r\n	    		 FROM altr_prodejci', 'C:\\dev\\www\\altra\\classes\\DB_MysqlStatement.php', 92, '2016-07-11 12:23:53', NULL),
(63, 0, 'uknown - Table ''altra.altr_prodejci'' doesn''t exist || SELECT \r\n	    		pk_prodejce\r\n	    		,prodejce\r\n	    		,adresa\r\n	    		,obec\r\n	    		,telefon\r\n	    		,web\r\n	    		,email\r\n	    		 FROM altr_prodejci', 'C:\\dev\\www\\altra\\classes\\DB_MysqlStatement.php', 92, '2016-07-11 12:24:08', NULL),
(64, 0, 'uknown - Unknown column ''prodejce'' in ''field list'' || SELECT \r\n	    		pk_prodejce\r\n	    		,prodejce\r\n	    		,adresa\r\n	    		,obec\r\n	    		,telefon\r\n	    		,web\r\n	    		,email\r\n	    		 FROM altr_prodejci', 'C:\\dev\\www\\altra\\classes\\DB_MysqlStatement.php', 92, '2016-07-11 17:50:39', NULL);

INSERT INTO `altr_menu` (`pk_skupmenu`, `skupina`, `popis`, `poradi`) VALUES
(3, 'Sprava', 'Správa stránek podle oprávnění', 30),
(2, 'Produky', 'Veškeré práce spojené s produky, vkládání a úpravy.', 20),
(1, 'Domu', 'Úvodní menu pro všechny uživatele', 10),
(4, 'Administrace', 'Přístup pro administrátora.', 40);

INSERT INTO `altr_opravneni` (`pk_opravneni`, `fk_uzivatel`, `iduloha`, `dins`, `dval`) VALUES
(122, 1, 4, '2016-07-11 12:10:46', NULL),
(123, 1, 8, '2016-07-11 12:10:46', NULL),
(124, 1, 9, '2016-07-11 12:10:46', NULL),
(125, 1, 10, '2016-07-11 12:10:46', NULL),
(126, 1, 11, '2016-07-11 12:10:46', NULL),
(127, 1, 12, '2016-07-11 12:10:46', NULL),
(128, 1, 13, '2016-07-11 12:10:46', NULL),
(129, 1, 14, '2016-07-11 12:10:46', NULL),
(130, 1, 15, '2016-07-11 12:10:46', NULL),
(131, 1, 16, '2016-07-11 12:10:46', NULL);

INSERT INTO `altr_ulohy` (`pk_uloha`, `uloha`, `idmenuskup`, `idclass`, `iduloha`, `link_text`, `link`, `pozn`) VALUES
(1, 'Ověřování uživatelů', 3, 3, 4, 'Seznam uživatelů', 'sprava.php?mod=3&amp;met=uzSeznam', ''),
(2, 'Správa uživatelů', 4, 0, 8, 'Správa uživatelů', '', ''),
(3, 'Úvodní stránka', 1, 0, 9, 'Úvod', 'sprava.php', ''),
(4, 'Editace uživatele', 0, 0, 10, '---', '---', 'Volá se ze seznamu uživatelů'),
(5, 'Změna osobních údajů', NULL, 5, 11, '', '', ' Zmena udaju uzivatelem'),
(6, 'Změna hesla uživateli', NULL, 3, 12, '', '', 'Nastavení nového hesla uživatele správcem nebo administrátorem'),
(7, NULL, 3, 0, 13, 'Vytvořit nového uživatele', 'sprava.php?mod=3&amp;met=uzNovy', ''),
(8, 'Vložení nového produktu', 1, 6, 14, 'Vložení produktu', 'sprava.php?mod=6&amp;met=prodForm', ''),
(9, 'Zobrazení seznamu produktů', 1, 6, 15, 'Seznam produktů', 'sprava.php?mod=6&amp;met=prodSeznam', 'Zobrazení seznamu produktů, odkud je přístup k jejich editaci'),
(10, 'Odstranění produktu', 1, 6, 16, '', '', 'Editace a odstranění produktu');

INSERT INTO `altr_uzivatele` (`pk_uzivatel`, `nik`, `jmeno`, `prijmeni`, `muz`, `verejne_jm`, `email`, `heslo`, `status`, `dins`, `d_deaktiv`) VALUES
(1, 'admin', 'Administrator', 'Hlavní', 0, 0, '', '$2y$10$qXBQSedJYGyltDm/xIHPMufkiFxhaVZ8QPaEDXI2uxj8sepAmDlsK', 2048, '2016-05-29 14:57:35', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
