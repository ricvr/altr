<?php 
define ("LADENI",true); /**< \brief true=program se ladí false=neladí se */
define ("POPIS_KATEGORIE",true); /**< \brief TRUE=pri vyberu kategorie se zobrazi jeji popis FALSE=nezobrazi se */
define ("BODY_BACKGROUND_IMAGE",true);/**< Zobrazovani obrazku na pozadi. __true obrazek, __false bez obrazku */

define ("STATUS_NEPRIHLASENY",2);
define ("STATUS_REGISTROVANY",8);
define ("STATUS_OVERENY",32);
define ("STATUS_VEDOUCI",128);
define ("STATUS_SPRAVCE",512);
define ("STATUS_ADMIN",2048);


define('TABLE_PREFIX','altr'); /**< Prefix pro nazvy tabulek */
define('ERROR_FILE','error.log');
define('PRODUCT_IMAGES_DIR',"products". DIRECTORY_SEPARATOR ."images". DIRECTORY_SEPARATOR);
define("TAG_TITLE_COMMON", "Altra-obuv"); /**< Zacatek tagu <title> který se doponí dalším, napr. nazvem produktu */
define("TAG_TITLE_GENERAL", "Altra sportovní obuv na běh"); /**< Obecný obsah tagu <title> */
define("TAG_DESCRIPTION", "Altra, obuv pro sport i každodenní nošení. Běh v terénu i na silnici"); /**< Obsah tagu description */
define("TAG_KEYWORDS", "Altra běh a běhání v terénu na silnici, každodenní nošení, Zdeněk Moravčík"); /**< Obsah tagu keywords */

define("IDULOHA_UZIVATELE_SEZNAM", 4) ;
define("IDULOHA_UZIVATEL_EDITACE", 10) ; /**< \brief editace uživatele vybraného ze seznamu uživatelů */
define("IDULOHA_UZIVATEL_UPRAVA_UDAJU", 11) ; /**< \brief Zmena udaju uzivatelem */
define("IDULOHA_NASTAVENI_HESLA", 12) ; /**< \brief Nastaveni hesla Spravcem|Administratorem */
define("IDULOHA_UZIVATEL_NOVY", 13) ; /**< \brief Opravneni k vytvoreni noveho uzivatele */

define("IDULOHA_PRODUKTY_VLOZENI", 14) ; /**< \brief Vkladani produktu */
define("IDULOHA_PRODUKTY_SEZNAM", 15) ; /**< \brief Zobrazeni seznamu prodkutku */
define("IDULOHA_PRODUKTY_EDITACE", 16) ; /**< \brief Zmena a odstraneni produktu */
define("IDULOHA_PRODEJCI", 17) ; /**< \brief Sprava seznamu prodejcu/obchodnich partneru */
define("IDULOHA_KATEGORIE_SPRAVA", 18) ; /**< \brief Sprava kategorii, do kterych se zarazuji produkty */

/*
  Nastaveni velikosti obrazku pro ulozeni do db
*/
define("MALY_SIRKA",200);
define("STRED_SIRKA",800);

define("MAX_VELIKOST_OBRAZKU",300000); /**< Max velikost 300kB*/
?>