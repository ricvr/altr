<?php 
/**
 * $Id$
 * @file sprava.php 
 *
 *  \brief     Úvodní skript pro spravu stranek
 * \details V tomto souboru jsou volány třídy, které budou provádět jednotlivé funkce pro vkladani a udrzbu produktu
 *
 *  \author    RiC
 *    
 *  \version   0.1
 *  \date      29.5.2016
 *  \pre     --- 
 *  \bug     ---
 *  \warning ---
 *  \copyright 
 * 
 *
 * Chyby jsou zachyceny funkcí \c ObsluhaChyb(), která využije class \c Loging a zapíše ji do db.
 *
 * Začátek stránky je vytvořen voláním funkce \c hlavicka(), která obsahu je část \c <head> stranky.
 *
 * Následně je pomocí odpovídajících třídy vytvořen vlastní obsah stránky a na závěr je vložen soubor \c zapati.php, 
 * který (překvapivě) obsahuje záptatí stránky.
 *
 */ 
/**
 *
 */ 
session_start();
//set_include_path(dirname (__FILE__));
set_include_path(get_include_path().";/classes");
date_default_timezone_set('Europe/Prague');
set_error_handler("ObsluhaChyb");	
include 'konstanty.php';

include_once 'HttpPozadavek.php';

if(!isset($_SESSION['user']['pk_uzivatel'])){
  $_SESSION['user']['pk_uzivatel']=0;
}
/**
 */ 
if(($_SERVER['SERVER_ADDR']=="127.0.0.1") OR ($_SERVER['SERVER_ADDR']=="::1"))
{
    //
    // Nastavení při spuštění na lokalním serveru 

    // nastaveni zobrazovani chyb pro vyvoj
    ini_set('display_errors', '1');
    error_reporting(E_ALL);

  }else{
    //
    // Pro jiny nez lokalni server

    // zakaz zobrazeni chyb
    ini_set('display_errors', '0');
    error_reporting(E_ALL ^ E_NOTICE);

    // obsluha chyb vlastni funkci
    set_error_handler("ObsluhaChyb");	
  }
ob_start();
hlavicka();

echo "<body onLoad='vyskazahlavi();'>";
echo "<div id='obal'>";

echo "<div id='zahlavi'>";
echo "<h1><em><a href='index.php'>AltraFootWear.cz</a></em> - administrace</h1>";
echo "</div>";





$opravneni = new Opravneni($_SESSION['user']['pk_uzivatel'],session_id(),0);
if($opravneni->kontrola_timeout() < 0 && $_SESSION['user']['pk_uzivatel'] > 0){
	unset($_SESSION['user']);
  //throw new PrfException("Delší dobu nebyla zaznamenána žádná aktivita.",10);
  $timeout_msg = "<div class='info'>Došlo k automatickému odhlášení. <br>Po dobu " . Options::$timeout . "&nbsp;s nebyla zaznamenáná žádná aktivita.</div>";
  //$prihlaseni = new Prihlaseni();
  //$prihlaseni->prLogOff();
}

echo "<div id='menu'>";
menu();
echo "  <div id='login'>";
loginInfo();
echo "  </div>"; //id='login'
echo "</div>";

echo "<div id='obsah'>";

/* Blok try kde se porovadi vlastni logika programu */
try{
      $httppoz = new HttpPozadavek();

      // kontrola pouzite metody a jeji nastaveni
      if (count($_GET)>0){
        $httppoz->setFormMetoda('GET');
      }elseif(count($_POST)>0){
        $httppoz->setFormMetoda('POST');
      }else{
        //echo"<br>Volání bez parametru";
      }
      $httppoz->ctiParametry();

      /* Pokude vyprsel timout, je nastavena promena a vypise se zprava
       * Není-li nastavena, vykona se volana uloha *
       */
      if(isset($timeout_msg)){
      	 
        echo $timeout_msg; /* Pokud vyprsel timetou, zde se vypise zprava */
      }else{
      	 
        $class = mod2class($httppoz->getClassNo());
        call_user_func(array($class,$httppoz->getMetoda()),$httppoz->argumenty);
      }
ob_flush();
flush();
}
catch(PrfException $prfex){
  if($prfex->getCode()==10){
    //echo $prfex->getMessage();
    echo "<div class='varovani'>" . $prfex->getMessage()."</div>";
  }
}catch(Exception $e){
 
    obsluhaChyb($e->getCode(),$e->getMessage(),  $e->getFile(), $e->getLine());
    if(LADENI){
      var_dump($e);
    }
    echo "<div class='varovani'>Na serveru nastala nepředvídaná situace.</div>";
    //header("Location:index.php?msg='Byli jste odhlášení, nebyla zaznamenána žádná aktivita.'");
}
echo "</div>"; // id='obsah'
echo"  <div id='zapati'>";
include "zapati.php";
echo"  </div> <!-- div zapati -->";
echo "</div><!-- #obal -->";
echo "</body>";
echo "</html>";

/**
 * \brief Vytvari hlavni menu
 * \details Nacita polozky z databaze a z nich sestavi nabidku uloh podle prav uzivatele
  * @param typ popis 
  * @return string vystup 
 */
 function menu()
 {
//var_dump($_SESSION['user']['prava']);

echo "  <ul id='hlavni-menu'>";

/* Nejdrive kontrola, jestli uzivatel je prihlasen */
if(isset($_SESSION['user'])){
  $pk_uzivatel = $_SESSION['user']['pk_uzivatel'];
}else{
  $pk_uzivatel = 0;
}
$usr = new Uzivatele();
$arrPrava = $usr->dejPravaUzivatele($pk_uzivatel);// načtení pole s právy a linky

// Kontrola, jestli ma nejaka prava
if(count($arrPrava)>0){
  $idmenuskup = $arrPrava[0]['idmenuskup'];
  //$skupina = $arrPrava[0]['skupina'];
} else{
  return;
}
//echo "<br>",$idmenuskup,'<br>',$arrPrava[0]['idmenuskup'];
if($idmenuskup > 0){
  echo "<li class='vodorovne'><ul>";
  echo "<li>" . $arrPrava[0]['skupina'] . "</li>";
}
$counter = 0;
foreach($arrPrava as $key => $val){
  /* IdMenuSkup==0 <-- uloha, ktera nepatri do menu. */
  if($val['idmenuskup'] == 0){
    continue;
  }
  if($idmenuskup == $val['idmenuskup']){
  	// nedela se nic
  }else{
    /* Nastaveni dalsi skupiny */
    if($counter > 0){
        echo "</ul></li>"; // konec predchozi skupiny
        $counter++;
    }else{
        echo ""; // konec predchozi skupiny
        $counter++;
    }
    $idmenuskup = $val['idmenuskup'];
    echo "<li class='vodorovne'><ul>"; //zacatek skupiny
    echo "<li>" . $val['skupina'] . "</li>";
  }
  echo "<li class='menu-link'>";
  echo "<a href='" . $val['link'] . "'>" . $val['link_text'] . "</a>" ;
  echo "</li>";
}
echo "</ul></li>";
echo "</ul>";
unset($usr);
}
/**
 * \brief Prevede cislo \c mod (modul) na instanci objektu, ktery vraci.
 * \details Pokud bude predano nezname cislo, bude vracena instance class Uvod
 *
 *
 * @param int $mod cislo tridy, jez bude vracena jako objekt
 * @return objekt tridy, jejiz cislo dostala jako parametr
 */
 function mod2class($mod)
  {

    switch($mod){
    case 1:
      return new Sprava;
      break;
    case 3:
      return new Uzivatele;
      break;
    case 4:
      return new Prihlaseni;
      break;
    case 5:
      return new Uzivatelzmeny;
      break;
    case 6:
      return new Produkty;
      break;
    case 7:
      return new Obrazky;
      break;
    case 8:
      return new Prodejci;
      break;
    case 9:
      return new KategorieEdit;
      break;
     default:
      return new Sprava; /* neznama trida, zobrazi se uvod */
    }
  }

    /**
     * \brief Hlavicka \c heac cele stranky
     * \details Sem lze zapisovat vse, co je potreba nacist a lement <head>...</head>
     * Doctype je nastaven pro HTML5
     */
function hlavicka(){
echo"<!DOCTYPE html>\n";
echo"<html>\n";
echo"<head>\n";
echo"<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n";
echo "<meta http-equiv='Expires' content='".GMDate('D, d m Y H:i:s')." GMT'>\n";
echo"<link rel='stylesheet' type='text/css' href='styles/sprava.css'>\n";
echo "\n<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>";
echo "\n<script src='jquery.js'></script>";
echo "\n<script src='js/ckeditor/ckeditor.js'></script>";
echo"<title>AltraFootwear - Administrace</title>\n";
?>
<script>
function vyskazahlavi(){
    var vyska =$('#zahlavi-levy-obr').css('height');
    $('#zahlavi').css('height',vyska);
    //var vyskah1 =$('#zahlavi-stred').css('height');
    $('#zahlavi-stred > img').css('height',vyska);
}

$(document).ready(function(){
  vyskazahlavi();
  $(window).resize(function() {
           vyskazahlavi();
           });
});
    
</script>
<?php
echo"</head>\n";
}

/**
 * Vlastni ovladac (handler) pro zpracovani neosetrenych chyb|stavu.
 *
 * Nazev funkce je prirazen pomoci funkce \c set_error_handler
 * \code
 * set_error_handler("ObsluhaChyb");	
 * \endcode
 *
 * @param integer $errno  Cislo chyby 
 * @param string $errstr Chybova zprava 
 * @param string $errfile Jmeno souboru, kde se chyba projevila
 * @param integer $errline Cislo radku, kde se chyba projevila
 * @return string vystup 
 */
function obsluhaChyb($errno, $errstr, $errfile, $errline)
{
  if(LADENI){
    echo "<p>Byla zachycena chyba: <br>Cislo: $errno <br> $errstr <br>Soubor: $errfile <br>r. $errline";
  }
  $severity = '';
  switch($errno){
  case E_NOTICE:
    $severity = 'NOTICE';
    break;
  case E_WARNING:
    $severity = 'WARNING';
    break;
  case E_ERROR:
    $severity = 'FATAL';
    break;
  case E_USER_NOTICE:
    $severity = 'NOTICE';
    break;
  case E_USER_WARNING:
    $severity = 'WARNING';
    break;
  case E_USER_ERROR:
    $severity = 'FATAL';
    break;
  default:
    $severity = 'uknown';
  }
  $log = new Loging($errno,$severity . ' - ' . $errstr, $errfile, $errline);
  $log->setFile(ERROR_FILE);
  $log->writeDb();
  //header("HTTP/1.0 500 Internal Server Error");
    //return;
  //throw new Exception();
}
/**
  * \brief Vypise linky pro prihlaseni a registraci, nebo k odhlasení a info o prihlasenem uzivateli
  * @param typ popis 
  * @return string vystup 
 */
 function loginInfo()
{
  if((!isset($_SESSION['user']))||($_SESSION['user']['pk_uzivatel']<=0)){
    echo "<a href=sprava.php?mod=4&amp;met=prFormular>Přihlášení</a>";
    //echo " | <a href=index.php?mod=3&amp;met=registrace>Registrace</a>";
  }else{
    echo "<a href='sprava.php?mod=5&amp;met=uuVlastniEditace&amp;pk=".  $_SESSION['user']['pk_uzivatel'] ."'>";
    echo "<img src='img/ozub-kolo.png' height='16' alt='Nastavení účtu'/>";
    echo "</a> ";
    echo $_SESSION['user']['nik'];
    echo " (" . statusnazev($_SESSION['user']['status']) . ") ";
    echo " | <a href='sprava.php?mod=4&amp;met=prLogOff'>Odhlašení</a>";
  }
}
 /**
  * 
  * Spousti se pri vytvareni instance tridy jejiz declarace nebyla nalezena.
  *
  * @param string $className Nazev tridy, ktera nebyla nalezena 
  * @return void
  */

  function __autoload($className)
{
  $file = 'classes' . DIRECTORY_SEPARATOR . $className . '.php';
  include_once $file;
return;
}
/**
  *  \brief Vraci slovni nazev statusu uzivatele
  *  @param int $status Cislo statusu 
  *  @return string Status slovy 
  */
 function statusnazev($idstatus)
{
  switch($idstatus){
  case 2: 
    return "Neregistrovaný";
    break;
  case 8: 
    return "Registrovaný";
    break;
  case 32: 
    return "Ověřený";
    break;
  case 128: 
    return "Vedoucí";
    break;
  case 512: 
    return "Správce";
    break;
  case 2048: 
    return "Admin";
    break;
  default:
    return "- - -";
    break;
  }

}
?>

